//
//  SongQuery.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 13/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import MediaPlayer

struct SongInfo {
    
    var albumTitle: String
    var artistName: String
    var songTitle:  String
    
    var songId   :  NSNumber
}

struct AlbumInfo {
    
    var albumTitle: String
    var songs: [SongInfo]
}

class SongQuery {
    
    func get(songCategory: String,collection : DynamicCollectionView,playlistNum: Int = 0) -> [SongInfo] {
        
        var albums: [AlbumInfo] = []
        let albumsQuery: MPMediaQuery
     
  
        if songCategory == "Artist" {
            albumsQuery = MPMediaQuery.artists()
           
        } else if songCategory == "Album" {
            albumsQuery = MPMediaQuery.albums()
            
        } else {
            albumsQuery = MPMediaQuery.playlists()
        }
        
        let albumItems: [MPMediaItemCollection]
        // let albumsQuery: MPMediaQuery = MPMediaQuery.albums()
        if songCategory != "Artist"{
     albumItems =  [albumsQuery.collections![playlistNum]] as [MPMediaItemCollection]
        }else{
       albumItems  = albumsQuery.collections! as [MPMediaItemCollection]
        }
        //  var album: MPMediaItemCollection
        var AllSongs : [SongInfo] = []
        for album in albumItems {
            
            let albumItems: [MPMediaItem] = album.items as [MPMediaItem]
           
            // var song: MPMediaItem
            
            var songs: [SongInfo] = []
         
            var albumTitle: String = ""
            for song in albumItems {
               // if song.isCloudItem == false {
                print("now:",song.title)
                if songCategory == "Artist" {
                    albumTitle = song.value( forProperty: MPMediaItemPropertyArtist ) as! String
                    
                } else if songCategory == "Album" {
                    albumTitle = song.value( forProperty: MPMediaItemPropertyAlbumTitle ) as! String
                    
                    
                }
                if song.value( forProperty: MPMediaItemPropertyArtist) != nil {
                let songInfo: SongInfo = SongInfo(
                    albumTitle: song.value( forProperty: MPMediaItemPropertyAlbumTitle ) as! String,
                    artistName: song.value( forProperty: MPMediaItemPropertyArtist ) as! String,
                    songTitle:  song.value( forProperty: MPMediaItemPropertyTitle ) as! String,
                    songId:     song.value( forProperty: MPMediaItemPropertyPersistentID ) as! NSNumber
                )
                songs.append( songInfo )
               // }
                }else{
                    let songInfo: SongInfo = SongInfo(
                        albumTitle: song.value( forProperty: MPMediaItemPropertyAlbumTitle ) as! String,
                        artistName: "",
                        songTitle:  song.value( forProperty: MPMediaItemPropertyTitle ) as! String,
                        songId:     song.value( forProperty: MPMediaItemPropertyPersistentID ) as! NSNumber
                    )
                    songs.append( songInfo )
                }
            }
            
            let albumInfo: AlbumInfo = AlbumInfo(
                
                albumTitle: albumTitle,
                songs: songs
            )
            
            AllSongs = AllSongs + songs
            albums.append( albumInfo )
        }
        //collection.reloadData()
        //let a = Array(AllSongs.prefix(upTo: 10))
        
        print(AllSongs)
        return AllSongs
        
    }
    func getItem2( songId: NSNumber,row:Int,playlistNum:Int ) -> MPMediaItem {
        
        let property: MPMediaPropertyPredicate = MPMediaPropertyPredicate( value: songId, forProperty: MPMediaItemPropertyPersistentID )
        
        let query: MPMediaQuery = MPMediaQuery()
        
        query.addFilterPredicate( property )
        print("playlistNum:",playlistNum)
        print("row:",row)
       // let x =
        
        let items = MPMediaQuery.playlists().collections?[playlistNum].items[row]
   print(items?.title)
        
        return items!
    }
    func findSongWithPersistentIdString(persistentIDString: String) -> MPMediaItem? {
        let predicate = MPMediaPropertyPredicate(value: persistentIDString, forProperty: MPMediaItemPropertyPersistentID)
        let songQuery = MPMediaQuery()
        songQuery.addFilterPredicate(predicate)
        
        var song: MPMediaItem?
        if let items = songQuery.items, items.count > 0 {
            song = items[0]
        }
        return song
    }
    func getItem( songId: NSNumber ) -> MPMediaItem {
        
        let property: MPMediaPropertyPredicate = MPMediaPropertyPredicate( value: songId, forProperty: MPMediaItemPropertyPersistentID )
        
        let query: MPMediaQuery = MPMediaQuery()
        query.addFilterPredicate( property )
        
        var items: [MPMediaItem] = query.items! as [MPMediaItem]
        
        return items[items.count - 1]
        
    }
    
}
