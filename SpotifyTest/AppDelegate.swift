//
//  AppDelegate.swift
//  SpotifyTest
//
//  Created by Seth Rininger on 10/27/16.
//  Copyright © 2016 Seth Rininger. All rights reserved.
//

import UIKit
import AudioPlayerManager
import AVFoundation
import MediaPlayer
import Alamofire
import SwiftyJSON
import SwiftSpinner
import IQKeyboardManagerSwift
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SPTAudioStreamingDelegate {
  var timer: DispatchSourceTimer?
    var timer2: DispatchSourceTimer?
    var window: UIWindow?
    var TestTimer : DispatchSourceTimer?
    @objc var session: SPTSession?
    var firstInterrupt = false
    @objc var player: SPTAudioStreamingController?
    @objc let kClientId = "754dd512a3ee424eb1311ce013b778f3"
    @objc let kClientSecret = "0eb665c8a1a44eeb8087541bc55dfdfb"
    @objc let kCallbackURL = "larry://authorization"
    @objc let kTokenSwapURL = "https://larry2.herokuapp.com/api/spotify/auth/getToken"
    @objc let kTokenRefreshServiceURL = "https://larry2.herokuapp.com/api/spotify/auth/refreshToken"
    @objc let kSessionUserDefaultsKey = "SpotifySession"
    let musicPlayerManager = MusicPlayerManager.shared
    @objc func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //fetchDeveloperToken() { developer in }
        IQKeyboardManager.sharedManager().enable = true
        SPTAuth.defaultInstance().clientID = kClientId
//NotificationCenter.default.addObserver(self, selector: #selector(self.onAudioSessionEvent), name: .AVAudioSessionInterruption, object: nil)
        SPTAuth.defaultInstance().redirectURL = URL(string:kCallbackURL)
        SPTAuth.defaultInstance().tokenSwapURL = URL(string:kTokenSwapURL)
        SPTAuth.defaultInstance().requestedScopes = [SPTAuthStreamingScope,SPTAuthPlaylistReadPrivateScope]
       SPTAuth.defaultInstance().tokenRefreshURL = URL(string: kTokenRefreshServiceURL)!
        SPTAuth.defaultInstance().sessionUserDefaultsKey = kSessionUserDefaultsKey
         NotificationCenter.default.addObserver(self, selector: #selector(self.startCount), name: NSNotification.Name(rawValue: "startCount"), object: nil)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
       
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        if SPTAuth.defaultInstance().session != nil {
         //NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "startCount"), object: self)
        }
        return true
    }
    @objc func startCount(){
        stopTimer()
        startTimer()
    }
    func stopTimer() {
        timer?.cancel()
        timer = nil
    }
    
    deinit {
        self.stopTimer()
    }
    @objc func onAudioSessionEvent(_ notification: Notification) {
    //Check the type of notification, especially if you are sending multiple AVAudioSession events here
        print("inter")
    if (notification.name == .AVAudioSessionInterruption) {
        print("Interruption notification received!")
        //Check to see if it was a Begin interruption
        if notification.userInfo != nil {
        if ((notification.userInfo?[AVAudioSessionInterruptionTypeKey] as! NSNumber) == NSNumber(value: Int8(AVAudioSessionInterruptionType.began.rawValue))) {
            print("Interruption began!")
         /*   if player2.shared.musicPlayerManager.isPlaying() {
          
         //   if player2.shared.musicPlayerManager.isPlaying() || AudioPlayerManager.shared.isPlaying() {
                player2.shared.play_pause(self)
                player2.shared.updateTabBar()
            } */
            
           // }
                if player2.shared.SpotiyOrApple == "apple" {
                     if firstInterrupt  {
                        player2.shared.play_pause_apple()
                        player2.shared.updateTabBar()
                     }else {
                         NotificationCenter.default.post(name: .AVAudioSessionInterruption , object: NSNumber(value: Int8(AVAudioSessionInterruptionType.ended.rawValue)))
                      
                    }
                    firstInterrupt = true
                }else {
                   player2.shared.play_pause_apple()
                    player2.shared.updateTabBar()
            }
            }else {
                if AudioPlayerManager.shared.isPlaying() {
                player2.shared.play_pause(self)
                player2.shared.updateTabBar()
                }
            }
        }else {
            print("Interruption ended!")
            //Resume your audio
            // if player2.shared.musicPlayerManager.musicPlayerController.currentPlaybackTime != nil || AudioPlayerManager.shared.currentTrack != nil {
            
            if player2.shared.SpotiyOrApple == "apple" {
                
                player2.shared.play_pause_apple()
                player2.shared.updateTabBar()
                
            }else {
                if AudioPlayerManager.shared.isPlaying() {
                    player2.shared.play_pause(self)
                    player2.shared.updateTabBar()
                }
            }
        }
        //
        
        }
        else {
            print("Interruption ended!")
            //Resume your audio
           // if player2.shared.musicPlayerManager.musicPlayerController.currentPlaybackTime != nil || AudioPlayerManager.shared.currentTrack != nil {

        if player2.shared.SpotiyOrApple == "apple" {
           
                player2.shared.play_pause_apple()
                player2.shared.updateTabBar()
           
        }else {
            if AudioPlayerManager.shared.isPlaying() {
                player2.shared.play_pause(self)
                player2.shared.updateTabBar()
            }
        }
           // }
            
        }
    }

    func startTimer() {
        let queue = DispatchQueue(label: "com.domain.app.timer")  // you can also use `DispatchQueue.main`, if you want
        timer = DispatchSource.makeTimerSource(queue: queue)
        timer!.schedule(deadline: .now(), repeating: .seconds(3500))
        timer!.setEventHandler { [weak self] in
            
            self?.renewTokenAndShowPlayer()
        }
        timer!.resume()
        let queue2 = DispatchQueue(label: "com.domain.app.timer")  // you can also use `DispatchQueue.main`, if you want
     /*   timer2 = DispatchSource.makeTimerSource(queue: queue2)
        timer2!.schedule(deadline: .now(), repeating: .seconds(1))
       var i = 0
        timer2!.setEventHandler { [weak self] in
            i = i + 1
            
           print("seccc",i)
        }
        timer2!.resume() */
        
            
        
        
        
        
        
    }
    @objc func renewTokenAndShowPlayer() {
        
        let auth = SPTAuth.defaultInstance()
        if auth?.session != nil {
        if let c =  auth?.session.encryptedRefreshToken  {
        let params: Parameters = [
            "grant_type": "refresh_token",
            "refresh_token": auth!.session.encryptedRefreshToken
        ]
            DispatchQueue.main.async(execute: {
                 SwiftSpinner.show("Refreshing Data...")
                
            })
           
        // print("error:",auth!.session.encryptedRefreshToken)
        Alamofire.request(auth!.tokenRefreshURL.absoluteString , method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                let a = JSON(response.data)
                print(a)
                let session =  SPTSession(userName: "754dd512a3ee424eb1311ce013b778f3", accessToken: a["access_token"].stringValue, encryptedRefreshToken: auth!.session.encryptedRefreshToken, expirationDate: Date(timeIntervalSinceNow: TimeInterval(a["expires_in"].intValue)))
                let userDefaults = UserDefaults.standard
                let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                
                userDefaults.set(sessionData, forKey: "SpotifySession")
                userDefaults.synchronize()
                SPTAuth.defaultInstance().session = session
                MediaPlayer.shared.configurePlayer(authSession: session!, id: SPTAuth.defaultInstance().clientID)
                print("refresh:",SPTAuth.defaultInstance().session.encryptedRefreshToken)
                 DispatchQueue.main.async(execute: {
                SwiftSpinner.hide()
                })
        }
        }
        }
        
    }
    func fetchDeveloperToken(completionHandler:@escaping (String) -> ()) {
        
        // MARK: ADAPT: YOU MUST IMPLEMENT THIS METHOD
        print("let's go")
        Alamofire.request("https://larry2.herokuapp.com/api/apple/auth/getToken", method: .get,encoding: JSONEncoding.default).responseJSON { response in
            
            let a = JSON(response.data)
            //let b = response.result
            print("a:",a["token"].stringValue)
            let developerAuthenticationToken = a["token"].stringValue
            UserDefaults.standard.set(developerAuthenticationToken, forKey: "developerToken")
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "saifToken"), object: nil)
            completionHandler(developerAuthenticationToken)
            
            
        }
        
    }
    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.endReceivingRemoteControlEvents()
        self.deactivateAudio()
    }
    func deactivateAudio () {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        UIApplication.shared.endReceivingRemoteControlEvents()
        
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // Ask SPTAuth if the URL given is a Spotify authentication callback

        print("The URL: \(url)")
        if SPTAuth.defaultInstance().canHandle(url) {
            var q = ((((url.absoluteString.components(separatedBy: "="))[1]).components(separatedBy: "&"))[0])
        
            print("code:saif",q)
            let params: Parameters = [
                "code": q,
                "redirect_uri" : kCallbackURL,
                "grant_type" : "authorization_code"
            ]
            Alamofire.request("https://larry2.herokuapp.com/api/spotify/auth/getToken" , method: .post, parameters: params, encoding: JSONEncoding.default)
                .responseJSON { response in
                    let a = JSON(response.data)
                    print("saif:",a)
                    
                   let session =  SPTSession(userName: self.kClientId, accessToken: a["access_token"].stringValue, encryptedRefreshToken: a["refresh_token"].stringValue, expirationDate: Date(timeIntervalSinceNow: TimeInterval(a["expires_in"].intValue)))
                    let userDefaults = UserDefaults.standard
                    let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                    
                    userDefaults.set(sessionData, forKey: "SpotifySession")
                    userDefaults.synchronize()
                    SPTAuth.defaultInstance().session = session
                    print("refresh:",SPTAuth.defaultInstance().session.encryptedRefreshToken)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "startCount"), object: self)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "sessionUpdated"), object: self)
                    
                    
            }
              return  true
            
          /*  SPTAuth.defaultInstance().handleAuthCallback(withTriggeredAuthURL: URL(string: url.absoluteString + "&redirect_uri=" + kCallbackURL +  "&grant_type=authorization_code")) { error, session in
                // This is the callback that'll be triggered when auth is completed (or fails).
                if (error != nil)  {
                    print("*** Auth error: \(error)")
                    return
                }
                else {
                    let userDefaults = UserDefaults.standard
                    let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                    
                    userDefaults.set(sessionData, forKey: "SpotifySession")
                    userDefaults.synchronize()
                    SPTAuth.defaultInstance().session = session
                    print("refresh:",SPTAuth.defaultInstance().session.encryptedRefreshToken)
                } */
              //  NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "sessionUpdated"), object: self)
           // }
          
        }
        return false
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
         SocketIOManager.sharedInstance.closeConnection()
        print("didbackground")
        
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
         SocketIOManager.sharedInstance.establishConnection()
        print("didbecome")
       
           
        
                player2.shared.updateTabBar()
            
      
        
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
       print("foreground")
       player2.shared.updateTabBar()
        if MusicPlayerManager.shared.isPlaying() {
            player2.shared.TestAhmed = true
        player2.shared.updateCurrentItemMetadata()
            player2.shared.startTimer()
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        SocketIOManager.sharedInstance.establishConnection()
        print("ResignActive")
       
        
    }
    override func remoteControlReceived(with event: UIEvent?) {
        print("jé ey")
        //if MediaPlayer.shared.isPlaying{
             MediaPlayer.shared.remoteControlReceivedWithEvent(event)
        //}else{
          //player2.shared.musicPlayerManager.remoteControlReceivedWithEvent(event)
        //   print("eventAppdelegate")
       //     MusicPlayerManager.shared.remoteControlReceivedWithEvent(event)
        //}
        
        //MediaPlayer.shared.remo
    }
    
   }

