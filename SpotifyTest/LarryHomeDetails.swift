//
//  LarryHomeDetails.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 04/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import MapleBacon
import MediaPlayer
import Spartan
class LarryHomeDetails : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Musics.arrayObject?.count != nil {
            return (Musics.arrayObject?.count)!
        }else {
            return 0
        }
    }
    let gradientLayer = CAGradientLayer()
    func animateLayer(){
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        //gradientAnimation.delegate = self
        gradientAnimation.fromValue = [0.0,0.0,0.25]
        gradientAnimation.toValue = [0.75,1.0,1.0]
        gradientAnimation.duration = 3.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        
        gradientLayer.add(gradientAnimation, forKey: nil)
        
    }
    func CreateLayer() {
        let thirdthFrame = CGRect(x: 0 , y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: 13)
        let secondFrame = CGRect(x: 0  , y: thirdthFrame.width / 2 , width: thirdthFrame.width / 2 , height: thirdthFrame.height )
        let firstUIView  = UIView(frame: thirdthFrame)
        
        gradientLayer.colors = [UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor, UIColor(red: 196 / 255 , green: 70 / 255, blue: 107 / 255, alpha: 1).cgColor,UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor ]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.locations = [0.0 , 0.75, 1.0 ]
        
        let secondLabel = UILabel(frame: secondFrame)
        secondLabel.text = "Early release"
        secondLabel.textAlignment = .center
        secondLabel.textColor = UIColor.white
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.font = UIFont.systemFont(ofSize: 12.0)
        firstUIView.addSubview(secondLabel)
        self.navigationController?.navigationBar.addSubview(firstUIView)
        gradientLayer.frame = firstUIView.frame
        firstUIView.layer.insertSublayer(gradientLayer, at: 0)
        
        let widthConstraint = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.navigationController?.navigationBar.frame.width)!)
        secondLabel.addConstraint(widthConstraint)
        let heightConstraint = NSLayoutConstraint(item: secondLabel, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: thirdthFrame.height)
        secondLabel.addConstraint(heightConstraint)
        let xConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerX, relatedBy: .equal, toItem: firstUIView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerY, relatedBy: .equal, toItem: firstUIView, attribute: .centerY, multiplier: 1, constant: 0)
        firstUIView.addConstraint(xConstraint)
        firstUIView.addConstraint(yConstraint)
        animateLayer()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playerLarry = player2.shared
        playerLarry.InterPlaylist = true
        playerLarry.Musics = self.Musics
        playerLarry.whereNowInter = indexPath.row
         if Musics[indexPath.row]["provider"].stringValue == "spotify" {
        playerLarry.SpotiyOrApple = "spot"
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // let musicPlayerController = appDelegate.musicPlayerManager.shared
        
        if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
            do {
                
                try AVAudioSession.sharedInstance().setActive(true)
            }
            catch let error {
                print(error.localizedDescription)
            }
            
           
            //self.deactivateAudioSession()
            print("sure playing local")
            playerLarry.duration = 0.0
            if playerLarry.timerApple != nil && playerLarry.timersApple != nil &&  playerLarry.timer != nil && playerLarry.timers != nil{
            playerLarry.timerApple.cancel()
           
            playerLarry.timer.cancel()
           
            playerLarry.timerApple = nil
            playerLarry.timersApple = nil
            playerLarry.timer = nil
          
            }
            if playerLarry.timers != nil {
                print("timer nil")
                playerLarry.timers.cancel()
                playerLarry.timers  = nil
                //playerLarry.timers = nil
            }
            
            // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
        }
            sleep(1)
         MusicPlayerManager.shared.musicPlayerController.stop()
        
        playerLarry.x1 = Musics[indexPath.row]["title"].stringValue
        playerLarry.x2 = Musics[indexPath.row]["artist"].stringValue
        playerLarry.x3 = UIImage(named:"default_song")!
        playerLarry.SpotifySearch = false
        //playerLarry.albumsSpotify = tracks
        playerLarry.whernowSpotify = indexPath.row
        tabBarController?.popupBar.popupItem?.progress = Float(0.0)
        //print("id:",tracks[indexPath.row].track.id as! String)
        //playerLarry.timerSpotify.cancel()
        tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
        tabBarController?.popupBar.progressViewStyle = .top
        tabBarController?.popupInteractionStyle = .drag
        tabBarController?.popupBar.backgroundStyle = .light
        tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
        _ = Spartan.getTracks(ids: [Musics[indexPath.row]["song_id"].stringValue], market: .us, success: { (track) in
            
            let a : [Track] = track
            print(a[0].popularity)
           // self.interval = Double(a[0].durationMs / 1000)
            self.tabBarController?.popupBar.popupItem?.title =  a[0].name
            var artistname = ""
            for art in a[0].artists{
                artistname = artistname + art.name + ", "
            }
            artistname.removeLast()
            artistname.removeLast()
            self.tabBarController?.popupBar.popupItem?.subtitle = artistname
            artistname = ""
            let url = URL(string: a[0].album.images[0].url)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.tabBarController?.popupBar.popupItem?.image = UIImage(data: data!)
                }
            }
            // self.tabBarController?.popupBar.popupItem?.image
            playerLarry.collection = self
            playerLarry.tt = a
            playerLarry.durationspotify = a[0].durationMs
            playerLarry.playSound(trackSpotify: a[0])
            
        }, failure: { (error) in
            print(error)
        })
         }else {
            if Musics[indexPath.row]["artist"].stringValue != "Item Not Available" {
                
                // tabBarController?.dismissPopupBar(animated: true, completion: nil)
                let playerLarry = player2.shared
                
                playerLarry.SpotiyOrApple = "apple"
                playerLarry.appleSearch = false
                if MediaPlayer.shared.isPlaying {
                    MediaPlayer.shared.pause()
                    if playerLarry.timerSpotify != nil && playerLarry.timerTemp != nil {
                    playerLarry.timerSpotify.cancel()
                    // playerLarry.timer.cancel()
                    playerLarry.timerTemp.cancel()
                    
                    playerLarry.timerSpotify = nil
                    playerLarry.timer = nil
                    playerLarry.timerTemp = nil
                    }
                    
                }
     
                tabBarController?.popupBar.popupItem?.progress = Float(0.0)
                
                
                
                playerLarry.collection = self
                tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
                playerLarry.playInterPlaylist(Musics: self.Musics, at: indexPath.row)
                
                tabBarController?.popupBar.progressViewStyle = .top
                
                tabBarController?.popupBar.backgroundStyle = .light
                tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
                
                
            }else{
                let alert = UIAlertController(title: "Item Not Available", message: "This item can't be played", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
            
        }
        
        
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        let icloud_image = cell.viewWithTag(200) as! UIImageView
        if Musics[indexPath.row]["provider"].stringValue == "apple" {
            
            title.text = Musics[indexPath.row]["title"].stringValue
            artist.text = Musics[indexPath.row]["artist"].stringValue
            
            if Musics[indexPath.row]["image"].stringValue != "" {
                imageview.setImage(withUrl: URL(string: Musics[indexPath.row]["image"].stringValue)!, placeholder: UIImage(named: "default_song"), crossFadePlaceholder: true, cacheScaled: true, completion: nil)
                
            }else {
                imageview.image = UIImage(named: "default_song")
            }
            if let item: MPMediaItem = songQuery.findSongWithPersistentIdString(persistentIDString: Musics[indexPath.row]["song_id"].stringValue) {
                if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                    imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                    
                }
                if item.assetURL == nil{
                    icloud_image.isHidden = false
                    
                    
                    
                }
            }else {
                if (self.Musics.arrayObject?.count)! > 1 {
                self.Musics.arrayObject?.remove(at: indexPath.row)
                }else {
                    self.Musics.arrayObject?.removeAll()
                }
                self.collection.reloadData()
            }
            
        }else {
            title.text = Musics[indexPath.row]["title"].stringValue
            artist.text = Musics[indexPath.row]["artist"].stringValue
            if Musics[indexPath.row]["image"].stringValue != "" {
                imageview.setImage(withUrl: URL(string: Musics[indexPath.row]["image"].stringValue)!, placeholder: UIImage(named: "default_song"), crossFadePlaceholder: true, cacheScaled: true, completion: nil)
                
            }else {
                imageview.image = UIImage(named: "default_song")
            }
            icloud_image.isHidden = true
            
            
        }
        return cell
        //let ImageSelect = cell.viewWithTag(54) as! UIImageView
       /* var verif = false
        for a in self.tableMusic {
            if a == String(indexPath.row) {
                verif = true
            }
        }
        ImageSelect.isHidden = verif == true ? false : true
        print(indexPath.row)
        print("title:",albums[indexPath.row].songTitle)
        title.text =  albums[indexPath.row].songTitle
        artist.text = albums[indexPath.row].artistName
        let songId: NSNumber = albums[indexPath.row].songId
        imageview.image = UIImage(named:"default_song")
        if local_remote {
            let item: MPMediaItem = songQuery.getItem2( songId: songId,row: indexPath.row,playlistNum: self.playlistNumX )
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                
            }
            if item.assetURL == nil{
                icloud_image.isHidden = false
                
                
                
            }
        }else{
            let item: MPMediaItem = songQuery.getItem( songId: songId )
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                
            }
        }
        
        return cell */
        
        
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        for const in cell.constraints {
            const.isActive = true
        }
    }
     var height : CGFloat!
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    var Musics : JSON = []
    var songQuery: SongQuery = SongQuery()
    var gradientLayer2: CAGradientLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
         createGradientLayer()
        Spartan.authorizationToken = SPTAuth.defaultInstance().session.accessToken
        self.collection.delegate = self
        self.collection.dataSource = self
        height = self.collection.collectionViewLayout.collectionViewContentSize.height
    }
    func createGradientLayer() {
        gradientLayer2 = CAGradientLayer()
        
        gradientLayer2.frame = self.view.bounds
        
        gradientLayer2.colors = [UIColor(red: 0, green: 249 / 255, blue: 0, alpha: 1).cgColor, UIColor(red: 1 , green: 0, blue: 1, alpha: 1).cgColor]
        
        self.view.layer.insertSublayer(gradientLayer2, at: 0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
