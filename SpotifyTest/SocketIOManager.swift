//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Ahmed Haddar on 03/10/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    //51.254.37.192
    //, config: [.log(true)]
    var socket: SocketIOClient = SocketIOClient(socketURL: URL(string: "https://larry2.herokuapp.com")! as URL)
    override init() {
        super.init()
        
    }
   
    func establishConnection() {
        print("connecting..")
  
        socket.connect()
    }
    
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func connectUsername(id:String,completionHandler : @escaping ((Bool) -> Void)){
        socket.emit("connectUser", id)
        completionHandler(true)
    }
    func search(Text : String,Spotify: String,Apple:String){
        
        socket.emit("search",["query" : Text,"spotifyToken" : Spotify,"appleToken": Apple])
    }
    
    func connectToServerWithNickname(nickname: String,userid: String) {
     
        
        self.socket.emit("subscribe",["room" :  nickname,"userid" : userid ])
        //self.socket.joinNamespace(nickname)
        
        //JoinRooms ()
       
        //les conversations (conversation) eli 3and user
        //subscribe tous les converastions
        //Lors du click get all messages mel BD
        //send_msg ChatMessage
        
        listenForOtherMessages()
    }
    func exitChatWithNickname(nickname: String, completionHandler: () -> Void) {
        socket.emit("exitUser", nickname)
        completionHandler()
    }
    
    func stopmessage(Conv:String,nickname: String) {
        socket.emit("stopTyping", Conv,nickname)
    }
    func sendMessage(idNickname: String, nickname: String, msg : String, conv : String) {
        socket.emit("chatMessage",idNickname ,nickname, msg, conv)
    }
    func unscubscribe(Conv:String,nickname: String) {
        socket.emit("unsubscribe",["room" :  nickname,"userid" : nickname ])
    }
    
    func sendStartTypingMessage(Conv:String,nickname: String) {
        print("yekteb")
        socket.emit("startTyping", Conv,nickname)
    }
    func GetResults(completionHandler: @escaping (_ messageInfo: JSON) -> Void){
    socket.on("result") { (dataArray,socketAck) -> Void in
    var results = dataArray
    let a = JSON(results)
     completionHandler(a)
    }
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: [String : AnyObject]) -> Void) {
        socket.on("newChatMessage") { (dataArray, socketAck) -> Void in
            var messageDictionary = [String: AnyObject]()
                    print("allo")
                        print(dataArray)
            
            //messageDictionary["conv"] = dataArray[5] as! Int as AnyObject?
            completionHandler(messageDictionary )
        }
    }
    private func listenForOtherMessages() {
        socket.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
          
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        
        socket.on("userExitUpdate") { (dataArray, socketAck) -> Void in
            print("Exit:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
      //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
        }
        
        socket.on("newUserIsTyping") { (dataArray, socketAck) -> Void in
            print(dataArray[0] as! String)
        print(dataArray[0] as? [String: AnyObject])
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray as? [String])
        }
        socket.on("newUserStoppedTyping") { (dataArray, socketAck) -> Void in
            print(dataArray[0] as! String)
            print(dataArray[0] as? [String: AnyObject])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: dataArray as? [String])
        }
        socket.on("userOnline") { (dataArray, socketAck) -> Void in
            print("userOnline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnectNow"), object: dataArray as? [String])
            
        }
        socket.on("userOffline") { (dataArray, socketAck) -> Void in
            print("userOffline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
        }
    }
    

}
