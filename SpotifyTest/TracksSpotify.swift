//
//  TracksSpotify.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 15/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import  UIKit
import Spartan
import MapleBacon
import AVFoundation
import AudioPlayerManager
import Alamofire
import SwiftyJSON
import MediaPlayer
class TracksSpotify : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    var userId = ""
    var playlistId = ""
    var interval : Double = 0.0
    var tracks : [PlaylistTrack] = []
    var timer : Timer?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spotifyCell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        title.text = tracks[indexPath.row].track.name
        artist.text = tracks[indexPath.row].track.artists[0].name
        if tracks[indexPath.row].track.album.images.count != 0  {
            imageview.setImage(withUrl: URL(string: tracks[indexPath.row].track.album.images[0].url)!, placeholder: UIImage(named:"default_song"), cacheScaled: true)
        }else{
            imageview.image = UIImage(named:"default_song")
        }
        return cell
    }
      override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)
        self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)], for: UIControlState.selected)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewwillAppear:")
        if tabBarController?.popupBar.popupItem?.leftBarButtonItems != nil {
            print("viewwillAppear: <> nil")
        if player2.shared.SpotiyOrApple == "spot"{
            if MediaPlayer.shared.isPlaying == false {
                tabBarController?.popupBar.popupItem?.leftBarButtonItems![1].image = UIImage(named:"play")
            }else{
                tabBarController?.popupBar.popupItem?.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
            }
        }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let playerLarry = player2.shared
       
         playerLarry.SpotiyOrApple = "spot"
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
          // let musicPlayerController = appDelegate.musicPlayerManager.shared
        
        if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
            MusicPlayerManager.shared.musicPlayerController.stop()
            print("sure playing local")
            playerLarry.duration = 0.0
            
            playerLarry.timerApple = nil
            playerLarry.timersApple = nil
            playerLarry.timer = nil
            playerLarry.timers = nil
            
            
            //self.deactivateAudioSession()
           // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
        }
       
        
        playerLarry.x1 = tracks[indexPath.row].track.name
        playerLarry.x2 = tracks[indexPath.row].track.artists[0].name
        playerLarry.x3 = UIImage(named:"default_song")!
        playerLarry.SpotifySearch = false
        playerLarry.albumsSpotify = tracks
        playerLarry.whernowSpotify = indexPath.row
        tabBarController?.popupBar.popupItem?.progress = Float(0.0)
        print("id:",tracks[indexPath.row].track.id as! String)
        //playerLarry.timerSpotify.cancel()
        tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
        tabBarController?.popupBar.progressViewStyle = .top
        tabBarController?.popupInteractionStyle = .drag
        tabBarController?.popupBar.backgroundStyle = .light
        tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
        _ = Spartan.getTracks(ids: [tracks[indexPath.row].track.id as! String], market: .us, success: { (track) in
           
            let a : [Track] = track
            print(a[0].popularity)
            self.interval = Double(a[0].durationMs / 1000)
            self.tabBarController?.popupBar.popupItem?.title =  a[0].name
            var artistname = ""
            for art in a[0].artists{
                artistname = artistname + art.name + ", "
            }
            artistname.removeLast()
            artistname.removeLast()
            self.tabBarController?.popupBar.popupItem?.subtitle = artistname
            artistname = ""
            let url = URL(string: a[0].album.images[0].url)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.tabBarController?.popupBar.popupItem?.image = UIImage(data: data!)
                }
            }
            // self.tabBarController?.popupBar.popupItem?.image
            playerLarry.collection = self
            playerLarry.tt = a
           playerLarry.durationspotify = a[0].durationMs
            playerLarry.playSound(trackSpotify: a[0])
            
        }, failure: { (error) in
            print(error)
        })
        
        
    
    }
    
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if SPTAuth.defaultInstance().session.isValid(){
        _ = Spartan.getPlaylistTracks(userId: "spotify", playlistId: playlistId, limit: 100, offset: 0, market: .us, success: { (pagingObject) in
            // Get the playlist tracks via pagingObject.items
            self.tracks = pagingObject.items
            self.collection.reloadData()
        }, failure: { (error) in
            print(error)
        })
        }else{
            
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
