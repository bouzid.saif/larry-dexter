//
//  PaddingTextField.swift
//  AhmedInterfaces
//
//  Created by Ahmed Haddar on 22/10/2017.
//  Copyright © 2017 Ahmed Haddar. All rights reserved.
//
import Foundation
import UIKit

class PaddingTextField: UITextField {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds,
                                     UIEdgeInsetsMake(0, 15, 0, 15))
    }
}
