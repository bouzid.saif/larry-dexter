//
//  MediaPlayer.swift
//  Spotify Test
//
//  Created by Wojciech on 2017-09-18.
//  Copyright © 2017 Wojtek. All rights reserved.
//

import Foundation
import Spartan
protocol MediaPlayerDelegate: class {
    func mediaPlayerDidFinishTrack()
    func mediaPlayerDidFail(error: Error)
    func mediaPlayerDidStartPlaying(track: Track)
    func mediaPlayerDidPause()
    func mediaPlayerDidChange(trackProgress: Double)
    func mediaPlayerDidResume()
}

class MediaPlayer: NSObject {
    
    static let shared = MediaPlayer()
    private override init() {}
    weak var delegate: MediaPlayerDelegate?
    private var player: SPTAudioStreamingController?
    private (set) var currentTrack: Track?
    var isPlaying: Bool {
        if let player = player,
            let state = player.playbackState {
            return state.isPlaying
        }
        return false
    }
    
    func loadAlbum(url: String, completion: @escaping (_ album: SPTAlbum?, _ error: Error?) -> Void) {
        SPTAlbum.album(withURI: URL(string: url), accessToken: SPTAuth.defaultInstance().session.accessToken, market: nil) { (error, response) in
            completion(response as? SPTAlbum, error)
        }
    }
    
    func play(track: Track) {
        print(track.uri)
        player?.playSpotifyURI(track.uri , startingWith: 0, startingWithPosition: 0, callback: { (error) in
            print("hello blla")
            if let error = error {
                print("error of course",error)
                self.delegate?.mediaPlayerDidFail(error: error)
            } else {
                print("it's okay track valid")
                self.currentTrack = track
                self.delegate?.mediaPlayerDidStartPlaying(track: track)
            }
        })
    }
    
    func seek(to progress: Float) {
        guard let current = currentTrack else {return}
        //current.durationMs
        player?.seek(to: Double(progress) * Double(current.durationMs), callback: { (error) in
            if let error = error {
                self.delegate?.mediaPlayerDidFail(error: error)
            }
        })
    }
    
    func resume() {
        player?.setIsPlaying(true, callback: { (error) in
            if let error = error {
                self.delegate?.mediaPlayerDidFail(error: error)
            } else {
                self.delegate?.mediaPlayerDidResume()
            }
        })
    }
    
    func pause() {
        player?.setIsPlaying(false, callback: { (error) in
            if let error = error {
                self.delegate?.mediaPlayerDidFail(error: error)
            } else {
                self.delegate?.mediaPlayerDidPause()
            }
        })
    }
    func stop() {
        do {
   try SPTAudioStreamingController.sharedInstance().stop()
        }catch{
            print("failed to deactivate Spotify player")
        }
    }
    func configurePlayer(authSession:SPTSession, id: String) {
        if self.player == nil {
            self.player = SPTAudioStreamingController.sharedInstance()
            self.player!.playbackDelegate = self
            self.player!.delegate = self
            if self.player!.diskCache == nil {
            self.player!.diskCache = SPTDiskCache(capacity: 1024 * 1024 * 64)
            }
            try! player?.start(withClientId: id)
            if self.player!.loggedIn == false {
            self.player!.login(withAccessToken: authSession.accessToken)
            }
        }
    }
    
}

extension MediaPlayer: SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePosition position: TimeInterval) {
        delegate?.mediaPlayerDidChange(trackProgress: position/Double(currentTrack!.durationMs))
    }
    
    func audioStreamingDidLogin(_ audioStreaming: SPTAudioStreamingController!) {
        print("logged in")
    }

    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didStopPlayingTrack trackUri: String!) {
        delegate?.mediaPlayerDidFinishTrack()
    }
    func audioStreaming(_ audioStreaming: SPTAudioStreamingController!, didChangePlaybackStatus isPlaying: Bool) {
       // delegate?.mediaPlayerDidFinishTrack()
    }
    
}
