//
//  LarryPinCode.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 30/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
class LarryPinCode : UIViewController,KOPinCodeViewDelegate {
    
    @IBOutlet weak var pinView: KOPinCodeView!
    var email = ""
    func pinDidEnterAllSymbol(_ symbolArray: [Any]!, string pin: String!) {
        print(pin)
        SwiftSpinner.show("Verifying...")
        let params: Parameters = [
            "email": email ,
            "code" : pin
        ]
        Alamofire.request("https://larry2.herokuapp.com/api/user/reset" , method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                SwiftSpinner.hide()
                let a = JSON(response.data)
                 if a["status"].boolValue {
                   
                    let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LarryChangePassword") as! LarryChangePassword
                    view.token = a["token"].stringValue
                    //view.email = self.email.text!
                    self.navigationController?.pushViewController(view, animated: true)
                    self.pinView.initPin(withCount: 6)
                 }else {
                    let alert = UIAlertController(title: "Confirmation Code", message: "Please verify your code", preferredStyle: .alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                        self.pinView.initPin(withCount: 6)
                        // self.navigationController?.popToRootViewController(animated: true)
                    }))
                    
                    // show the alert
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
        
        
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
           UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pinView.initPin(withCount: 6)
        pinView.delegate = self
        pinView.lineDeep = 2
        pinView.secure = false
        pinView.confirm = false
        pinView.symbolColor = UIColor.white
        pinView.typeKeyboard = UIKeyboardType.numberPad
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
