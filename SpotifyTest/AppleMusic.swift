//
//  AppleMusic.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 09/06/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import MediaPlayer
class AppleMusic : UIViewController {
    @objc let applicationMusicPlayer = MPMusicPlayerController.systemMusicPlayer
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let a = appleMusicCheckIfDeviceCanPlayback()
        if a != "0" || a != "-1" {
            appleMusicRequestPermission()
           
            
        }
        
    }
    @objc func appleMusicPlayTrackId(ids:[String]) {
        
        applicationMusicPlayer.setQueue(with: ids)
        if #available(iOS 10.1, *) {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                self.applicationMusicPlayer.prepareToPlay(completionHandler: {(response) in
                print(response)
                self.applicationMusicPlayer.play()
                print("saif",self.applicationMusicPlayer.nowPlayingItem?.title)
            })
            }
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    @objc func appleMusicCheckIfDeviceCanPlayback() -> String {
         var x = ""
        let serviceController = SKCloudServiceController()
        serviceController.requestCapabilities { (capability:SKCloudServiceCapability, err:Error?) in
           
            switch capability {
                
            case [] :
                
                print("The user doesn't have an Apple Music subscription available. Now would be a good time to prompt them to buy one?")
                x = "0"
                
            case SKCloudServiceCapability.musicCatalogPlayback:
                
                print("The user has an Apple Music subscription and can playback music!")
                x = "1"
            case SKCloudServiceCapability.addToCloudMusicLibrary:
                
                print("The user has an Apple Music subscription, can playback music AND can add to the Cloud Music Library")
                x = "2"
            default:
                x = "-1"
                break
                
            }
            
        }
        return x
        
    }
    @objc func appleMusicRequestPermission() {
        switch SKCloudServiceController.authorizationStatus() {
            
        case .authorized:
            
            print("The user's already authorized - we don't need to do anything more here, so we'll exit early.")
            appleMusicPlayTrackId(ids: ["6866568034157343384"])
            return
            
        case .denied:
            
            print("The user has selected 'Don't Allow' in the past - so we're going to show them a different dialog to push them through to their Settings page and change their mind, and exit the function early.")
            
            // Show an alert to guide users into the Settings
            
            return
            
        case .notDetermined:
            
            print("The user hasn't decided yet - so we'll break out of the switch and ask them.")
            break
            
        case .restricted:
            
            print("User may be restricted; for example, if the device is in Education mode, it limits external Apple Music usage. This is similar behaviour to Denied.")
            return
            
        }

        SKCloudServiceController.requestAuthorization { (status:SKCloudServiceAuthorizationStatus) in
            
            switch status {
                
            case .authorized:
                
                print("All good - the user tapped 'OK', so you're clear to move forward and start playing.")
                
            case .denied:
                
                print("The user tapped 'Don't allow'. Read on about that below...")
                
            case .notDetermined:
                
                print("The user hasn't decided or it's not clear whether they've confirmed or denied.")
                
            case .restricted:
                
                print("User may be restricted; for example, if the device is in Education mode, it limits external Apple Music usage. This is similar behaviour to Denied.")
                
            }
            
        }
        
    }
    
}
