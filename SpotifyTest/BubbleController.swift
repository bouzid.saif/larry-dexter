//
//  BubbleController.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 15/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
class BubbleController : UIViewController {
    @IBOutlet weak var magneticView: MagneticView!{
        didSet {
            magnetic.magneticDelegate = self
            
            #if DEBUG
                magneticView.showsFPS = true
                magneticView.showsDrawCount = true
                magneticView.showsQuadCount = true
            #endif
        }
    }
    var magnetic: Magnetic {
    
        return magneticView.magnetic
    }
     func add(_ sender: Int) {
        let name = UIImage.names[sender]
        let color = UIColor.colors[sender]
        let image = UIImage.imageName[sender]
        let node = ImageNode(text: name.capitalized, image: image, color: color, radius: 40)
        node.name = name
        node.image = image
        magnetic.addChild(node)
        
        // Image Node: image displayed by default
        // let node = ImageNode(text: name.capitalized, image: UIImage(named: name), color: color, radius: 40)
        // magnetic.addChild(node)
    }
    func add2(_ sender: Int) {
        let name = UIImage.names2[sender]
        let color = UIColor.colors[sender]
         let image = UIImage.imageName2[sender]
        let node = ImageNode(text: name.capitalized, image: image, color: color, radius: 40)
        node.name = name
        node.image = image
        magnetic.addChild(node)
        
        // Image Node: image displayed by default
        // let node = ImageNode(text: name.capitalized, image: UIImage(named: name), color: color, radius: 40)
        // magnetic.addChild(node)
    }
    func add3(_ sender: Int) {
        let name = UIImage.names3[sender]
        let color = UIColor.colors[sender]
         let image = UIImage.imageName3[sender]
        let node = ImageNode(text: name.capitalized, image: image, color: color, radius: 40)
        node.name = name
         node.image = image
        magnetic.addChild(node)
        
        // Image Node: image displayed by default
        // let node = ImageNode(text: name.capitalized, image: UIImage(named: name), color: color, radius: 40)
        // magnetic.addChild(node)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for  i in 0..<5 {
            add(i)
        }
    }
    @IBAction func reset(reverse:Bool = false) {
        let speed = magnetic.physicsWorld.speed
        magnetic.physicsWorld.speed = 0
        let sortedNodes = magnetic.children.flatMap { $0 as? Node }.sorted { node, nextNode in
            let distance = node.position.distance(from: magnetic.magneticField.position)
            let nextDistance = nextNode.position.distance(from: magnetic.magneticField.position)
            return distance < nextDistance && node.isSelected
        }
        var actions = [SKAction]()
        for (index, node) in sortedNodes.enumerated() {
            node.physicsBody = nil
            let action = SKAction.run { [unowned magnetic, unowned node] in
                if node.isSelected {
                    if reverse {
                    let point = CGPoint(x: magnetic.size.width / 2 , y: magnetic.size.height + 40)
                    
                    let movingXAction = SKAction.moveTo(x: point.x, duration: 0.2)
                    let movingYAction = SKAction.moveTo(y: -point.y, duration: 0.4)
                    let resize = SKAction.scale(to: 0.3, duration: 0.4)
                    let throwAction = SKAction.group([movingXAction, movingYAction, resize])
                    node.run(throwAction) { [unowned node] in
                        node.removeFromParent()
                    }
                    }else {
                        let point = CGPoint(x: magnetic.size.width / 2, y: magnetic.size.height + 40)
                        
                        let movingXAction = SKAction.moveTo(x: point.x, duration: 0.2)
                        let movingYAction = SKAction.moveTo(y: point.y, duration: 0.4)
                        let resize = SKAction.scale(to: 0.3, duration: 0.4)
                        let throwAction = SKAction.group([movingXAction, movingYAction, resize])
                        node.run(throwAction) { [unowned node] in
                            node.removeFromParent()
                        }
                    }
                } else {
                    node.removeFromParent()
                }
            }
            actions.append(action)
            let delay = SKAction.wait(forDuration: TimeInterval(index) * 0.002)
            actions.append(delay)
        }
        magnetic.run(.sequence(actions)) { [unowned magnetic] in
            magnetic.physicsWorld.speed = speed
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension BubbleController: MagneticDelegate {
    
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        print("didSelect -> \(node)")
        if node.name == "Library" {
            let point = CGPoint(x: magnetic.size.width / 2 , y: magnetic.size.height / 2)
            let movingAction = SKAction.move(to: point, duration: 0.2)
            let scaleAction = SKAction.scale(by: 3, duration: 0.3)
            let testAction = SKAction.fadeOut(withDuration: 1)
           // let destructionString = Bundle.main.path(forResource: "magic", ofType: "sks")
            //let destruction = NSKeyedUnarchiver.unarchiveObject(withFile: destructionString!) as? SKEmitterNode
            //destruction?.position = node.position
            //magnetic.addChild(destruction!)

            let throwAction = SKAction.group([movingAction,scaleAction,testAction])
            node.run(throwAction) { [unowned node] in
             node.removeFromParent()
                self.reset(reverse: false)
                for  i in 0..<7 {
                    self.add2(i)
                }
               /* destruction?.run(SKAction.sequence([SKAction.wait(forDuration: 0.1), SKAction.scale(by: 1.5, duration: 0.1), SKAction.run({() -> Void in
                    destruction?.particleBirthRate = 0
                    
                })])) */
            }
           /*  */
            
              
            
                    
                
            
        
                    
      
            
    
           // magnetic.
        }else if node.name == "Back" {
            let point = CGPoint(x: magnetic.size.width / 2 , y: magnetic.size.height / 2)
            let movingAction = SKAction.move(to: point, duration: 0.2)
            let scaleAction = SKAction.scale(by: 3, duration: 0.3)
            let testAction = SKAction.fadeOut(withDuration: 1)
            let throwAction = SKAction.group([movingAction,scaleAction,testAction])
            node.run(throwAction) { [unowned node] in
                node.removeFromParent()
            self.reset(reverse: true)
            for  i in 0..<5 {
                self.add(i)
                
            }
            }
        }else if node.name == "Settings" {
            let point = CGPoint(x: magnetic.size.width / 2 , y: magnetic.size.height / 2)
            let movingAction = SKAction.move(to: point, duration: 0.2)
            let scaleAction = SKAction.scale(by: 3, duration: 0.3)
            let testAction = SKAction.fadeOut(withDuration: 1)
            let throwAction = SKAction.group([movingAction,scaleAction,testAction])
            node.run(throwAction) { [unowned node] in
                node.removeFromParent()
            self.reset(reverse: true)
            for  i in 0..<8 {
                self.add3(i)
                
            }
            }
        }
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        print("didDeselect -> \(node)")
    }
    
}
class ImageNode: Node {
    override var image: UIImage? {
        didSet {
            sprite.texture = image.map { SKTexture(image: $0) }
            
        }
    }
    override func selectedAnimation() {}
    override func deselectedAnimation() {}
}
extension UIImage {
    
    static let names: [String] = ["Library", "Settings", "Search", "Labs", "Social"]
      static let names2: [String] = ["Songs", "Playlists", "Playlists \n     +", "Artists", "Platforms","Genres","Back"]
     static let names3: [String] = ["Friends Connected", "F.A.Q", "My Contacts", "Equalizing", "Privacy Policy","Terms and conditions","Go Premium","Back"]
    static let imageName: [UIImage] = [UIImage(named: "audiobook")!,UIImage(named: "settings")!,UIImage(named: "search")!,UIImage(named: "atom")!,UIImage(named: "megaphone")!]
    static let imageName2: [UIImage] = [UIImage(named: "folder")!,UIImage(named: "playlist")!,UIImage(named: "playlist add")!,UIImage(named: "artists")!,UIImage(named: "icloud_ali2")!,UIImage(named: "genre")!,UIImage(named: "back")!]
    static let imageName3: [UIImage] = [UIImage(named: "friends")!,UIImage(named: "faq")!,UIImage(named: "contacts")!,UIImage(named: "equalizing")!,UIImage(named: "privacy")!,UIImage(named: "terms")!,UIImage(named: "premium")!,UIImage(named: "back")!]
}
extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: 1)
    }
    
    static var red: UIColor {
        return UIColor(red: 255, green: 59, blue: 48)
    }
    
    static var orange: UIColor {
        return UIColor(red: 255, green: 149, blue: 0)
    }
    
    static var yellow: UIColor {
        return UIColor(red: 255, green: 204, blue: 0)
    }
    
    static var green: UIColor {
        return UIColor(red: 76, green: 217, blue: 100)
    }
    
    static var tealBlue: UIColor {
        return UIColor(red: 90, green: 200, blue: 250)
    }
    
    static var blue: UIColor {
        return UIColor(red: 0, green: 122, blue: 255)
    }
    
    static var purple: UIColor {
        return UIColor(red: 88, green: 86, blue: 214)
    }
    
    static var pink: UIColor {
        return UIColor(red: 255, green: 45, blue: 85)
    }
    
    static let colors: [UIColor] = [.red, .orange, .yellow, .green, .tealBlue, .blue, .purple, .pink]
    
}
extension Array {
    
    func randomItem() -> Element {
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
    
}
