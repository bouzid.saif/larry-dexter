//
//  ChooseFromSpotify.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 02/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Spartan

class ChooseFromSpotify : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    var userId = ""
    var playlistId = ""
    var tableMusic : [String] = [""]
    let gradientLayer = CAGradientLayer()
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var collection: DynamicCollectionView!
    var tracks : [PlaylistTrack] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collection.delegate = self
        self.collection.dataSource = self
        self.collection.allowsMultipleSelection = true
        if SPTAuth.defaultInstance().session.isValid(){
            _ = Spartan.getPlaylistTracks(userId: "spotify", playlistId: playlistId, limit: 100, offset: 0, market: .us, success: { (pagingObject) in
                // Get the playlist tracks via pagingObject.items
                self.tracks = pagingObject.items
                
                if StaticPlaylist.sharedInstance.MusicPlaylist != nil {
                for q in StaticPlaylist.sharedInstance.MusicPlaylist{
                    if q.provider == "spotify" {
                        for i in 0 ... self.tracks.count - 1 {
                            if q.song_id == (self.tracks[i].track.id as! String) {
                               // self.collection.delegate?.collectionView!(self.collection, didSelectItemAt: IndexPath(row: i, section: 0))
                                self.tableMusic.append(String(i))
                            }
                        }
                    }
                }
                }
                     self.collection.reloadData()
          
            }, failure: { (error) in
                print(error)
            })
        }else{
        
        }
            
    }
    func animateLayer(){
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        //gradientAnimation.delegate = self
        gradientAnimation.fromValue = [0.0,0.0,0.25]
        gradientAnimation.toValue = [0.75,1.0,1.0]
        gradientAnimation.duration = 3.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        
        gradientLayer.add(gradientAnimation, forKey: nil)
        
    }
    func CreateLayer() {
        let thirdthFrame = CGRect(x: 0 , y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: 13)
        let secondFrame = CGRect(x: 0  , y: thirdthFrame.width / 2 , width: thirdthFrame.width / 2 , height: thirdthFrame.height )
        let firstUIView  = UIView(frame: thirdthFrame)
        
        gradientLayer.colors = [UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor, UIColor(red: 196 / 255 , green: 70 / 255, blue: 107 / 255, alpha: 1).cgColor,UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor ]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.locations = [0.0 , 0.75, 1.0 ]
        
        let secondLabel = UILabel(frame: secondFrame)
        secondLabel.text = "Early release"
        secondLabel.textAlignment = .center
        secondLabel.textColor = UIColor.white
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.font = UIFont.systemFont(ofSize: 12.0)
        firstUIView.addSubview(secondLabel)
        self.navigationController?.navigationBar.addSubview(firstUIView)
        gradientLayer.frame = firstUIView.frame
        firstUIView.layer.insertSublayer(gradientLayer, at: 0)
        
        let widthConstraint = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.navigationController?.navigationBar.frame.width)!)
        secondLabel.addConstraint(widthConstraint)
        let heightConstraint = NSLayoutConstraint(item: secondLabel, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: thirdthFrame.height)
        secondLabel.addConstraint(heightConstraint)
        let xConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerX, relatedBy: .equal, toItem: firstUIView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerY, relatedBy: .equal, toItem: firstUIView, attribute: .centerY, multiplier: 1, constant: 0)
        firstUIView.addConstraint(xConstraint)
        firstUIView.addConstraint(yConstraint)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     animateLayer()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? BaseCollectionViewCell
        //if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
        if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
        (cell?.viewWithTag(54) as! UIImageView).isHidden = false
        var verif = false
        for a in self.tableMusic {
            if a == String(indexPath.row) {
                verif = true
            }
        }
        if verif == false {
            self.tableMusic.append(String(indexPath.row))
            StaticPlaylist.sharedInstance.addToMusic(music: PlaylistMusicSaif(id: self.tracks[indexPath.row].track.id as! String, title: self.tracks[indexPath.row].track.name!, artist: self.tracks[indexPath.row].track.artists[0].name!, image: self.tracks[indexPath.row].track.album.images[0].url!, provider: "spotify"))
        }
        }else {
            //self.collection.delegate?.collectionView!(self.collection, didDeselectItemAt: indexPath)
            (cell?.viewWithTag(54) as! UIImageView).isHidden = true
            //self.collection.reloadItems(at: [indexPath])
            var i = -1
            for a in self.tableMusic {
                i = i + 1
                if a == String(indexPath.row) {
                    
                    self.tableMusic.remove(at: i)
                    StaticPlaylist.sharedInstance.removerMusic(music: PlaylistMusicSaif(id: self.tracks[indexPath.row].track.id as! String, title: self.tracks[indexPath.row].track.name!, artist: self.tracks[indexPath.row].track.artists[0].name!, image: self.tracks[indexPath.row].track.album.images[0].url!, provider: "spotify"))
                    i = i - 1
                }
                
            }
            self.collection.deselectItem(at: indexPath, animated: false)
        }
        //self.collection.reloadItems(at: [indexPath])
       // }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
      let cell = collectionView.cellForItem(at: indexPath) as? BaseCollectionViewCell
       // if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
        (cell?.viewWithTag(54) as! UIImageView).isHidden = true
        //self.collection.reloadItems(at: [indexPath])
        var i = -1
        for a in self.tableMusic {
              i = i + 1
            if a == String(indexPath.row) {
             
                self.tableMusic.remove(at: i)
                 StaticPlaylist.sharedInstance.removerMusic(music: PlaylistMusicSaif(id: self.tracks[indexPath.row].track.id as! String, title: self.tracks[indexPath.row].track.name!, artist: self.tracks[indexPath.row].track.artists[0].name!, image: self.tracks[indexPath.row].track.album.images[0].url!, provider: "spotify"))
                i = i - 1
            }
          
        }
      /*  }else {
            (cell?.viewWithTag(54) as! UIImageView).isHidden = false
            var verif = false
            for a in self.tableMusic {
                if a == String(indexPath.row) {
                    verif = true
                }
            }
            if verif == false {
                self.tableMusic.append(String(indexPath.row))
                StaticPlaylist.sharedInstance.addToMusic(music: PlaylistMusicSaif(id: self.tracks[indexPath.row].track.id as! String, title: self.tracks[indexPath.row].track.name!, artist: self.tracks[indexPath.row].track.artists[0].name!, image: self.tracks[indexPath.row].track.album.images[0].url!, provider: "spotify"))
            }
        } */
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spotifyCell", for: indexPath) as? BaseCollectionViewCell {
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        let ImageSelect = cell.viewWithTag(54) as! UIImageView
           
            var verif = false
            for a in self.tableMusic {
                if a == String(indexPath.row) {
                    verif = true
                }
            }
            ImageSelect.isHidden = verif == true ? false : true
        title.text = tracks[indexPath.row].track.name
        artist.text = tracks[indexPath.row].track.artists[0].name
        if tracks[indexPath.row].track.album.images.count != 0  {
            imageview.setImage(withUrl: URL(string: tracks[indexPath.row].track.album.images[0].url)!, placeholder: UIImage(named:"default_song"), cacheScaled: true)
        }else{
            imageview.image = UIImage(named:"default_song")
        }
        return cell
    }else {
    return UICollectionViewCell()
    }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   /* func loadSpotify() {
        Spartan.authorizationToken = SPTAuth.defaultInstance().session.accessToken
        Spartan.loggingEnabled = true
        _ = Spartan.getMe(success: { (user) in
            // Do something with the user object
            //self.userr = user.id as! String
            var c = [JSON]()
            _ = Spartan.getUsersPlaylists(userId: user.id as! String, limit: 50, offset: 0, success: { (pagingObject) in
                // Get the playlists via pagingObject.playlists
                self.spotifyPlaylist =   pagingObject.items
                var tempPlaylist = ""
                var temp = ""
                var j  = 0
                let group = DispatchGroup()
                for playlist in self.spotifyPlaylist {
                    group.enter()
                    
                    print("1")
                    print(temp)
                    if SPTAuth.defaultInstance().session.isValid(){
                        _ = Spartan.getPlaylistTracks(userId: "spotify", playlistId: playlist.id as! String, limit: 100, offset: 0, market: .us, success: { (pagingObject) in
                            // Get the playlist tracks via pagingObject.items
                            if j == 4 {
                                temp = "["
                            }else {
                                temp  = ""
                            }
                            self.tracks = pagingObject.items
                            
                            var i = 0
                            for bb in self.tracks {
                                var x = ""
                                //   if i < 4 {
                                x = " { \"title\" : \"\(bb.track.name!)\", "
                                x = x + " \"artist\" : \"\(bb.track.artists[0].name!)\" ,"
                                x = x + " \"image\" : \"\(bb.track.album.images[0].url!)\","
                                x = x + "\"song_id\" : \"\(bb.track.id as! String)\" ,"
                                x = x + "\"provider\" : \"Spotify\" }"
                                // x = x.replacingOccurrences(of: "&", with: "U+0026")
                                //  x = x.replacingOccurrences(of: "-", with: "U+002D")
                                //temp = temp + x
                                
                                /*     }else if i == 4 {
                                 x = " { \"title\" : \"\(bb.track.name!)\", "
                                 x = x + " \"artist\" : \"\(bb.track.artists[0].name!)\" ,"
                                 x = x + " \"image\" : \"\(bb.track.album.images[0].url!)\","
                                 x = x + "\"song_id\" : \"\(bb.track.id as! String)\" ,"
                                 x = x + "\"provider\" : \"Spotify\" } "
                                 // x = x.replacingOccurrences(of: "&", with: "U+0026")
                                 // x = x.replacingOccurrences(of: "-", with: "U+002D")
                                 temp = temp + x
                                 } */
                                
                                // i = i + 1
                                let p = JSON.parse(x)
                                c.append(p)
                                print("c::::",c)
                            }
                            //tempPlaylist =  temp
                            group.leave()
                        }, failure: { (error) in
                            print(error)
                        })
                    }else{
                        
                    }
                    if j == 4 {
                        tempPlaylist = tempPlaylist + "]"
                    }
                    j = j + 1
                    
                }
                group.notify(queue: DispatchQueue.main, execute: {
                    // tempPlaylist = "[" + tempPlaylist + " ]"
                    //c = JSON.parse("[" + tempPlaylist + "]")
                    // print("SpotifyMusic:",c)
                    // let data = tempPlaylist.data(using: .utf8, allowLossyConversion: true)
                    //print("SpotifyMusic:",tempPlaylist)
                    /*if let data = tempPlaylist.data(using: String.Encoding.utf8) {
                     if let x = try? JSON(data: data) {
                     
                     }
                     } */
                    DispatchQueue.main.async(execute: {
                        // print("SpotifyMusic2:",JSON.parse(tempPlaylist))
                    })
                    
                    let aa = "[ { \"title\" : \"Far From Home\",  \"artist\" : \"Swanky Tunes\" , \"image\" : \"https://i.scdn.co/image/9412fd2b9361022606fbf3d37047b734b0ca85e1\",\"song_id\" : \"6o51FfrewhAsdZgsqa8OfY\" ,\"provider\" : \"Spotify\" }, { \"title\" : \"Us\",  \"artist\" : \"Kaskade\" , \"image\" : \"https://i.scdn.co/image/c646e1915f97121832f5ac975272790e103ddde7\",\"song_id\" : \"6RXdg3NwyLU5pHUwkZq6VA\" ,\"provider\" : \"Spotify\" }, { \"title\" : \"More Than You Know\",  \"artist\" : \"Axwell / Ingrosso\" , \"image\" : \"https://i.scdn.co/image/2d6804b4b59ed8329cebe9fba23969e28c3084c1\",\"song_id\" : \"3slBHvoQcO8HjscbP2QKk5\" ,\"provider\" : \"Spotify\" }, { \"title\" : \"Under Control\",  \"artist\" : \"Calvin Harris\" , \"image\" : \"https://i.scdn.co/image/35f57037a887e9b35037bd82eb23c84a69a28ab4\",\"song_id\" : \"4J7CKHCF3mdL4diUsmW8lq\" ,\"provider\" : \"Spotify\" } , { \"title\" : \"Desire - Gryffin Remix\",  \"artist\" : \"Years & Years\" , \"image\" : \"https://i.scdn.co/image/cb0d20bf72dec2850f41927b68be5e57222bc794\",\"song_id\" : \"6JzCO4ZzxAXhY86vTcqM1Q\" ,\"provider\" : \"Spotify\" } ]"
                    // print("test:",JSON.parse(aa))
                    // c = JSON.init(parseJSON : "[" + tempPlaylist + "]")
                    
                    
                })
                /* dispatch_group_notify(group, group) {
                 collectionView.reload()
                 } */
                
                
                //self.collection.reloadData()
            }, failure: { (error) in
                print(error)
            })
        }, failure: { (error) in
            print(error)
        })
        
    } */
}
