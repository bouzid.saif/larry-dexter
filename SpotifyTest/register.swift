//
//  register.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 24/09/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
class register : UIViewController {
    @IBOutlet weak var firstname: UITextField!
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.isNavigationBarHidden = false
     
       // self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        // self.navigationController?.navigationBar.shadowImage = UIImage()
    
     //   self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor:UIColor.white]
        //self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = UIColor.white
    //self.navigationController?.view.backgroundColor = UIColor.black
            
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    @IBAction func register(_ sender: Any) {
        SwiftSpinner.show("Checking Username...")
        if username.text != "" {
            let params: Parameters = [
                "username": username.text! ,
                
                //"first_name" : firstname.text! ,
                // "last_name" : lastname.text!
            ]
            Alamofire.request("https://larry2.herokuapp.com/api/user/exist" , method: .post, parameters: params, encoding: JSONEncoding.default)
                .responseJSON { response in
                  SwiftSpinner.hide()
                    let bb = JSON(response.data)
                    if bb["status"].boolValue {
                        if bb["exist"].boolValue {
                            let alert = UIAlertController(title: "Register", message: "this username is taken", preferredStyle: .alert)
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                            
                            // show the alert
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        }else {
                            let register2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "register2") as! Register2
                            register2.username = self.username.text!
                            self.navigationController?.pushViewController(register2, animated: true)
                        }
                    }
            }
            
        }
     /*   SwiftSpinner.show("Loading...")
        if firstname.text != "" && lastname.text != "" && email.text != "" && username.text != "" && password.text != "" {
            let params: Parameters = [
                "user_name": username.text! ,
                "password": password.text! ,
                "email" : email.text!
                //"first_name" : firstname.text! ,
               // "last_name" : lastname.text!
            ]
            Alamofire.request("https://larry2.herokuapp.com/api/user/register" , method: .post, parameters: params, encoding: JSONEncoding.default)
                .responseJSON { response in
                    let a = JSON(response.data)
                    if a["status"].stringValue == "false"{
                        /* let player = self.storyboard!.instantiateViewController(withIdentifier: "player2") as! player2
                         self.navigationController!.pushViewController(player, animated: true) */
                        
                        SwiftSpinner.hide()
                        
                    }else if a["status"].stringValue == "true"{
                        SwiftSpinner.hide()
                      self.navigationController?.popViewController(animated: true)
                    }
                    /*
                     self.comments =  self.comments.merged(other: json1)
                     self.tableview.beginUpdates()
                     self.tableview.insertRows(at: [IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0)], with: .automatic)
                     self.verif = "yes"
                     self.tableview.endUpdates()
                     self.tableview.reloadData()
                     self.tableview.setNeedsLayout()
                     self.tableview.scrollToRow(at: IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                     */
                    
                   
                    
            }
        }else{
        SwiftSpinner.hide()
            let alert = UIAlertController(title: "Register", message: "please fill all the champs", preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            
            // show the alert
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
        } */
}
}
