//
//  AllMusic.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 11/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import UIKit
import LNPopupController
import Spartan
import MapleBacon
import AZSearchView
import SwiftyJSON
import AVFoundation
class AllMusic: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate {
    @IBOutlet weak var search: UISearchBar!
    
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var progressView: UIProgressView!
    var height : CGFloat!
     @objc var songs : [String] = ["Thunder","Something Just Like This","What About Else"]
    @objc var artists : [String] = ["Imagine Dragons","Imagine Dragons","Pink"]
    let accessibilityDateComponentsFormatter = DateComponentsFormatter()
    var timer : Timer?
    var spotifyPlaylist : [SimplifiedPlaylist] = []
    var userr = ""
    var searchController: AZSearchViewController!
    var resultArray:[String] = []
    var result : JSONX = []
    var resultsImage: [String] = []
     var interval : Double = 0.0
    
    @IBAction func SearchNow(_ sender: UIBarButtonItem) {
         searchController.show(in: self)
    }
    ////search
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let q  = searchBar.text
        searchController.show(in: self)
        
        //print(q)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
       
    }
    
    
    /////
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return spotifyPlaylist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        
        title.text = spotifyPlaylist[indexPath.row].name
        artist.text = String(spotifyPlaylist[indexPath.row].tracksObject.total) + " titles"
        print("spotify playlist :",spotifyPlaylist[indexPath.row].images.count)
        if spotifyPlaylist[indexPath.row].images.count != 0  {
            imageview.setImage(withUrl: URL(string: spotifyPlaylist[indexPath.row].images[0].url)!, placeholder: UIImage(named:"default_song"), cacheScaled: true)
        }else{
        imageview.image = UIImage(named:"default_song")
        }
        
        
        //cell.bounds.size.height = (title.bounds.size.height + artist.bounds.size.height + imageview.bounds.size.height) + 15
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AllMusic", for: indexPath)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let Tracksspoti = self.storyboard?.instantiateViewController(withIdentifier: "TracksSpotify") as! TracksSpotify
            Tracksspoti.userId = self.userr
        print((spotifyPlaylist[indexPath.row].id) as! String)
            Tracksspoti.playlistId = (spotifyPlaylist[indexPath.row].id) as! String
            self.navigationController?.pushViewController(Tracksspoti, animated: true)
            //  let appdelegate = UIApplication.shared.delegate as! AppDelegate
        /*
            tabBarController?.presentPopupBar(withContentViewController: player, animated: true, completion: nil)
            tabBarController?.popupBar.popupItem?.title = songs[indexPath.row]
            tabBarController?.popupBar.popupItem?.subtitle = artists[indexPath.row]
        tabBarController?.popupBar.popupItem?.image = UIImage(named: "default_song")
         tabBarController?.popupBar.progressViewStyle = .top
        
         tabBarController?.popupBar.backgroundStyle = .light
        tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
        
        //tabBarController?.popupBar.progressViewColor  = UIColor.red
         tabBarController?.popupBar.popupItem?.progress = Float(0.5)
        player.SpotiyOrApple = "spot"
        player.playSound(id: indexPath.row, wher: "begin") */
           // tabBarController?.popupBar.tintColor = UIColor(white: 38.0 / 255.0, alpha: 1.0)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)
        self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)], for: UIControlState.selected)
        self.collection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.searchController = AZSearchViewController()
        self.searchController.delegate = self
        self.searchController.dataSource = self
        self.searchController.statusBarStyle = .lightContent
        self.searchController.keyboardAppearnce = .dark
        self.searchController.searchBarPlaceHolder = "Search For Songs"
        
        
        self.searchController.navigationBarClosure = { bar in
            //The navigation bar's background color
            bar.barTintColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            
            //The tint color of the navigation bar
            bar.tintColor = UIColor.lightGray
        }
        let item = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(AllMusic.close(sender:)))
        item.tintColor = .white
        self.searchController.navigationItem.rightBarButtonItem = item
        //UITabBar.appearance().tintColor = UIColor.white
       // self.tabBarController?.tabBar.items![0].selectedImage.
        self.tabBarController?.tabBar.tintColor = UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)
       self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)], for: UIControlState.selected)

        collection.delegate = self
        collection.dataSource = self
        Spartan.authorizationToken = SPTAuth.defaultInstance().session.accessToken
        Spartan.loggingEnabled = true
        _ = Spartan.getMe(success: { (user) in
            // Do something with the user object
            self.userr = user.id as! String
            _ = Spartan.getUsersPlaylists(userId: user.id as! String, limit: 20, offset: 0, success: { (pagingObject) in
                // Get the playlists via pagingObject.playlists
                self.spotifyPlaylist =   pagingObject.items
                
                self.collection.reloadData()
            }, failure: { (error) in
                print(error)
            })
        }, failure: { (error) in
            print(error)
        })
        
        height = self.collection.collectionViewLayout.collectionViewContentSize.height

        if #available(iOS 10.0, *) {
          //  flowlayout.estimatedItemSize =  CGSize(width: 96, height: 133)
            
           // flowlayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        } else {
            // Fallback on earlier versions
        }
        
        // Do any additional setup after loading the view.
    }
    @objc func close(sender:AnyObject?){
        searchController.searchBar.text = ""
         self.result = []
        searchController.reloadData()
        searchController.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AllMusic: AZSearchViewDelegate{
    
    func searchView(_ searchView: AZSearchViewController, didSearchForText text: String) {
        searchView.dismiss(animated: false, completion: nil)
    }
    
    func searchView(_ searchView: AZSearchViewController, didTextChangeTo text: String, textLength: Int) {
        self.result = []
        if text != "" {
        SocketIOManager.sharedInstance.search(Text: text, Spotify: SPTAuth.defaultInstance().session.accessToken, Apple: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldRN1ZOWTNRTDMifQ.eyJpYXQiOjE1MTIzOTkwMzEsImV4cCI6MTUxMzk3NjczMSwiaXNzIjoiNzk4OVk1REY1QyJ9.zIvQrDZ_fVSr4tuTKvrscwAs6bdXU8NmrW_PLzZ6k3tjOntphzlDz2yWX9Feuiw3yuQaafNBs8Qs8X4Aw7MdAw")
        SocketIOManager.sharedInstance.GetResults{ (results) -> Void in
            self.result = []
            self.result = JSONX.parse(results.arrayValue[0].description)
            print("Results0:",results.arrayValue)
            //self.result = JSON(results["results"].arrayObject)
            print("***************")
            print("results:",self.result)
            print("***************")
            searchView.reloadData()
        }
        }
        
        if text == ""{
            self.result = []
            searchView.reloadData()
        }
        
    }
    
    func searchView(_ searchView: AZSearchViewController, didSelectResultAt index: Int, text: String) {
        searchView.searchBar.text = ""
        if self.result[index]["type"].stringValue == "apple"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "apple"
            
            if MediaPlayer.shared.isPlaying {
                MediaPlayer.shared.pause()
                playerLarry.timerSpotify.cancel()
                // playerLarry.timer.cancel()
                playerLarry.timerTemp.cancel()
                
                playerLarry.timerSpotify = nil
                playerLarry.timer = nil
                playerLarry.timerTemp = nil
                
            }
           
                
                print("localMusicStopped")
                playerLarry.playlistApple = 0
                playerLarry.SongApple = 0
                playerLarry.local_remote = true
            playerLarry.appleSearch = true
            
            let app = SongInfo(albumTitle: self.result[index]["album"].stringValue, artistName: self.result[index]["artistName"].stringValue, songTitle: self.result[index]["songName"].stringValue, songId: NSNumber(value:Int(self.result[index]["id"].stringValue)!))
            playerLarry.albums = [app]
            playerLarry.whernow = IndexPath(row: 0, section: 0).row
            tabBarController?.popupBar.popupItem?.progress = Float(0.0)
            
            
            
            playerLarry.collection = self
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            //playerLarry.playLocal(album:[app],at: IndexPath(row: 0, section: 0).row)
            playerLarry.playAppleSearch(result: self.result,index: index)
            tabBarController?.popupBar.progressViewStyle = .top
        
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
            self.result = []
            self.searchController.reloadData()
        }else if self.result[index]["type"].stringValue == "spotify"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "spot"
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            // let musicPlayerController = appDelegate.musicPlayerManager.shared
            
            if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
                MusicPlayerManager.shared.musicPlayerController.stop()
                print("sure playing local")
                playerLarry.duration = 0.0
               
                playerLarry.timerApple = nil
                playerLarry.timer = nil
                
                
                self.deactivateAudioSession()
                // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
            }
            
             playerLarry.SpotifySearch = true
           
            //playerLarry.timerSpotify.cancel()
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            tabBarController?.popupBar.progressViewStyle = .top
            tabBarController?.popupInteractionStyle = .drag
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
            _ = Spartan.getTracks(ids: [ self.result[index]["id"].stringValue], market: .us, success: { (track) in
                
                let a : [Track] = track
            
                playerLarry.x1 = a[0].name
                playerLarry.x2 = a[0].artists[0].name
                playerLarry.x3 = UIImage(named:"default_song")!
                
                //playerLarry.albumsSpotify = tracks
                playerLarry.whernowSpotify = index
                self.tabBarController?.popupBar.popupItem?.progress = Float(0.0)
               // print("id:",tracks[indexPath.row].track.id as! String)
                //playerLarry.timerSpotify.cancel()
                self.tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
                self.tabBarController?.popupBar.progressViewStyle = .top
                self.tabBarController?.popupInteractionStyle = .drag
                self.tabBarController?.popupBar.backgroundStyle = .light
                self.tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.tabBarController?.popupBar.popupItem?.title =  a[0].name
                var artistname = ""
                for art in a[0].artists{
                    artistname = artistname + art.name + ", "
                }
                artistname.removeLast()
                artistname.removeLast()
                self.tabBarController?.popupBar.popupItem?.subtitle = artistname
                artistname = ""
                let url = URL(string: a[0].album.images[0].url)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.tabBarController?.popupBar.popupItem?.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                playerLarry.collection = self
                playerLarry.tt = a
                playerLarry.durationspotify = a[0].durationMs
                playerLarry.TrackSpotifySearch = a[0]
                playerLarry.playSound(trackSpotify: a[0])
                
            }, failure: { (error) in
                print(error)
            })
            self.result = []
            self.searchController.reloadData()
        }
        
        searchView.dismiss(animated: true, completion: {
            // self.pushWithTitle(text: text)
        })
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
}
extension AllMusic: AZSearchViewDataSource{
    
    
    
    func results() -> JSONX {
        if searchController.searchBar.text != ""{
        return self.result
        }else{
            self.result = []
            return self.result
        }
    }
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchView.cellIdentifier)
        cell?.textLabel?.text = self.result[indexPath.row]["songName"].stringValue + " - " + self.result[indexPath.row]["artistName"].stringValue
        if self.result[indexPath.row]["type"].stringValue == "apple" {
            cell?.imageView?.image = #imageLiteral(resourceName: "apple_music")
        }else if self.result[indexPath.row]["type"].stringValue == "spotify" {
            cell?.imageView?.image = #imageLiteral(resourceName: "Spotify-icon")
        }
        // cell?.imageView?.image = #imageLiteral(resourceName: "ic_history").withRenderingMode(.alwaysTemplate)
        //cell?.imageView?.tintColor = UIColor.gray
        cell?.contentView.backgroundColor = .white
        return cell!
    }
    
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
}

