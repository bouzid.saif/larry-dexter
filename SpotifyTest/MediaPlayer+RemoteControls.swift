//
//  MediaPlayer+RemoteControls.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 26/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit

extension MediaPlayer {
    
    @objc public func remoteControlReceivedWithEvent(_ event: UIEvent?) {
        if let _event = event {
            switch _event.subtype {
            case UIEventSubtype.remoteControlPlay:
                player2.shared.remoteControlReceived(with: event)
            case UIEventSubtype.remoteControlPause:
                player2.shared.remoteControlReceived(with: event)
            case UIEventSubtype.remoteControlNextTrack:
              player2.shared.remoteControlReceived(with: event)
            case UIEventSubtype.remoteControlPreviousTrack:
               player2.shared.remoteControlReceived(with: event)
            case UIEventSubtype.remoteControlTogglePlayPause:
               player2.shared.remoteControlReceived(with: event)
            default:
                break
            }
        }
    }
}
