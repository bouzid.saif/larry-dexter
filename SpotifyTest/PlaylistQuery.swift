//
//  PlaylistQuery.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 06/11/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import Foundation
import MediaPlayer

struct PlaylistInfo {
    
    var albumTitle: String
    var seedItem : [MPMediaItem]
    var botTitle: String
    var songId   :  NSNumber
}



class PlaylistQuery {
    
    func get(songCategory: String,collection : DynamicCollectionView,with:Bool) -> [PlaylistInfo] {
        
        var albums: [AlbumInfo] = []
        let albumsQuery: MPMediaQuery
          albumsQuery = MPMediaQuery.playlists()
        
        
        // let albumsQuery: MPMediaQuery = MPMediaQuery.albums()
        let albumItems = albumsQuery.collections!
        //  var album: MPMediaItemCollection
       
         var songs: [PlaylistInfo] = []
        for album in albumItems {
            
           // let albumItems: [MPMediaItem] = album.items as [MPMediaItem]
            // var song: MPMediaItem
            
         
            let songInfo: PlaylistInfo = PlaylistInfo(
                albumTitle: album.value( forProperty: MPMediaPlaylistPropertyName ) as! String,
                seedItem : album.value(forProperty: MPMediaPlaylistPropertySeedItems) as! [MPMediaItem],
                botTitle: album.value(forProperty: MPMediaPlaylistPropertyAuthorDisplayName) as! String,
                songId:     album.value( forProperty: MPMediaPlaylistPropertyPersistentID ) as! NSNumber
            )
            
            songs.append(songInfo)
           
            
           
        }
        if with {
        let playlistLocal : PlaylistInfo = PlaylistInfo(albumTitle: "LocalMusic", seedItem:[MPMediaItem()], botTitle: "Apple", songId:(UInt64()) as! NSNumber)
        songs.append(playlistLocal)
        }
        //collection.reloadData()
        //let a = Array(AllSongs.prefix(upTo: 10))
        
        print(songs)
        return songs
        
    }
    
    func getItem( songId: NSNumber,row:Int ) -> MPMediaItemArtwork {
        
        let property: MPMediaPropertyPredicate = MPMediaPropertyPredicate( value: songId, forProperty: MPMediaItemPropertyPersistentID )
        
        let query: MPMediaQuery = MPMediaQuery()
        
        query.addFilterPredicate( property )
        
        var items = MPMediaQuery.playlists().collections?[row]
        
        if let  x = items?.representativeItem?.artwork {
            return (items?.representativeItem?.artwork)!
        }else{
            return MPMediaItemArtwork(image: UIImage(named: "default_song")!)
        }
        
    }
    
}
