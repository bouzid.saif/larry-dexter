//
//  AppleRemotePlaylists.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 06/11/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import LNPopupController
import MediaPlayer
import AudioPlayerManager
import StoreKit
import AZSearchView
import MapleBacon
import SwiftyJSON
import AVFoundation
import Spartan
class AppleRemotePlaylists: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var progressView: UIProgressView!
    var height : CGFloat!
    var i = -1
    let accessibilityDateComponentsFormatter = DateComponentsFormatter()
    var timer : Timer?
    var albums: [SongInfo] = []
    var Playlists: [PlaylistInfo] = []
    var songQuery: SongQuery = SongQuery()
    var playlistQuery : PlaylistQuery = PlaylistQuery()
    var interval : Double = 0.0
    var appleMusicManager = AppleMusicManager()
    
    lazy var authorizationManager: AuthorizationManager = {
        return AuthorizationManager(appleMusicManager: self.appleMusicManager)
    }()
    
     var authorizationDataSource: AuthorizationDataSource!
    static let userTokenUserDefaultsKey = "UserTokenUserDefaultsKey"
    static let authorizationDidUpdateNotification = Notification.Name("authorizationDidUpdateNotification")
    static let cloudServiceDidUpdateNotification = Notification.Name("cloudServiceDidUpdateNotification")
      var didPresentCloudServiceSetup = false
    var searchController: AZSearchViewController!
    var resultArray:[String] = []
    var result : JSONX = []
    var resultsImage: [String] = []
    
    @IBAction func SearchNow(_ sender: UIBarButtonItem) {
        searchController.show(in: self)
    }
    //////Search
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let q  = searchBar.text
        searchController.show(in: self)
        //print(q)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
    }
    
    
    //////
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Playlists.count != 0{
            return Playlists.count
        }else{
            return 0
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if  Playlists.count != 0 {
            return 1
        }else{
            return 0
        }
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "musicCell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
       // let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        print(indexPath.row)
        print("title:",Playlists[indexPath.row].albumTitle)
        title.text =  Playlists[indexPath.row].albumTitle
       // artist.text = ""
        let songId: NSNumber = Playlists[indexPath.row].songId
        
        if Playlists[indexPath.row].albumTitle != "LocalMusic"{
        imageview.image = UIImage(named:"default_song")
        if   let item: MPMediaItemArtwork = playlistQuery.getItem( songId: songId ,row: indexPath.row) {
        
            imageview.image = item.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
            print("image:",imageview.image?.size)
            if imageview.image == nil {
                 imageview.image = UIImage(named:"default_song")
            }
            
        }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        let cell = collectionView.cellForItem(at: indexPath)
        let title = cell?.viewWithTag(2) as! UILabel
        // tabBarController?.dismissPopupBar(animated: true, completion: nil)
        let Tracks = self.storyboard?.instantiateViewController(withIdentifier: "LocalMusic") as! LocalMusic
        Tracks.NavigationTitle = title.text!
        if title.text != "LocalMusic"{
        Tracks.local_remote = true
           
        }else{
            print("localMusic")
            Tracks.local_remote = false
        }
        Tracks.playlistNumX = indexPath.row
       // Tracksspoti.userId = self.userr
        //print((spotifyPlaylist[indexPath.row].id) as! String)
       // Tracksspoti.playlistId = (spotifyPlaylist[indexPath.row].id) as! String
        self.navigationController?.pushViewController(Tracks, animated: true)
        
        
        // tabBarController?.popupBar.tintColor = UIColor(white: 38.0 / 255.0, alpha: 1.0)
    }
    @objc func _timerTicked(_ timer: Timer) {
        tabBarController?.popupBar.popupItem?.progress += 0.002
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor.magenta
        if self.tabBarController?.tabBar.items?.count == 3 {
        self.tabBarController?.tabBar.items![1].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
            ], for: UIControlState.selected)
        i = -1
        }else {
            self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
                ], for: UIControlState.selected)
            i = -1
        }
        // self.collection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchController = AZSearchViewController()
        self.searchController.delegate = self
        self.searchController.dataSource = self
        self.searchController.statusBarStyle = .lightContent
        self.searchController.keyboardAppearnce = .dark
        self.searchController.searchBarPlaceHolder = "Search For Songs"
        
        
        self.searchController.navigationBarClosure = { bar in
            //The navigation bar's background color
            bar.barTintColor = #colorLiteral(red: 0.9019607843, green: 0.2235294118, blue: 0.4, alpha: 1)
            
            //The tint color of the navigation bar
            bar.tintColor = UIColor.lightGray
        }
        let item = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(AppleRemotePlaylists.close(sender:)))
        item.tintColor = .white
        self.searchController.navigationItem.rightBarButtonItem = item
        self.tabBarController?.tabBar.tintColor = UIColor.magenta
          if self.tabBarController?.tabBar.items?.count == 4 {
        self.tabBarController?.tabBar.items![2].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
            ], for: UIControlState.selected)
          }else {
            self.tabBarController?.tabBar.items![1].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
                ], for: UIControlState.selected)
        }
        collection.delegate = self
        collection.dataSource = self
     
        height = self.collection.collectionViewLayout.collectionViewContentSize.height
         authorizationDataSource = AuthorizationDataSource(authorizationManager: authorizationManager)
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthorizationManager.cloudServiceDidUpdateNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: AuthorizationManager.authorizationDidUpdateNotification,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(handleAuthorizationManagerDidUpdateNotification),
                                       name: .UIApplicationWillEnterForeground,
                                       object: nil)
        let cloudServiceCapabilities = authorizationManager.cloudServiceCapabilities
        
        if cloudServiceCapabilities.contains(.musicCatalogPlayback) {
           
        } else {
           
        }
      
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                self.Playlists = self.playlistQuery.get(songCategory: "Playlists", collection: self.collection, with: true)
                // self.albums = self.songQuery.get(songCategory: "Playlists",collection : self.collection)
                DispatchQueue.main.async {
                     self.collection.reloadData()
                }
                
            } else {
                self.displayMediaLibraryError()
            }
        }
        self.title = "AppleMusic"
    }
    @objc func close(sender:AnyObject?){
        searchController.searchBar.text = ""
        self.result = []
        searchController.reloadData()
        searchController.dismiss(animated: true)
    }
    @objc func handleAuthorizationManagerDidUpdateNotification() {
        DispatchQueue.main.async {
            if SKCloudServiceController.authorizationStatus() == .notDetermined || MPMediaLibrary.authorizationStatus() == .notDetermined {
                //self.navigationItem.rightBarButtonItem?.isEnabled = true
            } else {
               // self.navigationItem.rightBarButtonItem?.isEnabled = false
                
                if #available(iOS 10.1, *) {
                    if self.authorizationManager.cloudServiceCapabilities.contains(.musicCatalogSubscriptionEligible) &&
                        !self.authorizationManager.cloudServiceCapabilities.contains(.musicCatalogPlayback) {
                        self.presentCloudServiceSetup()
                    }
                } else {
                    // Fallback on earlier versions
                }
                
            }
            
           
        }
    }
    func presentCloudServiceSetup() {
        
        guard didPresentCloudServiceSetup == false else {
            return
        }
        
        /*
         If the current `SKCloudServiceCapability` includes `.musicCatalogSubscriptionEligible`, this means that the currently signed in iTunes Store
         account is elgible for an Apple Music Trial Subscription.  To provide the user with an option to sign up for a free trial, your application
         can present the `SKCloudServiceSetupViewController` as demonstrated below.
         */
        
        if #available(iOS 10.1, *) {
            let cloudServiceSetupViewController = SKCloudServiceSetupViewController()
       
        cloudServiceSetupViewController.delegate = self
        
        cloudServiceSetupViewController.load(options: [.action: SKCloudServiceSetupAction.subscribe]) { [weak self] (result, error) in
            guard error == nil else {
                fatalError("An Error occurred: \(error!.localizedDescription)")
            }
            
            if result {
                self?.present(cloudServiceSetupViewController, animated: true, completion: nil)
                self?.didPresentCloudServiceSetup = true
            }
        }
        } else {
            // Fallback on earlier versions
        }
    }

   

    
    
    func displayMediaLibraryError() {
        
        var error: String
        switch MPMediaLibrary.authorizationStatus() {
        case .restricted:
            error = "Media library access restricted by corporate or parental settings"
        case .denied:
            error = "Media library access denied by user"
        default:
            error = "Unknown error"
        }
        
        let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        controller.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension AppleRemotePlaylists: SKCloudServiceSetupViewControllerDelegate {
    @available(iOS 10.1, *)
    func cloudServiceSetupViewControllerDidDismiss(_ cloudServiceSetupViewController: SKCloudServiceSetupViewController) {
        
    }
}
extension AppleRemotePlaylists: AZSearchViewDelegate{
    
    func searchView(_ searchView: AZSearchViewController, didSearchForText text: String) {
        searchView.dismiss(animated: false, completion: nil)
    }
    
    func searchView(_ searchView: AZSearchViewController, didTextChangeTo text: String, textLength: Int) {
        self.result = []
        if text != "" {
            SocketIOManager.sharedInstance.search(Text: text, Spotify: SPTAuth.defaultInstance().session.accessToken, Apple: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldRN1ZOWTNRTDMifQ.eyJpYXQiOjE1MTIzOTkwMzEsImV4cCI6MTUxMzk3NjczMSwiaXNzIjoiNzk4OVk1REY1QyJ9.zIvQrDZ_fVSr4tuTKvrscwAs6bdXU8NmrW_PLzZ6k3tjOntphzlDz2yWX9Feuiw3yuQaafNBs8Qs8X4Aw7MdAw")
            SocketIOManager.sharedInstance.GetResults{ (results) -> Void in
                self.result = []
                self.result = JSONX.parse(results.arrayValue[0].description)
                print("Results0:",results.arrayValue)
                //self.result = JSON(results["results"].arrayObject)
                print("***************")
                print("results:",self.result)
                print("***************")
                searchView.reloadData()
            }
        }
        
        if text == ""{
            self.result = []
            searchView.reloadData()
        }
        
    }
    
    func searchView(_ searchView: AZSearchViewController, didSelectResultAt index: Int, text: String) {
        searchView.searchBar.text = ""
        if self.result[index]["type"].stringValue == "apple"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "apple"
            
            if MediaPlayer.shared.isPlaying {
                MediaPlayer.shared.pause()
                playerLarry.timerSpotify.cancel()
                // playerLarry.timer.cancel()
                playerLarry.timerTemp.cancel()
                
                playerLarry.timerSpotify = nil
                playerLarry.timer = nil
                playerLarry.timerTemp = nil
                
            }
            
            
            print("localMusicStopped")
            playerLarry.playlistApple = 0
            playerLarry.SongApple = 0
            playerLarry.local_remote = true
            playerLarry.appleSearch = true
            
            let app = SongInfo(albumTitle: self.result[index]["album"].stringValue, artistName: self.result[index]["artistName"].stringValue, songTitle: self.result[index]["songName"].stringValue, songId: NSNumber(value:Int(self.result[index]["id"].stringValue)!))
            playerLarry.albums = [app]
            playerLarry.whernow = IndexPath(row: 0, section: 0).row
            tabBarController?.popupBar.popupItem?.progress = Float(0.0)
            
            
            
            playerLarry.collection = self
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            //playerLarry.playLocal(album:[app],at: IndexPath(row: 0, section: 0).row)
            playerLarry.playAppleSearch(result: self.result,index: index)
            tabBarController?.popupBar.progressViewStyle = .top
            
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
            self.result = []
            self.searchController.reloadData()
        }else if self.result[index]["type"].stringValue == "spotify"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "spot"
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            // let musicPlayerController = appDelegate.musicPlayerManager.shared
            
            if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
                MusicPlayerManager.shared.musicPlayerController.stop()
                print("sure playing local")
                playerLarry.duration = 0.0
                
                playerLarry.timerApple = nil
                playerLarry.timer = nil
                
                
                self.deactivateAudioSession()
                // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
            }
            
            playerLarry.SpotifySearch = true
            
            //playerLarry.timerSpotify.cancel()
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            tabBarController?.popupBar.progressViewStyle = .top
            tabBarController?.popupInteractionStyle = .drag
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
            _ = Spartan.getTracks(ids: [ self.result[index]["id"].stringValue], market: .us, success: { (track) in
                
                let a : [Track] = track
                
                playerLarry.x1 = a[0].name
                playerLarry.x2 = a[0].artists[0].name
                playerLarry.x3 = UIImage(named:"default_song")!
                
                //playerLarry.albumsSpotify = tracks
                playerLarry.whernowSpotify = index
                self.tabBarController?.popupBar.popupItem?.progress = Float(0.0)
                // print("id:",tracks[indexPath.row].track.id as! String)
                //playerLarry.timerSpotify.cancel()
                self.tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
                self.tabBarController?.popupBar.progressViewStyle = .top
                self.tabBarController?.popupInteractionStyle = .drag
                self.tabBarController?.popupBar.backgroundStyle = .light
                self.tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.tabBarController?.popupBar.popupItem?.title =  a[0].name
                var artistname = ""
                for art in a[0].artists{
                    artistname = artistname + art.name + ", "
                }
                artistname.removeLast()
                artistname.removeLast()
                self.tabBarController?.popupBar.popupItem?.subtitle = artistname
                artistname = ""
                let url = URL(string: a[0].album.images[0].url)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.tabBarController?.popupBar.popupItem?.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                playerLarry.collection = self
                playerLarry.tt = a
                playerLarry.durationspotify = a[0].durationMs
                playerLarry.TrackSpotifySearch = a[0]
                playerLarry.playSound(trackSpotify: a[0])
                
            }, failure: { (error) in
                print(error)
            })
            self.result = []
            self.searchController.reloadData()
        }
        
        searchView.dismiss(animated: true, completion: {
            // self.pushWithTitle(text: text)
        })
    }
 
}
extension AppleRemotePlaylists: AZSearchViewDataSource{
    
    
    
    func results() -> JSONX {
        if searchController.searchBar.text != ""{
            return self.result
        }else{
            self.result = []
            return self.result
        }
    }
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchView.cellIdentifier)
        cell?.textLabel?.text = self.result[indexPath.row]["songName"].stringValue + " - " + self.result[indexPath.row]["artistName"].stringValue
        if self.result[indexPath.row]["type"].stringValue == "apple" {
            cell?.imageView?.image = #imageLiteral(resourceName: "apple_music")
        }else if self.result[indexPath.row]["type"].stringValue == "spotify" {
            cell?.imageView?.image = #imageLiteral(resourceName: "Spotify-icon")
        }
        // cell?.imageView?.image = #imageLiteral(resourceName: "ic_history").withRenderingMode(.alwaysTemplate)
        //cell?.imageView?.tintColor = UIColor.gray
        cell?.contentView.backgroundColor = .white
        return cell!
    }
    
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
}

