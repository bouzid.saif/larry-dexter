//
//  LarryProfil.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 30/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//
import UIKit
import SwiftyJSON
import AVFoundation
class LarryProfil: UIViewController {
    
    @IBOutlet weak var profil_nom: UILabel!
    @IBOutlet weak var profil_image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let abc = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = abc.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var accc = JSON(data: dataFromString!)
        profil_nom.text = accc["first_name"].stringValue + " " +  accc["last_name"].stringValue
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logout(_ sender: Any) {
        self.deactivateAudioSession()
        let login = self.storyboard?.instantiateViewController(withIdentifier: "loginn") as! loginn
        // let presentaion = UIPresentationController(presentedViewController: self, presenting: login)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        //login.view.window!.layer.add(transition, forKey: kCATransition)
        //let navigation = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(login, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor.blue
        if self.tabBarController?.tabBar.items?.count == 4 {
            self.tabBarController?.tabBar.items![3].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.blue
                ], for: UIControlState.selected)
        }else {
            self.tabBarController?.tabBar.items![2].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.blue
                ], for: UIControlState.selected)
        }
    }
    func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        UIApplication.shared.endReceivingRemoteControlEvents()
        
    }
    @IBAction func connectSpoty(_ sender: Any) {
    }
    
}


