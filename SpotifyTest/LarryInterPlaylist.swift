//
//  LarryInterPlaylist.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 31/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
import Spartan
import MediaPlayer
class LarryInterPlaylist : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,ImagePickerDelegate {
    let gradientLayer = CAGradientLayer()
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count != 0 {
            self.ImagePlaylist = images[0]
        }
        print(self.ImagePlaylist?.size)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        if images.count != 0 {
        self.ImagePlaylist = images[0]
        }
        print(self.ImagePlaylist?.size)
        imagePicker.dismiss(animated: true, completion: nil)
         self.uploadImageToserver()
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == spotify_Collection {
            if self.spotifyPlaylist.count != 0 {
                return self.spotifyPlaylist.count
            }else {
                return 0
            }
        }else {
            if Playlists.count != 0{
                return Playlists.count
            }else{
                return 0
            }
        }
    }
    var test = 0
    func loadSpotify() {
        Spartan.authorizationToken = SPTAuth.defaultInstance().session.accessToken
        Spartan.loggingEnabled = true
        _ = Spartan.getMe(success: { (user) in
            // Do something with the user object
            self.userr = user.id as! String
             var c = [JSON]()
            _ = Spartan.getUsersPlaylists(userId: user.id as! String, limit: 50, offset: 0, success: { (pagingObject) in
                // Get the playlists via pagingObject.playlists
                self.spotifyPlaylist =   pagingObject.items
                self.spotify_Collection.isHidden = false
               
               SwiftSpinner.hide()
                self.spotify_Collection.reloadData()
        }, failure: { (error) in
            print(error)
        })
        }, failure:{ (error) in
            print(error)
        } )
        
    }
    func animateLayer(){
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        //gradientAnimation.delegate = self
        gradientAnimation.fromValue = [0.0,0.0,0.25]
        gradientAnimation.toValue = [0.75,1.0,1.0]
        gradientAnimation.duration = 3.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        
        gradientLayer.add(gradientAnimation, forKey: nil)
        
    }
    func CreateLayer() {
        let thirdthFrame = CGRect(x: 0 , y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: 13)
        let secondFrame = CGRect(x: 0  , y: thirdthFrame.width / 2 , width: thirdthFrame.width / 2 , height: thirdthFrame.height )
        let firstUIView  = UIView(frame: thirdthFrame)
        
        gradientLayer.colors = [UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor, UIColor(red: 196 / 255 , green: 70 / 255, blue: 107 / 255, alpha: 1).cgColor,UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor ]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.locations = [0.0 , 0.75, 1.0 ]
        
        let secondLabel = UILabel(frame: secondFrame)
        secondLabel.text = "Early release"
        secondLabel.textAlignment = .center
        secondLabel.textColor = UIColor.white
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.font = UIFont.systemFont(ofSize: 12.0)
        firstUIView.addSubview(secondLabel)
        self.navigationController?.navigationBar.addSubview(firstUIView)
        gradientLayer.frame = firstUIView.frame
        firstUIView.layer.insertSublayer(gradientLayer, at: 0)
        
        let widthConstraint = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.navigationController?.navigationBar.frame.width)!)
        secondLabel.addConstraint(widthConstraint)
        let heightConstraint = NSLayoutConstraint(item: secondLabel, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: thirdthFrame.height)
        secondLabel.addConstraint(heightConstraint)
        let xConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerX, relatedBy: .equal, toItem: firstUIView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerY, relatedBy: .equal, toItem: firstUIView, attribute: .centerY, multiplier: 1, constant: 0)
        firstUIView.addConstraint(xConstraint)
        firstUIView.addConstraint(yConstraint)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          animateLayer()
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == spotify_Collection {
        let Tracksspoti = self.storyboard?.instantiateViewController(withIdentifier: "ChooseFromSpotify") as! ChooseFromSpotify
        Tracksspoti.userId = self.userr
        print((spotifyPlaylist[indexPath.row].id) as! String)
        Tracksspoti.playlistId = (spotifyPlaylist[indexPath.row].id) as! String
        self.navigationController?.pushViewController(Tracksspoti, animated: true)
        }else {
            let cell = Apple_Collection.cellForItem(at: indexPath)
            let title = cell?.viewWithTag(2) as! UILabel
            // tabBarController?.dismissPopupBar(animated: true, completion: nil)
            let Tracks = self.storyboard?.instantiateViewController(withIdentifier: "ChooseFromApple") as! ChooseFromApple
            Tracks.NavigationTitle = title.text!
            if title.text != "LocalMusic"{
                Tracks.local_remote = true
                
            }else{
                print("localMusic")
                Tracks.local_remote = false
            }
            Tracks.playlistNumX = indexPath.row
            // Tracksspoti.userId = self.userr
            //print((spotifyPlaylist[indexPath.row].id) as! String)
            // Tracksspoti.playlistId = (spotifyPlaylist[indexPath.row].id) as! String
            self.navigationController?.pushViewController(Tracks, animated: true)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == spotify_Collection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            let title = cell.viewWithTag(2) as! UILabel
            let  artist = cell.viewWithTag(3) as! UILabel
            let imageview = cell.viewWithTag(1) as! UIImageView
            title.text = self.spotifyPlaylist[indexPath.row].name!
            artist.text = String(self.spotifyPlaylist[indexPath.row].tracksObject.total) + " titles"
            if spotifyPlaylist[indexPath.row].images.count != 0  {
                imageview.setImage(withUrl: URL(string: spotifyPlaylist[indexPath.row].images[0].url)!, placeholder: UIImage(named:"default_song"), cacheScaled: true)
            }else{
                imageview.image = UIImage(named:"default_song")
            }
                return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)
            let title = cell.viewWithTag(2) as! UILabel
            let  artist = cell.viewWithTag(3) as! UILabel
            let imageview = cell.viewWithTag(1) as! UIImageView
            
            title.text =  Playlists[indexPath.row].albumTitle
             //artist.text = Playlists[indexPath.row]
            let songId: NSNumber = Playlists[indexPath.row].songId
            
            if Playlists[indexPath.row].albumTitle != "LocalMusic"{
                imageview.image = UIImage(named:"default_song")
                if   let item: MPMediaItemArtwork = playlistQuery.getItem( songId: songId ,row: indexPath.row) {
                    imageview.image = item.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                    
                }
            }
            
                return cell
        }
    
    }
    func loadAppleMusic() {
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                self.Playlists = self.playlistQuery.get(songCategory: "Playlists", collection: self.Apple_Collection,with:false)
                // self.albums = self.songQuery.get(songCategory: "Playlists",collection : self.collection)
                DispatchQueue.main.async(execute: {
                     self.Apple_Collection.reloadData()
                })
                
                
                
            } else {
                self.displayMediaLibraryError()
            }
        }
    }
    func displayMediaLibraryError() {
    
    var error: String
    switch MPMediaLibrary.authorizationStatus() {
    case .restricted:
    error = "Media library access restricted by corporate or parental settings"
    case .denied:
    error = "Media library access denied by user"
    default:
    error = "Unknown error"
    }
    
    let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
    controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    controller.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
    if #available(iOS 10.0, *) {
    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
    } else {
    // Fallback on earlier versions
    }
    }))
    present(controller, animated: true, completion: nil)
    }
    
    @IBOutlet weak var spotify_Collection: DynamicCollectionView!
    var spotify_songs : JSON = []
    var apple_songs : JSON = []
    var userr = ""
    var NamePlaylist = ""
    var spotifyPlaylist : [SimplifiedPlaylist] = []
    var Playlists: [PlaylistInfo] = []
    var playlistQuery : PlaylistQuery = PlaylistQuery()
    var ImagePlaylist : UIImage? = nil
     var tracks : [PlaylistTrack] = []
    var alert = UIAlertController(title: "New Playlist", message: "Enter a name for your playlist", preferredStyle:
        UIAlertControllerStyle.alert)
    var urlImage = "https://larry2.herokuapp.com/uploads/"
    @IBOutlet weak var segementation: UISegmentedControl!
    @IBOutlet weak var Apple_Collection: DynamicCollectionView!
    func sendData() {
        DispatchQueue.main.async(execute: {
            
              SwiftSpinner.show("Creating the Playlist...")
        })
      
        let abcd = UserDefaults.standard.value(forKey: "Token") as! String
        let abc = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = abc.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var accc = JSON(data: dataFromString!)
        let header: HTTPHeaders = [
            "Authorization" : "Bearer " + abcd
        ]
        var y = "["
        for a in StaticPlaylist.sharedInstance.MusicPlaylist {
            var x = "{\"title\" : "
            x = x + "\"\(String(describing: a.title!))\","
            x = x + "\"artist\" : "
            x = x + "\"\(String(describing: a.artist!))\","
            x = x + "\"image\" : "
            x = x + "\"\(String(describing: a.image!))\","
            x = x + "\"song_id\" : "
            x = x + "\"\(String(describing: a.song_id!))\","
            x = x + "\"provider\" : "
            x = x + "\"\(String(describing: a.provider!))\"},"
            print(JSON.parse(x))
            y = y + x
            //try? self.spotify_songs.merge(with: JSON.parse(x))
            // self.spotify_songs.arrayValue.append(JSON.parse(x))
        }
        
        y = String(y.dropLast()) + "]"
        self.spotify_songs = JSON.parse(y)
 
        print("object:",self.spotify_songs)
        var photoServer = ""
        if self.urlImage != "https://larry2.herokuapp.com/uploads/" {
            photoServer =  self.urlImage
        }else {
            photoServer = ""
        }
        
        let params : Parameters = [
            "name" : "\(NamePlaylist)" ,
            "user" : "\"\(accc["_id"].stringValue)\"",
            "image" : "\(photoServer)" ,
            "songs" : self.spotify_songs.rawValue
            
        ]
        Alamofire.request("https://larry2.herokuapp.com/api/playlist" , method: .post,  parameters: params, encoding: JSONEncoding.default,headers : header)
            .responseString{  response in
                print(response)
                SwiftSpinner.hide()
                StaticPlaylist.sharedInstance.MusicPlaylist.removeAll()
                let q = JSON(response.data)
                print(q)
                self.navigationController?.popViewController(animated: true)
                /*  self.playlists = q
                 if self.playlists.arrayObject?.count != nil {
                 if self.playlists.arrayObject!.count > 0 {
                 self.collection.isHidden = false
                 self.youdont.isHidden = true
                 self.collection.reloadData()
                 }
                 } */
                
        }
        
    }
    @objc func handleSpinner ()
    {
        if (test == 2)
        {
            SwiftSpinner.hide()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        spotify_Collection.delegate = self
        spotify_Collection.dataSource = self
        Apple_Collection.delegate = self
        Apple_Collection.dataSource = self
        
           self.tabBarController?.tabBar.isHidden = true
        SwiftSpinner.show("Loading Playlists...")
        loadSpotify()
        loadAppleMusic()
        
    }
    
    @IBAction func SelectCollection(_ sender: Any) {
        if segementation.selectedSegmentIndex == 0 {
            self.spotify_Collection.isHidden = false
            self.Apple_Collection.isHidden = true
        }else {
            self.spotify_Collection.isHidden = true
            self.Apple_Collection.isHidden = false
        }
    }
    @objc func textfieldDidchange(TextField : UITextField) {
        if TextField.text != "" {
            self.NamePlaylist = TextField.text!
            self.alert.actions[1].isEnabled = true
        }else {
            self.alert.actions[1].isEnabled = false
        }
    }
    func uploadImageToserver(){
        print("kissou")
        
        //SwiftSpinner.show("Uploading Images...")
        //LoaderAlert.shared.show(withStatus: "Uploading Photo...")
        SwiftSpinner.show("Uploading Photo...")
        //file
        let myUrl = NSURL(string: "https://larry2.herokuapp.com/api/upload")
        
        
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        
        
        let param = [
            "name"  : "Anonymous"
        ]
       
        
        print("foisx")
        
        //print(image)
        let boundary = self.generateBoundaryString()
       /* let ab = UserDefaults.standard.value(forKey: "User") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!) */
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        // request.setValue(a["value"].stringValue, forHTTPHeaderField: "X-Auth-token")
        //let temp = image!.resizeWith(percentage: 0.1)
        
        let imageData: NSData = UIImageJPEGRepresentation(ImagePlaylist!.scaleImage(toSize: CGSize(width: 1024, height: 1024))!, 1) as! NSData
        
        if(imageData==nil)  { return }
        
        request.httpBody = self.createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData as NSData, boundary: boundary) as Data
        
        
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print("JSON:",json as Any )
                let j = JSON(json)
                print("*$***3*******$****************")
                print(j)
                 SwiftSpinner.hide()
                if j["status"].boolValue {
                    self.urlImage = self.urlImage + j["name"].stringValue
                    self.sendData()
                }else {
                   
                    let alertNow  = UIAlertController(title: "Upload photo", message: "An error has occurred, please retry", preferredStyle:
                        UIAlertControllerStyle.alert)
                    let actiongg =  UIAlertAction(title: "Cancel", style:  UIAlertActionStyle.cancel , handler:{ (UIAlertAction)in
                        self.sendData()
                        
                    }
                    )
                    
                    let actiong =  UIAlertAction(title: "Retry", style:  UIAlertActionStyle.destructive , handler:{ (UIAlertAction)in
                        self.uploadImageToserver()
                        
                    }
                    )
                    alertNow.addAction(actiongg)
                    alertNow.addAction(actiong)
                    self.present(alertNow, animated: true, completion: nil)
                    
                }
                
            }catch
            {
                print("errrorsaifPhoto")
                print(error)
            }
            
        }
        
        task.resume()
    }
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func CreatePlaylist(_ sender: Any) {
       
       // let action = UIAlertAction(title: "Name Input", style: .default) { (alertAction) in
         //   let textField = alert.textFields![0] as UITextField
            
       // }
        self.alert = UIAlertController(title: "New Playlist", message: "Enter a name for your playlist", preferredStyle:
            UIAlertControllerStyle.alert)
        self.alert.addTextField { (textField) in
            textField.placeholder = "Playlist name"
            textField.delegate = self
            textField.addTarget(self, action: #selector(self.textfieldDidchange(TextField:)), for: .editingChanged)
           // alert.actions[0].isEnabled = false
            
        }
        let action7 =  UIAlertAction(title: "NO", style:  UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            self.sendData()
            
        }
        )
        let action6 = UIAlertAction(title: "YES", style:  UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            let imagePickerController = ImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.imageLimit = 1
            
            self.present(imagePickerController, animated: true, completion: {
                
                
            })
            
            
        })
        let alertx = UIAlertController(title: "New Playlist", message: "Do you want to pick a cover for your new playlist? ", preferredStyle:
            UIAlertControllerStyle.alert)
        
        alertx.addAction(action7)
        alertx.addAction(action6)
        let action2 = UIAlertAction(title: "Create", style: .default , handler:{ (UIAlertAction)in
            
            
            self.present(alertx, animated: true, completion:nil)
            
            
        })
       // let action4 = UIAlertAction(title: "Photo", style: .destructive , handler:{ (UIAlertAction)in
            
        
      //  })
        let action =  UIAlertAction(title: "Cancel", style:  UIAlertActionStyle.cancel)
        self.alert.addAction(action)
        //self.alert.addAction(action4)
            self.alert.addAction(action2)
        self.alert.actions[1].isEnabled = false
       // print("count:",self.alert.actions.count)
        //self.present(alert, animated:true, completion: nil)
       // alert.addTextField(configurationHandler: textFieldHandler)
        
       // alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            
            
            
       // }))
        if StaticPlaylist.sharedInstance.MusicPlaylist.count != 0 {
       self.present(self.alert, animated: true, completion:nil)
        }else {
            let alert2 = UIAlertController(title: "New Playlist", message: "Please Choose some songs before", preferredStyle:
                UIAlertControllerStyle.alert)
              let action3 =  UIAlertAction(title: "OK", style:  UIAlertActionStyle.default)
            
            alert2.addAction(action3)
            self.present(alert2, animated: true, completion:nil)
        }
        
    }
    func textFieldHandler(textField: UITextField!)
    {
        if (textField) != nil {
            textField.placeholder = "Playlist name"
            
        }
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

