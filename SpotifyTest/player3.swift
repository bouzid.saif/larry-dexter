//
//  player3.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 12/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import KDEAudioPlayer
class player3 : UIViewController,AudioPlayerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let delegate: AudioPlayerDelegate = self
        let player = AudioPlayer()
        player.delegate = delegate
        let UR = URL(fileURLWithPath: "http://www.noiseaddicts.com/samples_1w72b820/2514.mp3")
       guard let URL = Bundle.main.url(forResource: "Something Just Like This", withExtension: "mp3") else { return }
    
        let item = AudioItem(highQualitySoundURL: UR)
        player.play(item: item!)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
