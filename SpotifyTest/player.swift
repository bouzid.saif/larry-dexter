//
//  player.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 10/07/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    func setRadius(radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2;
       // self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
       // self.layer.borderColor = UIColor.white.cgColor
        self.clipsToBounds = true
       // self.layer.masksToBounds = true;
    }
}
extension UIView {
    func setRadius1(radius: CGFloat? = nil) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2;
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
}
class player : UIViewController, InteractivePlayerViewDelegate {
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var player: InteractivePlayerView!
    private var timer: Timer!
    @IBOutlet weak var timeLabel: UILabel!
    @objc var duration : Float = 0
    var delegate: InteractivePlayerViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.player!.delegate = self
         //let url = Bundle.main.url(forResource: "ClickSound", withExtension: "mp3")!
        self.player.progress = 60.0
        self.player.start()
        self.startTimer()
        imageview.setRadius()
        print("imageview : height \(imageview.frame.height) , width \(imageview.frame.width)")
       // player.setRadius1()
        print("view : height \(player.frame.height) , width \(player.frame.width)")
    }
     @objc func startTimer(){
        
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(player.updateTime), userInfo: nil, repeats: true)
        
        
    }
    @objc func updateTime(){
        
        self.duration += 0.1
        let totalDuration = Int(self.duration)
        let min = totalDuration / 60
        let sec = totalDuration % 60
        timeLabel.textColor = UIColor.white
        timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
        
        if self.duration >= 60.0 {
            stopTimer()
        }
        
    }
     @objc func stopTimer(){
        
        if(timer != nil) {
            timer.invalidate()
            timer = nil
            
           
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func actionOneButtonTapped(sender: UIButton, isSelected: Bool) {
        print("shuffle \(isSelected.description)")
    }
    
    @objc func actionTwoButtonTapped(sender: UIButton, isSelected: Bool) {
        print("like \(isSelected.description)")
    }
    
    @objc func actionThreeButtonTapped(sender: UIButton, isSelected: Bool) {
        print("replay \(isSelected.description)")
        
    }
    
    @objc func interactivePlayerViewDidChangedDuration(playerInteractive: InteractivePlayerView, currentDuration: Double) {
        print("current Duration : \(currentDuration)")
    }
    
    @objc func interactivePlayerViewDidStartPlaying(playerInteractive: InteractivePlayerView) {
        print("interactive player did started")
    }
    
    @objc func interactivePlayerViewDidStopPlaying(playerInteractive: InteractivePlayerView) {
        print("interactive player did stop")
    }

    
    
}
