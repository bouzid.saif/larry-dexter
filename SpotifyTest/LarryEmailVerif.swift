//
//  LarryEmailVerif.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 30/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
class LarryEmailVerif : UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var button_verify: UIButton!
    @IBOutlet weak var email: PaddingTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.email.delegate = self
        self.button_verify.isEnabled = false
         self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        if textField.text!.characters.count > 0 {
            self.button_verify.isEnabled = true
        }else {
           self.button_verify.isEnabled = false
        }
    }
    @IBAction func verify(_ sender: Any) {
         SwiftSpinner.show("Verifying...")
        let params: Parameters = [
            "email": self.email.text!
        ]
        print(params.description)
        Alamofire.request("https://larry2.herokuapp.com/api/user/forgot" , method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                SwiftSpinner.hide()
                 let a = JSON(response.data)
                print(a)
                if a["exist"].boolValue {
                    if a["status"].boolValue {
                        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LarryPinCode") as! LarryPinCode
                        view.email = self.email.text!
                        self.navigationController?.pushViewController(view, animated: true)
                    }else {
                        let alert = UIAlertController(title: "Forgot Password", message: "An error Occured,please try again", preferredStyle: .alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                           // self.navigationController?.popToRootViewController(animated: true)
                        }))
                        
                        // show the alert
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                }else {
                    let alert = UIAlertController(title: "Forgot Password", message: "this email dosen't exist", preferredStyle: .alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                       // self.navigationController?.popToRootViewController(animated: true)
                    }))
                    
                    // show the alert
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
        }
        
    }
}
