//
//  BaseCollectionViewCell.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 02/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgb & 0xFF00) >> 8) / 255.0, blue: CGFloat(rgb & 0xFF) / 255.0, alpha: alpha)
    }
}
private let highlightedColor = UIColor(rgb: 0xD8D8D8) // same with UITableViewCell's selected backgroundColor

class BaseCollectionViewCell: UICollectionViewCell {
    
   
}
