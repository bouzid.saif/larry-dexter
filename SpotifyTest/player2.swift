//
//  player2.swift
//  Spotify
//
//  Created by Bouzid saif on 12/07/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import  GaugeKit
import AVFoundation
import ChameleonFramework
import AudioPlayerManager
import GestureRecognizerClosures
import MediaPlayer
import Spartan
import Chronos
import AZSearchView
import SwiftyJSON
extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
}
class player2 : UIViewController ,SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate {
    static let shared = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "playerx") as! player2
    var durationspotify : Int = 0
      var timer: DispatchTimer!
     var timerSpotify : DispatchTimer!
    var timerTemp : DispatchTimer!
    var timerApple : DispatchTimer!
    var timers : DispatchTimer!
    var timersApple : DispatchTimer!
    var TestAhmed = true
    @objc var play = true
    @objc var songs : [String] = ["Imagine Dragons","Something Just Like This","Pink"]
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var artist: UILabel!
    @objc var id = 0
    var whernow = 0
    var whernowSpotify = 0
    @IBOutlet weak var track: UILabel!
    @objc var duration : Float = 0
    @IBOutlet weak var circle: Gauge!
    var SpotiyOrApple = ""
    var collection = UIViewController()
    var interval : Double = 0.0
    var songQuery: SongQuery = SongQuery()
    var albums: [SongInfo] = []
    var albumsSpotify : [PlaylistTrack] = []
    var tt : [Track] = []
    @objc let audioSession = AVAudioSession.sharedInstance()
    var x1 : String = ""
    var x2 : String = ""
    var x3 : UIImage = UIImage()
    var nowinfo =   MPNowPlayingInfoCenter.default().nowPlayingInfo
    var local_remote = false
    var playlistApple = 0
    var SongApple = 0
    var appleSearch = false
    var resultAppleSearch:JSONX = []
    var appleIndexSearch = 0
    var SpotifySearch = false
    var TrackSpotifySearch : Track? = nil
    var InterPlaylist = false
    var Musics : JSON = []
    var whereNowInter = 0
     var musicPlayerManager = MusicPlayerManager.shared
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        AudioPlayerManager.shared.setup()
        //AudioPlayerManager.shared.
        
      
        if SpotifySearch == false && appleSearch == false{
             let pause = UIBarButtonItem(image: UIImage(named: "nowPlaying_pause"), style: .plain, target: self, action: #selector(player2.play_bar))
        let next = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(player2.next_bar))
        let preview = UIBarButtonItem(image: UIImage(named: "nowPlaying_prev"), style: .plain, target: self, action: #selector(player2.preview_bar))
            self.popupItem.leftBarButtonItems = [ preview, pause ]
            self.popupItem.rightBarButtonItems = [ next ]
        }else{
            let pause = UIBarButtonItem(image: UIImage(named: "nowPlaying_pause"), style: .plain, target: self, action: #selector(player2.play_bar))
            let preview = UIBarButtonItem(image: UIImage(named: "nowPlaying_prev"), style: .plain, target: self, action: #selector(player2.preview_bar))
            self.popupItem.leftBarButtonItems = [preview, pause]
        }
        
    }
    @objc func play_bar(){
        if SpotiyOrApple == "spot"{
            if MediaPlayer.shared.isPlaying {
                self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
                MediaPlayer.shared.pause()
            }else{
               
                self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
                MediaPlayer.shared.resume()
            }
        }else{
            if musicPlayerManager.isPlaying() {
                print("musicPlayer is Playing")
                musicPlayerManager.musicPlayerController.pause()
                self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
                timer.pause()
                timerApple.pause()
                timers.pause()
                timersApple.pause()
            }else {
                musicPlayerManager.musicPlayerController.play()
                self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
              
                timer.start(true)
                timerApple.start(true)
               // timersApple.start(true)
               // timers.start(true)
            }
        if play {
            self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
            timer.pause()
            timerApple.pause()
            timers.pause()
            timersApple.pause()
           
            AudioPlayerManager.shared.pause()
            play = false
        }else{
            self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
            AudioPlayerManager.shared.play()
            timer.start(true)
            timerApple.start(true)
          //  timersApple.start(true)
           // timers.start(true)
            play = true
            
        }
        }
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
   @objc func next_bar(){
    
    self.duration = 0
    if InterPlaylist == false {
    if SpotiyOrApple == "spot" {
       // playSound(id: 0, wher: "next")
        
        if albumsSpotify.count <= whernowSpotify + 1 {
            whernowSpotify = 0
           
        }else{
            
            whernowSpotify = whernowSpotify + 1
            
        }
        //MediaPlayer.shared.pause()
        self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
       
        _ = Spartan.getTracks(ids: [albumsSpotify[whernowSpotify].track.id as! String], market: .us, success: { (track) in
            
            let a : [Track] = track
            print(a[0].popularity)
            self.interval = Double(a[0].durationMs / 1000)
            self.popupItem.title =  a[0].name
            self.popupItem.subtitle = a[0].artists[0].name
            let url = URL(string: a[0].album.images[0].url)
            self.playSound(trackSpotify: a[0])
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.popupItem.image = UIImage(data: data!)
                }
            }
            // self.tabBarController?.popupBar.popupItem?.image
            
            
        }, failure: { (error) in
            print(error)
        })
    }else{
        print("albumsCount",albums.count)
        if (albums.count - 1) == whernow {
            whernow = 0
        }else{
            
            whernow = whernow + 1
            if albums[whernow].artistName == "Item Not Available"{
                if whernow + 1 == (albums.count - 1){
                    whernow = 0
                }else{
                whernow = whernow + 1
                }
            }
        }
        
        print("whernow:",whernow)
        playLocal(album: albums, at: whernow)
        
    }
    }else {
        var testProvider = ""
        if Musics[whereNowInter]["provider"].stringValue == "spotify" {
            testProvider = "spotify"
        }else {
            let testProvider = "apple"
        }
        if (Musics.arrayObject?.count)! <= whereNowInter + 1 {
            whereNowInter = 0
            
        }else{
            
            whereNowInter = whereNowInter + 1
            
        }
        //MediaPlayer.shared.pause()
       // self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        if Musics[whereNowInter]["provider"].stringValue == "spotify" {
            if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
                if timerApple != nil && timersApple != nil &&  timer != nil && timers != nil{
                    timerApple.cancel()
                    timersApple.cancel()
                    timer.cancel()
                    timers.cancel()
                    
                }
                MusicPlayerManager.shared.musicPlayerController.stop()
                print("sure playing local")
                duration = 0.0
                
                
               // self.deactivateAudioSession()
                // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
            }
            SpotiyOrApple = "spot"
        _ = Spartan.getTracks(ids: [Musics[whereNowInter]["song_id"].stringValue], market: .us, success: { (track) in
            
            let a : [Track] = track
            print(a[0].popularity)
            self.interval = Double(a[0].durationMs / 1000)
            self.popupItem.title =  a[0].name
            self.popupItem.subtitle = a[0].artists[0].name
            let url = URL(string: a[0].album.images[0].url)
            self.playSound(trackSpotify: a[0])
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.popupItem.image = UIImage(data: data!)
                }
            }
            // self.tabBarController?.popupBar.popupItem?.image
            
            
        }, failure: { (error) in
            print(error)
        })
        
        }else {
            SpotiyOrApple = "apple"
         self.playInterPlaylist(Musics: self.Musics, at: whereNowInter)
        }
        
    }
        
        //self.play = false
    }
  @objc  func preview_bar(){
        self.duration = 0
        
        stopTimer()
     if InterPlaylist == false {
    if SpotiyOrApple == "spot" {
        if whernowSpotify == 0 {
            whernowSpotify = albumsSpotify.count - 1
            
        }else{
            whernowSpotify = whernowSpotify - 1
            
        }
        //MediaPlayer.shared.pause()
        self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        if SpotifySearch == false {
        _ = Spartan.getTracks(ids: [albumsSpotify[whernowSpotify].track.id as! String], market: .us, success: { (track) in
            
            let a : [Track] = track
            print(a[0].popularity)
            self.interval = Double(a[0].durationMs / 1000)
            self.popupItem.title =  a[0].name
            self.popupItem.subtitle = a[0].artists[0].name
            let url = URL(string: a[0].album.images[0].url)
            self.playSound(trackSpotify: a[0])
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.popupItem.image = UIImage(data: data!)
                }
            }
            // self.tabBarController?.popupBar.popupItem?.image
            
            
        }, failure: { (error) in
            print(error)
        })
        }else{
            self.interval = Double((TrackSpotifySearch?.durationMs)! / 1000)
            self.popupItem.title = TrackSpotifySearch?.name
            self.popupItem.subtitle = TrackSpotifySearch?.artists[0].name
            let url = URL(string: (TrackSpotifySearch?.album.images[0].url)!)
             self.playSound(trackSpotify: TrackSpotifySearch!)
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.popupItem.image = UIImage(data: data!)
                }
            }
           
        }
    }else{
        if whernow == 0 {
            whernow = albums.count - 1
            
            
            
        }else{
            whernow = whernow - 1
               if albums[whernow].artistName == "Item Not Available"{
                if whernow - 1 == -1 {
                whernow = albums.count - 1
                }else{
                    whernow = whernow - 1
                }
            }
        }
       
        print("whernow:",whernow)
       
             playLocal(album: albums, at: whernow)
            
          
        
    
    }
     }else {
        self.popupItem.leftBarButtonItems![0].isEnabled = false
        self.popupItem.leftBarButtonItems![1].isEnabled = false
        self.popupItem.rightBarButtonItems![0].isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 2 , execute: {
            self.popupItem.leftBarButtonItems![0].isEnabled = true
             self.popupItem.leftBarButtonItems![1].isEnabled = true
             self.popupItem.rightBarButtonItems![0].isEnabled = true
            
        })
        print("sisssssssss")
        if whereNowInter == 0 {
            whereNowInter = (Musics.arrayObject?.count)! - 1
            
        }else{
            whereNowInter = whereNowInter - 1
            
        }
        //MediaPlayer.shared.pause()
        //self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        if Musics[whereNowInter]["provider"].stringValue == "spotify" {
            SpotiyOrApple = "spot"
            if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
                if timerApple != nil && timersApple != nil &&  timer != nil && timers != nil{
                    timerApple.cancel()
                    timersApple.cancel()
                    timer.cancel()
                    timers.cancel()
                    timerApple = nil
                    timersApple = nil
                    timer = nil
                    timers = nil
                }
                MusicPlayerManager.shared.musicPlayerController.stop()
                print("sure playing local")
                duration = 0.0
                
                
                self.deactivateAudioSession()
                // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
            }
            _ = Spartan.getTracks(ids: [Musics[whereNowInter]["song_id"].stringValue], market: .us, success: { (track) in
                
                let a : [Track] = track
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.popupItem.title =  a[0].name
                self.popupItem.subtitle = a[0].artists[0].name
                let url = URL(string: a[0].album.images[0].url)
                self.playSound(trackSpotify: a[0])
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.popupItem.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                
                
            }, failure: { (error) in
                print(error)
            })
            
        }else {
            SpotiyOrApple = "apple"
            self.playInterPlaylist(Musics: self.Musics, at: whereNowInter)
        }
    }
        //self.play = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         image.setRadius()
        print("yes ViewdidlLoad")
      
       
        let swipeR = UISwipeGestureRecognizer(target: self, action: #selector(player2.next_preview(_:)))
        swipeR.direction = UISwipeGestureRecognizerDirection.right
        self.circle.addGestureRecognizer(swipeR)
        let swipeL = UISwipeGestureRecognizer(target: self, action: #selector(player2.next_preview(_:)))
        swipeL.direction = UISwipeGestureRecognizerDirection.left
        self.circle.addGestureRecognizer(swipeL)
        //playSound(id: 1, wher: "begin")
        
          let gestureRecognizer = UIRotationGestureRecognizer { gestureRecognizer in
           
        }
        
        self.circle.addGestureRecognizer(gestureRecognizer)

       /* self.circle.onRotate { rotate in  print("rotation: ",rotate.rotation)
            
            
            )} */
        let rotation = self.circle.gestureRecognizers!.last as! UIRotationGestureRecognizer
        if SPTAuth.defaultInstance().session != nil {
        MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
        MediaPlayer.shared.delegate = self
        }
            
          
           /* if MediaPlayer.shared.currentTrack != nil {
            MediaPlayer.shared.resume()
            } */
            
            track.text = x1
            artist.text = x2
            self.popupItem.title = x1
            self.popupItem.subtitle = x2
           // popupItem.image = x3
            self.circle.reloadInputViews()
            let notificationCenter = NotificationCenter.default
            notificationCenter.removeObserver(self, name: MusicPlayerManager.didUpdateState, object: nil)
       // self.handleNewSession()
       

                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(handleMusicPlayerManagerDidUpdateState),
                                                       name: MusicPlayerManager.didUpdateState,
                                                       object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateCurrentItemMetadata),
                                               name: NSNotification.Name(rawValue: "updateForGround"),
                                               object: nil)
                AudioPlayerManager.shared.setup()
        if SpotifySearch == true || appleSearch == true {
        self.popupItem.rightBarButtonItems?.removeAll()
        }else{
            if self.popupItem.rightBarButtonItems?.count == 0 {
                let next = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(player2.next_bar))
                self.popupItem.rightBarButtonItems = [ next ]
            }
        }
            
        
        
    }
   
    @objc func handleMusicPlayerManagerDidUpdateState() {
        DispatchQueue.main.async {
            self.updatePlaybackControls()
            //self.updateCurrentItemMetadata()
            
            
        }
    }
    func updatePlaybackControls() {
    let playbackState = musicPlayerManager.musicPlayerController.playbackState
    
    switch playbackState {
    case .interrupted, .paused, .stopped:
        
    self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        
    case .playing:
    self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
      self.TestAhmed = true
     updateCurrentItemMetadata()
    default:
    break
    }
        if playbackState == .paused {
            self.TestAhmed = false
        }
    if playbackState == .stopped {
    
    } else {
    
    }
    
   
    }
  /*   func updateAppleMusicPlayer(){
        if let nowPlayingItem = musicPlayerManager.musicPlayerController.nowPlayingItem {
         print("saif:",self.musicPlayerManager.musicPlayerController.currentPlaybackTime)
            self.circle.maxValue = CGFloat(nowPlayingItem.playbackDuration)
            let secc = CGFloat(nowPlayingItem.playbackDuration) * 1000000000
            let min = totalDuration / 60
            let sec = totalDuration % 60
            timeLabel.textColor = UIColor.white
            timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
            
        }
    } */
    @objc func updateCurrentItemMetadata() {
        
        if let nowPlayingItem = musicPlayerManager.musicPlayerController.nowPlayingItem {
            if appleSearch == false {
            print("ena rani na3malha  w yedek")
            self.popupItem.image = nowPlayingItem.artwork?.image(at: CGSize(width: 40 , height: 40)) ?? UIImage(named:"default_song")
                
            self.image.image = nowPlayingItem.artwork?.image(at: CGSize(width: self.image.frame.width, height: self.image.frame.height)) ?? UIImage(named:"default_song")
                
           self.track.text = nowPlayingItem.title
             self.popupItem.title = nowPlayingItem.title
              self.artist.text = nowPlayingItem.artist
             self.popupItem.subtitle = nowPlayingItem.artist
                if TestAhmed {
                self.circle.maxValue =  CGFloat(TimeInterval(exactly: nowPlayingItem.playbackDuration)!  )
                }
                print("TestAhmed:",TestAhmed)
                if TestAhmed {
                    if !self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN  {
                        if Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime) == 0 {
                            self.interval  = nowPlayingItem.playbackDuration
                            self.duration = 0.0
                             self.circle.rate = 0.0
                            print(self.interval , "hhhhhhh")
                        } else {
                            print("aaaaaa")
                        self.duration = Float(self.musicPlayerManager.musicPlayerController.currentPlaybackTime)
                            self.interval  = nowPlayingItem.playbackDuration
                            UIView.performWithoutAnimation {
                                print("rateeeeeeeeeee:",CGFloat(Int(nowPlayingItem.playbackDuration) - Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime)))
                               //  self.circle.rate = CGFloat(Int(nowPlayingItem.playbackDuration) - Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime))
                            }
                           
                           // self.interval = Double(Int(nowPlayingItem.playbackDuration) - Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime))
                        }
                    }else {
                        print("eeeeeeee")
                        self.interval  = nowPlayingItem.playbackDuration
                        self.circle.rate = 0.0
                    }
                    timersApple =   DispatchTimer(interval: self.interval  * 0.02, closure: { (timer: RepeatingTimer, count: Int) in
                        DispatchQueue.main.async {
                            print("kakakaka")
                           self.circle.rate += 0.002
                            print(self.circle.rate)
                            print("********")
                            //self.circle.layoutSubviews()
                             self.duration += 0.002
                            
                        }
                        })
                    timersApple.start(true)
                     timers =  DispatchTimer(interval: 0.1, closure: { (timer: RepeatingTimer, count: Int) in
                        //print(self.interval)
                        //self.interval += 0.1
                        if !self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN {
                        //print(Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime))
                            if !self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN  {
                                print(self.musicPlayerManager.musicPlayerController.currentPlaybackTime)
                                let min = Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN ? 0 : self.musicPlayerManager.musicPlayerController.currentPlaybackTime ) / 60
                        let sec = Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN ? 0 : self.musicPlayerManager.musicPlayerController.currentPlaybackTime ) - (Int(self.musicPlayerManager.musicPlayerController.currentPlaybackTime.isNaN ? 0 : self.musicPlayerManager.musicPlayerController.currentPlaybackTime) / 60) * 60
                        DispatchQueue.main.async {
                           self.duration += 0.1
                              self.circle.rate = CGFloat(self.duration)
                           
                        self.timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
                            
                        //print("SaifX:",)
                    }
                            }
                        } else {
                            self.timeLabel.text = "--:--"
                        }
               })
                
                
               
                timers.start(true)
                 self.TestAhmed = false
                }
               
            }else{
                
               // self.popupItem.image = self.image.image
                //self.image.image = nowPlayingItem.artwork?.image(at: CGSize(width: self.image.frame.width, height: self.image.frame.height))
                self.track.text = resultAppleSearch[appleIndexSearch]["songName"].stringValue
                self.popupItem.title = resultAppleSearch[appleIndexSearch]["songName"].stringValue
                self.artist.text = resultAppleSearch[appleIndexSearch]["artistName"].stringValue
                self.popupItem.subtitle = resultAppleSearch[appleIndexSearch]["artistName"].stringValue
                self.circle.maxValue = CGFloat(TimeInterval(exactly: resultAppleSearch[appleIndexSearch]["duration"].intValue)! / 1000)
                print("self.circle.maxValue:",self.circle.maxValue)
            }
          
        } else {
             self.popupItem.image = nil
            self.image.image = nil
            self.track.text = " "
            self.popupItem.title = " "
            self.artist.text =  "  "
            self.popupItem.subtitle = " "
        }
    }
    /*
     self.track.text = albums[at].songTitle
     self.popupItem.title = albums[at].songTitle
     self.artist.text = albums[at].artistName
     self.popupItem.subtitle = albums[at].artistName
     let songId: NSNumber = albums[at].songId
     let item: MPMediaItem = songQuery.getItem2(songId: album[at].songId,row: self.SongApple,playlistNum: self.playlistApple)
     self.image.image = UIImage(named:"default_song")!
     self.popupItem.image = UIImage(named:"default_song")!
     if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
     self.image.image = imageSound.image(at: CGSize(width: self.image.frame.width, height: self.image.frame.height))
     self.popupItem.image = imageSound.image(at: CGSize(width: 40 , height: 40))!
     
     }
     self.interval = item.playbackDuration
     self.play = true
     self.duration = 0
     self.popupItem.progress = 0.0
    */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func startTimer(){
        if timerSpotify != nil {
        timerSpotify = nil
        }
        if timerTemp != nil {
            timerTemp = nil
        }
        
             timer = DispatchTimer(interval: 0.1, closure: {
                (timer: RepeatingTimer, count: Int) in
                //self.duration += 0.1
               // let totalDuration = Int(self.duration)
               // let min = totalDuration / 60
                //let sec = totalDuration % 60
               DispatchQueue.main.async {
                //self.timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
               // self.circle.rate  = CGFloat(self.duration)
               // self.circle.layoutSubviews()
               // self.timeLabel.layoutSubviews()
                }
                
                if self.SpotiyOrApple == "apple" {
                if CGFloat(self.duration) >= self.circle.maxValue {
                    print(self.duration,self.circle.maxValue)
                    self.duration = 0
                    if self.appleSearch == false {
                   self.next_bar()
                    }else{
                        self.timeLabel.text = "--:--"
                    }
                }
                }
                
            })
        ///houni Kamel
         if let nowPlayingItem = musicPlayerManager.musicPlayerController.nowPlayingItem {
            
            if (self.popupItem.progress > 0 ) {
                print("interval:",self.interval * 0.002)
                let q = (Double(self.musicPlayerManager.musicPlayerController.currentPlaybackTime ) / self.interval )
                 print("q:",q)
                 print("progress:",self.popupItem.progress)
                //self.popupItem.progress = Float(Double(self.popupItem.progress) + (q * 0.002))
                
                self.popupItem.progress =  Float(q)
                 print("progress2:",self.popupItem.progress)
            }
        timerApple = DispatchTimer(interval: self.interval * 0.002, closure: { (timer: RepeatingTimer, count: Int) in
            DispatchQueue.main.async {
                
                
               // self.circle.rate += 0.002
                //print("vbbbnnn",self.circle.rate)
                self.popupItem.progress += 0.002
                 print("popup:",self.popupItem.progress)
                self.popupBar.layoutSubviews()
            }
        })
         }else {
            timerApple = DispatchTimer(interval: self.interval * 0.002, closure: { (timer: RepeatingTimer, count: Int) in
                DispatchQueue.main.async {
                    
                    
                    // self.circle.rate += 0.002
                    //print("vbbbnnn",self.circle.rate)
                    self.popupItem.progress += 0.002
                     print("popup:",self.popupItem.progress)
                    self.popupBar.layoutSubviews()
                }
            })
        }
        timer.start(true)
        timerApple.start(true)
       
           // (collection as! LocalMusic).timer = Timer.scheduledTimer(timeInterval: self.interval * 0.002, target :  (collection as! LocalMusic) , selector: #selector( (collection as! LocalMusic)._timerTicked(_:)), userInfo : nil , repeats : true )
        
        
        
    }

     func playSound(trackSpotify:Track){
        self.reloadInputViews()
       // SPTAudioStreamingController.sharedInstance().diskCache.writeCacheData(withKey: trackSpotify.name, data: trackSpotify, offset: 1024 * 1024 * 3)
        image.setImage(withUrl: URL(string: trackSpotify.album.images[0].url)!, completion: { (ImageInstance, error) in
            print("shiit yes")
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: (ImageInstance?.image)!)
        } )
        
         self.nowinfo = [MPMediaItemPropertyTitle: self.track.text ?? "ab",MPMediaItemPropertyArtist: self.artist.text ?? "ac",MPMediaItemPropertyArtwork: MPMediaItemArtwork(image: self.image.image ?? UIImage(named:"default_song")!),MPNowPlayingInfoPropertyElapsedPlaybackTime :0.0 ]
    var artistx = ""
       
       
        for art in trackSpotify.artists {
            artistx = artistx + art.name + ", "
        }
        artistx.removeLast()
        artistx.removeLast()
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: trackSpotify.name,MPMediaItemPropertyArtist: artistx,MPNowPlayingInfoPropertyPlaybackRate : 1,MPNowPlayingInfoPropertyElapsedPlaybackTime : 0.0,MPMediaItemPropertyPlaybackDuration: trackSpotify.durationMs / 1000]
      
        track.text = trackSpotify.name
        track.adjustsFontSizeToFitWidth = true
        artist.text = artistx
        artist.adjustsFontSizeToFitWidth = true
        artistx = ""
        self.duration = 0
        print("maxValue:",(CGFloat(Double(trackSpotify.durationMs) / 1000) * 0.002))
        self.circle.maxValue = 1
        self.circle.rate = 0
        self.popupItem.progress = 0.0
        self.timeLabel.text = "--:--"
        if timerSpotify != nil {
        timerSpotify.cancel()
        }
        self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
         MediaPlayer.shared.play(track: trackSpotify)
        
    }
    func updateTabBar(){
        if self.popupItem.leftBarButtonItems != nil {
            if SpotiyOrApple == "spot"{
                if MediaPlayer.shared.isPlaying == false {
                    self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
                }else{
                    self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
                }
            }else{
                if MusicPlayerManager.shared.isPlaying() == false{
                      self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
                }else{
                      self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
                }
            }
        }
    }
    func playAppleSearch(result:JSONX,index:Int){
        resultAppleSearch = result
        appleIndexSearch = index
        if SpotifySearch == true || appleSearch == true {
            self.popupItem.rightBarButtonItems?.removeAll()
        }else{
            if self.popupItem.rightBarButtonItems?.count == 0 {
                let next = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(player2.next_bar))
                self.popupItem.rightBarButtonItems = [ next ]
            }
        }
        if local_remote{
            if timerSpotify != nil {
                timerSpotify.cancel()
            }
             //DispatchQueue.main.async(execute: {
                print("image:",result[index]["image"].stringValue)
                self.image.setImage(withUrl: URL(string: result[index]["image"].stringValue)!, placeholder: UIImage(named:"default_song"), crossFadePlaceholder: true, cacheScaled: true, completion: {(complete, error) in
                 self.image.image = complete?.image
                    self.popupItem.image = complete?.image
                    MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: self.image.image!)
                })
          //  })
         /*    DispatchQueue.main.async(execute: {
             MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: self.image.image!)
            }) */
    
            musicPlayerManager.musicPlayerController.setQueue(with: [result[index]["id"].stringValue])
            self.duration = 0
            self.popupItem.progress = 0.0
            self.interval = Double(result[index]["duration"].intValue / 1000)
            self.circle.maxValue = CGFloat(result[index]["duration"].intValue / 1000)
            print("circle:",CGFloat(result[index]["duration"].intValue))
            self.play = true
            musicPlayerManager.musicPlayerController.prepareToPlay(completionHandler: {(error) in
                print("playing")
                
              //  self.startTimer()
                  self.startTimer()
               // print(item.playbackDuration)
               // MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: result[index]["songName"].stringValue,MPMediaItemPropertyArtist: result[index]["artistName"].stringValue,MPNowPlayingInfoPropertyPlaybackRate : 1,MPNowPlayingInfoPropertyElapsedPlaybackTime : 0.0,MPMediaItemPropertyPlaybackDuration:  TimeInterval(exactly: result[index]["duration"].intValue)! / 1000]
               
                
            }
                )
            print("rate:",musicPlayerManager.musicPlayerController.currentPlaybackRate)
            musicPlayerManager.musicPlayerController.play()
        }
    }
    func playInterPlaylist(Musics:JSON,at:Int) {
        if Musics[at]["provider"].stringValue == "apple" {
            if self.popupItem.rightBarButtonItems?.count == 0 {
                let next = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(player2.next_bar))
                self.popupItem.rightBarButtonItems = [ next ]
            }
            if local_remote{
                if timerSpotify != nil {
                    timerSpotify.cancel()
                }
                let media = songQuery.findSongWithPersistentIdString(persistentIDString: Musics[at]["song_id"].stringValue)
                //let media = songQuery.getItem2(songId: album[at].songId,row: at,playlistNum: self.playlistApple)
                
                //track.text = media.title
                //artist.text = media.artist
                self.reloadInputViews()
                DispatchQueue.main.async(execute: {
                    self.image.image = UIImage(named:"default_song")
                })
                
                if  let imageSound: MPMediaItemArtwork = media?.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                    DispatchQueue.main.async(execute: {
                        self.image.image = imageSound.image(at: CGSize(width: self.image.frame.width , height: self.image.frame.height))
                        MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: self.image.image!)
                    })
                }
                // self.nowinfo = [MPMediaItemPropertyTitle: self.track.text ?? "ab",MPMediaItemPropertyArtist: self.artist.text ?? "ac",MPMediaItemPropertyArtwork: MPMediaItemArtwork(image: self.image.image ?? UIImage(named:"default_song")!),MPNowPlayingInfoPropertyElapsedPlaybackTime :0.0 ]
                
                let songId: NSNumber = albums[at].songId
                let item: MPMediaItem = songQuery.findSongWithPersistentIdString(persistentIDString: Musics[at]["song_id"].stringValue)!
                if item.assetURL == nil {
                    print("cloudItem")
                }
                
                print("interval",item.playbackDuration)
                self.interval = item.playbackDuration
                self.play = true
                self.duration = 0
                self.popupItem.progress = 0.0
                // updateCurrentItemMetadata()
                // let h = MPMediaItemCollection(items: [item])
                print(String(item.persistentID))
                if #available(iOS 10.1, *) {
                    // musicPlayerManager = MusicPlayerManager()
                    
                    musicPlayerManager.beginPlayback(itemCollection: MPMediaItemCollection.init(items: [item]))
                    if self.timer != nil {
                        self.stopTimer()
                    }
                    
                    musicPlayerManager.musicPlayerController.prepareToPlay(completionHandler: {(error) in
                        print("playing")
                        self.startTimer()
                        print(item.playbackDuration)
                        
                      
                        
                        
                    }
                    )
                    musicPlayerManager.musicPlayerController.play()
                    
                    //musicPlayerManager.beginPlayback(itemID: String(item.persistentID))
                } else {
                    // Fallback on earlier versions
                }
                // musicPlayerManager.beginPlayback(itemCollection: item)
                //AudioPlayerManager.shared
                //   AudioPlayerManager.shared.play(mediaItem: item)
                /* if (AudioPlayerManager.shared.isPlaying(mediaItem: item)){
                 print("true")
                 } */
                // musicPlayerManager.beginPlayback(itemID: String(describing: songId))
                //AudioPlayerManager.shared.play(mediaItem: media)
                
            }
            else{
                let media = songQuery.findSongWithPersistentIdString(persistentIDString: Musics[at]["song_id"].stringValue)
                if timerSpotify != nil {
                    timerSpotify.cancel()
                }
                //track.text = media.title
                //artist.text = media.artist
                self.reloadInputViews()
                
                
                
                self.track.text = Musics[at]["title"].stringValue
                self.popupItem.title = Musics[at]["title"].stringValue
                self.artist.text = Musics[at]["artist"].stringValue
                self.popupItem.subtitle = Musics[at]["artist"].stringValue
              
              
                let item: MPMediaItem = songQuery.findSongWithPersistentIdString(persistentIDString: Musics[at]["song_id"].stringValue)!
                self.image.image = UIImage(named:"default_song")!
                self.popupItem.image = UIImage(named:"default_song")!
                if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                    DispatchQueue.main.async(execute: {
                        self.image.image = imageSound.image(at: CGSize(width: self.image.frame.width, height: self.image.frame.height))
                        self.popupItem.image = imageSound.image(at: CGSize(width: 40 , height: 40))!
                    })
                }
                self.interval = item.playbackDuration
                self.play = true
                self.duration = 0
                self.popupItem.progress = 0.0
                if #available(iOS 10.1, *) {
                    //  musicPlayerManager = MusicPlayerManager()
                    print(item.title)
                    musicPlayerManager.beginPlayback(itemCollection: MPMediaItemCollection.init(items: [item]))
                    if self.timer != nil {
                        self.stopTimer()
                    }
                    
                    musicPlayerManager.musicPlayerController.prepareToPlay(completionHandler: {(error) in
                        print("playing")
                        self.startTimer()
                        MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
                        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: item.title!,MPMediaItemPropertyArtist: item.artist!,MPNowPlayingInfoPropertyPlaybackRate : 1,MPNowPlayingInfoPropertyElapsedPlaybackTime : 0.0,MPMediaItemPropertyPlaybackDuration: item.playbackDuration]
                        self.TestAhmed = true
                    }
                    )
                    musicPlayerManager.musicPlayerController.play()
                    
                    //musicPlayerManager.beginPlayback(itemID: String(item.persistentID))
                } else {
                    // Fallback on earlier versions
                }
                
            }
        }
        
        
    }
    
     func playLocal(album:[SongInfo],at:Int){
        if SpotifySearch == true || appleSearch == true {
            self.popupItem.rightBarButtonItems?.removeAll()
        }else{
            if self.popupItem.rightBarButtonItems?.count == 0 {
                let next = UIBarButtonItem(image: UIImage(named: "nextFwd"), style: .plain, target: self, action: #selector(player2.next_bar))
                self.popupItem.rightBarButtonItems = [ next ]
            }
        }
        if local_remote{
            if timerSpotify != nil {
                timerSpotify.cancel()
            }
            
            let media = songQuery.getItem2(songId: album[at].songId,row: at,playlistNum: self.playlistApple)
            
            //track.text = media.title
            //artist.text = media.artist
            self.reloadInputViews()
            DispatchQueue.main.async(execute: {
                self.image.image = UIImage(named:"default_song")
            })
           
            if  let imageSound: MPMediaItemArtwork = media.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                 DispatchQueue.main.async(execute: {
                    self.image.image = imageSound.image(at: CGSize(width: self.image.frame.width , height: self.image.frame.height))
                    MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: self.image.image!)
                 })
            }
           // self.nowinfo = [MPMediaItemPropertyTitle: self.track.text ?? "ab",MPMediaItemPropertyArtist: self.artist.text ?? "ac",MPMediaItemPropertyArtwork: MPMediaItemArtwork(image: self.image.image ?? UIImage(named:"default_song")!),MPNowPlayingInfoPropertyElapsedPlaybackTime :0.0 ]
            
            let songId: NSNumber = albums[at].songId
            let item: MPMediaItem = songQuery.getItem2(songId: album[at].songId,row: at,playlistNum: self.playlistApple)
            if item.assetURL == nil {
                print("cloudItem")
            }
            
            print("interval",item.playbackDuration)
            self.interval = item.playbackDuration
            self.play = true
            self.duration = 0
            self.popupItem.progress = 0.0
           // updateCurrentItemMetadata()
           // let h = MPMediaItemCollection(items: [item])
            print(String(item.persistentID))
            if #available(iOS 10.1, *) {
               // musicPlayerManager = MusicPlayerManager()
                
                musicPlayerManager.beginPlayback(itemCollection: MPMediaItemCollection.init(items: [item]))
                if self.timer != nil {
                    self.stopTimer()
                }
               
                musicPlayerManager.musicPlayerController.prepareToPlay(completionHandler: {(error) in
                    print("playing")
                     //self.startTimer()
                    //print(item.playbackDuration)
                   
                    self.startTimer()
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: item.title!,MPMediaItemPropertyArtist: item.artist!,MPNowPlayingInfoPropertyPlaybackRate : 1,MPNowPlayingInfoPropertyElapsedPlaybackTime : 0.0,MPMediaItemPropertyPlaybackDuration: item.playbackDuration]
                    self.TestAhmed = true
                    
                    
                }
                )
                musicPlayerManager.musicPlayerController.play()
                
                //musicPlayerManager.beginPlayback(itemID: String(item.persistentID))
            } else {
                // Fallback on earlier versions
            }
           // musicPlayerManager.beginPlayback(itemCollection: item)
            //AudioPlayerManager.shared
          //   AudioPlayerManager.shared.play(mediaItem: item)
           /* if (AudioPlayerManager.shared.isPlaying(mediaItem: item)){
                print("true")
            } */
           // musicPlayerManager.beginPlayback(itemID: String(describing: songId))
            //AudioPlayerManager.shared.play(mediaItem: media)
            
        }
        else{
        let media = songQuery.getItem(songId: album[at].songId)
            if timerSpotify != nil {
                timerSpotify.cancel()
            }
        //track.text = media.title
        //artist.text = media.artist
       self.reloadInputViews()
           
      
        
            self.track.text = albums[at].songTitle
            self.popupItem.title = albums[at].songTitle
            self.artist.text = albums[at].artistName
            self.popupItem.subtitle = albums[at].artistName
            let songId: NSNumber = albums[at].songId
            let item: MPMediaItem = songQuery.getItem( songId: songId )
            self.image.image = UIImage(named:"default_song")!
            self.popupItem.image = UIImage(named:"default_song")!
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                 DispatchQueue.main.async(execute: {
                self.image.image = imageSound.image(at: CGSize(width: self.image.frame.width, height: self.image.frame.height))
                self.popupItem.image = imageSound.image(at: CGSize(width: 40 , height: 40))!
                })
            }
            self.interval = item.playbackDuration
            self.play = true
            self.duration = 0
            self.popupItem.progress = 0.0
          /*  AudioPlayerManager.shared.play(mediaItem: media)
            for item2 in (AudioPlayerManager.shared.currentTrack?.nowPlayingInfo)! as [String : Any] {
                let key2 = item2.key
                let value = item2.value
                
                switch key2 {
                case "playbackDuration":
                    print("yes saif",value as! Float)
                    self.circle.maxValue =  value as! CGFloat
                    break
                default:
                    continue
                }
            }
            if timer != nil {
                stopTimer()
            }
            startTimer()
        } */
            if #available(iOS 10.1, *) {
              //  musicPlayerManager = MusicPlayerManager()
                print(item.title)
                musicPlayerManager.beginPlayback(itemCollection: MPMediaItemCollection.init(items: [item]))
                if self.timer != nil {
                    self.stopTimer()
                }
                
                musicPlayerManager.musicPlayerController.prepareToPlay(completionHandler: {(error) in
                    print("playing")
                    self.startTimer()
                     MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
                     MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: item.title!,MPMediaItemPropertyArtist: item.artist!,MPNowPlayingInfoPropertyPlaybackRate : 1,MPNowPlayingInfoPropertyElapsedPlaybackTime : 0.0,MPMediaItemPropertyPlaybackDuration: item.playbackDuration]
                    self.TestAhmed = true
                }
                )
                musicPlayerManager.musicPlayerController.play()
                
                //musicPlayerManager.beginPlayback(itemID: String(item.persistentID))
            } else {
                // Fallback on earlier versions
            }
        
        }
    }
    func play_pause_apple(){
        if local_remote {
            if musicPlayerManager.isPlaying() == true{
                if timer != nil {
                    timer.pause()
                    
                }
                if timers != nil {
                     timers.pause()
                }
                if timersApple != nil {
                     timersApple.pause()
                }
                if timerApple != nil {
                    timerApple.pause()
                   
                   
                }
            }else{
                timer.start(true)
                timerApple.start(true)
                //timersApple.start(true)
                //timers.start(true)
            }
            //musicPlayerManager.togglePlayPause()
            
        }else{
            if musicPlayerManager.isPlaying() == true{
                if timer != nil {
                    timer.pause()
                }
                if timers != nil {
                    timers.pause()
                }
                if timersApple != nil {
                    timersApple.pause()
                }
                if timerApple != nil {
                    timerApple.pause()
                    
                }
            }else{
                if timer != nil {
                    timer.start(true)
                }
                if timers != nil {
                    //timers.start(true)
                }
                if timersApple != nil {
                  //  timersApple.start(true)
                }
                if timerApple != nil {
                    timerApple.start(true)
                   
                }
            }
           // musicPlayerManager.togglePlayPause()
            
        }
    }
    @IBAction func play_pause(_ sender: Any) {
    print("kk")
        if SpotiyOrApple == "spot"{
            if MediaPlayer.shared.isPlaying {
                MediaPlayer.shared.pause()
            }else{
                MediaPlayer.shared.resume()
            }
        
        }else{
            if local_remote {
                if musicPlayerManager.isPlaying() == true{
                    print("azerty")
                      if timer != nil {
                    timer.pause()
                        }
                    if timers != nil {
                        timers.pause()
                    }
                    if timersApple != nil {
                        timersApple.pause()
                    }
                     if timerApple != nil {
                    timerApple.pause()
                        
                         }
                }else{
                    timer.start(true)
                    timerApple.start(true)
                   // timersApple.start(true)
                    //timers.start(true)
                }
                  musicPlayerManager.togglePlayPause()
                
            }else{
                if musicPlayerManager.isPlaying() == true{
                    print("aqwxc")
                    if timer != nil {
                    timer.pause()
                    }
                    if timers != nil {
                        timers.pause()
                    }
                    if timersApple != nil {
                        timersApple.pause()
                    }
                    if timerApple != nil {
                    timerApple.pause()
                       
                    }
                }else{
                       if timer != nil {
                    timer.start(true)
                             }
                    if timers != nil {
                       // timers.start(true)
                    }
                    if timersApple != nil {
                       // timersApple.start(true)
                    }
                      if timerApple != nil {
                    timerApple.start(true)
                        
                         }
                }
                 musicPlayerManager.togglePlayPause()
         
        }
        }
    }
    @objc func updateTime(){
        self.duration += 0.1
        let totalDuration = Int(self.duration)
        let min = totalDuration / 60
        let sec = totalDuration % 60
        timeLabel.textColor = UIColor.white
        timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
        self.circle.rate  = CGFloat(self.duration)
       
        if CGFloat(self.duration) >= self.circle.maxValue {
            print(self.duration,self.circle.maxValue)
            self.duration = 0
            stopTimer()
            if SpotiyOrApple == "spot" {
            //playSound(id: 0)
            }else{
                
            }
            self.play = false
        }
        
    }
    @objc func stopTimer(){
        if InterPlaylist == false {
        if SpotiyOrApple == "spot"{
        timerSpotify.cancel()
        }else{
            timer.cancel()
            timerApple.cancel()
            timersApple.cancel()
            timers.cancel()
        }
        }else {
            
            if timerSpotify != nil {
            timerSpotify.cancel()
            }
             if timer != nil {
            timer.cancel()
            }
             if timerApple != nil {
            timerApple.cancel()
            }
             if timersApple != nil {
            timersApple.cancel()
            }
             if timers != nil {
            timers.cancel()
            }
        }
            
            
        
      /*  if (collection as! LocalMusic).timer != nil {
         (collection as! LocalMusic).timer?.invalidate()
             (collection as! LocalMusic).timer = nil
        }
        */
    }

    @objc func next(){
        self.duration = 0
        stopTimer()
        if SpotiyOrApple == "spot" {
            if albumsSpotify.count <= whernowSpotify + 1 {
                whernowSpotify = 0
                
            }else{
                whernowSpotify = whernowSpotify + 1
                
            }
           // MediaPlayer.shared.pause()
            self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
            
            _ = Spartan.getTracks(ids: [albumsSpotify[whernowSpotify].track.id as! String], market: .us, success: { (track) in
                
                let a : [Track] = track
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.popupItem.title =  a[0].name
                self.popupItem.subtitle = a[0].artists[0].name
                let url = URL(string: a[0].album.images[0].url)
                self.playSound(trackSpotify: a[0])
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.popupItem.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                
                
            }, failure: { (error) in
                print(error)
            })
        }else{
            
        }
        
        self.play = false
       /* if id <= 1 {
            
            playSound(id: id)
        }else{
            id = 0
            playSound(id: id)
        } */
  
    }
    @objc func previews() {
        self.duration = 0
        
        stopTimer()
        if SpotiyOrApple == "spot" {
            if whernowSpotify == 0 {
                whernowSpotify = albumsSpotify.count
                
            }else{
                whernowSpotify = whernowSpotify - 1
                
            }
           // MediaPlayer.shared.pause()
            self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        
            _ = Spartan.getTracks(ids: [albumsSpotify[whernowSpotify].track.id as! String], market: .us, success: { (track) in
                
                let a : [Track] = track
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.popupItem.title =  a[0].name
                self.popupItem.subtitle = a[0].artists[0].name
                let url = URL(string: a[0].album.images[0].url)
                self.playSound(trackSpotify: a[0])
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.popupItem.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                
                
            }, failure: { (error) in
                print(error)
            })
        }else{
            
        }
        
        self.play = false
    }
    @IBAction func next_preview(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizerDirection.left{
           /* if SpotiyOrApple == "spot"{
            next()
            }else{
                next_bar()
            } */
            next_bar()
        }else if sender.direction == UISwipeGestureRecognizerDirection.right {
           /* if SpotiyOrApple == "spot" {
         previews()
            }else{
                preview_bar()
            } */
            preview_bar()
        }
    }
    override func remoteControlReceived(with event: UIEvent?) {
        let rc = event!.subtype
        
        print("received remote control \(rc.rawValue)") // 101 = pause, 100 = play
        switch rc {
        case UIEventSubtype.remoteControlPlay:
            if SpotiyOrApple == "spot"{
               
                    play_bar()
              
            }else{
                if musicPlayerManager.isPlaying() == false {
                    musicPlayerManager.musicPlayerController.play()
                   // self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
                    
                    timer.start(true)
                    timerApple.start(true)
                   // timersApple.start(true)
                   // timers.start(true)
                }
           
            }

            break
        case UIEventSubtype.remoteControlPause :
            if SpotiyOrApple == "spot" {
                
                    MediaPlayer.shared.pause()
                
            }else{
                if musicPlayerManager.isPlaying() {
                    musicPlayerManager.musicPlayerController.pause()
                    //self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
                    timer.pause()
                    timerApple.pause()
                    timers.pause()
                    timersApple.pause()
                }
            }
            break
        case .remoteControlPreviousTrack:
            if SpotiyOrApple == "spot"{
           previews()
            }else{
                self.preview_bar()
            }
            break;
        case .remoteControlNextTrack:
            if SpotiyOrApple == "spot"{
           next()
            }
            else{
              self.next_bar()
            }
            break;
        default:break
        }
    }
    
}
extension player2: MediaPlayerDelegate {
    
    func mediaPlayerDidStartPlaying(track: Track) {
     print("start playing right now Spotify , am i ?")
        var totalDuration = 0
        self.popupItem.leftBarButtonItems![1].image = UIImage(named:"nowPlaying_pause")
        var dur = 0.0
       timerTemp = DispatchTimer(interval: 1, closure: { (timer: RepeatingTimer, count : Int) in
            dur += 1
      
             totalDuration = Int(dur)
        let min = totalDuration / 60
        let sec = totalDuration % 60
            DispatchQueue.main.async {
             self.timeLabel.text = NSString(format: "%i:%02i",min,sec ) as String
              // MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPMediaItemPropertyPlaybackDuration] = track.durationMs / 1000
            }
            })
            print(Double(track.durationMs / 1000))
            print(track.durationMs / 1000)
             timerSpotify = DispatchTimer(interval: Double(track.durationMs / 1000) * 0.002, closure: {
                (timer: RepeatingTimer, count: Int) in
                
              
                
                DispatchQueue.main.async {
               
                self.popupItem.progress += 0.002
                self.circle.rate += 0.002
                //print("vbbbnnn",self.circle.rate)
                self.popupBar.layoutSubviews()
                }
            })
        timerSpotify.start(true)
        timerTemp.start(true)
        
    }
    
    func mediaPlayerDidChange(trackProgress: Double) {
       
    }
    
    func mediaPlayerDidPause() {
        if SpotiyOrApple == "spot"{
       timerSpotify.pause()
        timerTemp.pause()
        }else{
            timerSpotify = nil
            timerTemp = nil
        }
    }
    
    func mediaPlayerDidResume() {
        print("activated")
        if SpotifySearch == false || self.popupItem.progress != 0{
       timerSpotify.start(true)
        timerTemp.start(true)
    }else{
            self.playSound(trackSpotify: TrackSpotifySearch!)
    }
    }
    
    func mediaPlayerDidFail(error: Error) {
        showDefaultError()
        MediaPlayer.shared.pause()
        //timerSpotify.cancel()
        //timerTemp.cancel()
       // updatePlayButton(playing: false)
    }
    
    func mediaPlayerDidFinishTrack() {
        //updatePlayButton(playing: false)
       // progressSlider.setValue(0, animated: false)
      print("finsih Track")
        timerSpotify.cancel()
        timerTemp.cancel()
        self.timeLabel.text = "--:--"
        self.popupItem.progress = 0.0
        if SpotifySearch == false {
        next_bar()
        }else{
            print("trueeeeee")
             self.popupItem.leftBarButtonItems![1].image = UIImage(named:"play")
        }
    }
    @objc func activateAudioSession() {
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch let error {
            print(error.localizedDescription)
        }
    }
    
    fileprivate func showDefaultError() {
        let alert = UIAlertController(title: "Oops", message: "Something went wrong. Please try again.", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        //present(alert, animated: true, completion: nil)
    }
    
}
