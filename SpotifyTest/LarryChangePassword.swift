//
//  LarryChangePassword.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 30/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
class LarryChangePassword : UIViewController {
    var token = ""
    
    @IBOutlet weak var valider_btn: UIButton!
    @IBOutlet weak var password: PaddingTextField!
    @IBOutlet weak var retype_password: PaddingTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    @IBAction func proceed(_ sender: Any) {
        if password.text != "" && self.retype_password.text != "" {
            if password.text == retype_password.text {
                
                
                let params: Parameters = [
                    "password": self.password.text!
                ]
                /*let header : HTTPHeaders = [
                    "Authorization" : "Bear"
                ] */
                let header: HTTPHeaders = [
                    "Authorization" : "Bearer " + token
                ]
                print(header.description)
                Alamofire.request("https://larry2.herokuapp.com/api/user/editPassword" , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        let c = JSON(response.data)
                        print(c)
                        if c["error"].arrayObject != nil {
                            UserDefaults.standard.setValue(c.rawString(), forKey: "User")
                            
                            UserDefaults.standard.synchronize()
                        let alert = UIAlertController(title: "Edit Password", message: "your password was successfully Changed", preferredStyle: .alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                            self.navigationController?.popToRootViewController(animated: true)
                            // self.navigationController?.popToRootViewController(animated: true)
                        }))
                        
                        // show the alert
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                        }
                }
            }else {
                ///password ne concorde pas
                let alert = UIAlertController(title: "Edit Password", message: "Passwords missmatch", preferredStyle: .alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                    // self.navigationController?.popToRootViewController(animated: true)
                }))
                
                // show the alert
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            }
        }else {
            let alert = UIAlertController(title: "Edit Password", message: "please fill all in the blanks", preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
                // self.navigationController?.popToRootViewController(animated: true)
            }))
            
            // show the alert
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
            ///champs vide
        }
        
    }
}
