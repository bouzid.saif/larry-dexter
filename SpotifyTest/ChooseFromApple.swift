//
//  ChooseFromApple.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 02/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
import MediaPlayer

class ChooseFromApple : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
     var height : CGFloat!
    var albums: [SongInfo] = []
    var songQuery: SongQuery = SongQuery()
     var local_remote = false
    var playlistNumX = 0
     var NavigationTitle = ""
    var tableMusic : [String] = [""]
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if albums.count != 0{
            return albums.count
        }else{
            return 0
        }
        
    }
    let gradientLayer = CAGradientLayer()
    func animateLayer(){
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        //gradientAnimation.delegate = self
        gradientAnimation.fromValue = [0.0,0.0,0.25]
        gradientAnimation.toValue = [0.75,1.0,1.0]
        gradientAnimation.duration = 3.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        
        gradientLayer.add(gradientAnimation, forKey: nil)
        
    }
    func CreateLayer() {
        let thirdthFrame = CGRect(x: 0 , y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: 13)
        let secondFrame = CGRect(x: 0  , y: thirdthFrame.width / 2 , width: thirdthFrame.width / 2 , height: thirdthFrame.height )
        let firstUIView  = UIView(frame: thirdthFrame)
        
        gradientLayer.colors = [UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor, UIColor(red: 196 / 255 , green: 70 / 255, blue: 107 / 255, alpha: 1).cgColor,UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor ]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.locations = [0.0 , 0.75, 1.0 ]
        
        let secondLabel = UILabel(frame: secondFrame)
        secondLabel.text = "Early release"
        secondLabel.textAlignment = .center
        secondLabel.textColor = UIColor.white
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.font = UIFont.systemFont(ofSize: 12.0)
        firstUIView.addSubview(secondLabel)
        self.navigationController?.navigationBar.addSubview(firstUIView)
        gradientLayer.frame = firstUIView.frame
        firstUIView.layer.insertSublayer(gradientLayer, at: 0)
        
        let widthConstraint = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.navigationController?.navigationBar.frame.width)!)
        secondLabel.addConstraint(widthConstraint)
        let heightConstraint = NSLayoutConstraint(item: secondLabel, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: thirdthFrame.height)
        secondLabel.addConstraint(heightConstraint)
        let xConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerX, relatedBy: .equal, toItem: firstUIView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerY, relatedBy: .equal, toItem: firstUIView, attribute: .centerY, multiplier: 1, constant: 0)
        firstUIView.addConstraint(xConstraint)
        firstUIView.addConstraint(yConstraint)
        animateLayer()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        //if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
        if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
            (cell?.viewWithTag(54) as! UIImageView).isHidden = false
            var verif = false
            for a in self.tableMusic {
                if a == String(indexPath.row) {
                    verif = true
                }
            }
            if verif == false {
                self.tableMusic.append(String(indexPath.row))
               
               
                StaticPlaylist.sharedInstance.addToMusic(music: PlaylistMusicSaif(id: String(describing:self.albums[indexPath.row].songId), title: self.albums[indexPath.row].songTitle, artist: self.albums[indexPath.row].artistName, image: "", provider: "apple"))
            }
        }else {
            //self.collection.delegate?.collectionView!(self.collection, didDeselectItemAt: indexPath)
            (cell?.viewWithTag(54) as! UIImageView).isHidden = true
            //self.collection.reloadItems(at: [indexPath])
            var i = -1
            for a in self.tableMusic {
                i = i + 1
                if a == String(indexPath.row) {
                    
                    self.tableMusic.remove(at: i)
                    StaticPlaylist.sharedInstance.removerMusic(music: PlaylistMusicSaif(id: String(describing: self.albums[indexPath.row].songId), title: self.albums[indexPath.row].songTitle, artist: self.albums[indexPath.row].artistName, image: "", provider: "apple"))
                    i = i - 1
                }
                
            }
            self.collection.deselectItem(at: indexPath, animated: false)
        }
        //self.collection.reloadItems(at: [indexPath])
        // }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        // if (cell?.viewWithTag(54) as! UIImageView).isHidden == true {
        (cell?.viewWithTag(54) as! UIImageView).isHidden = true
        //self.collection.reloadItems(at: [indexPath])
        var i = -1
        for a in self.tableMusic {
            i = i + 1
            if a == String(indexPath.row) {
                
                self.tableMusic.remove(at: i)
                StaticPlaylist.sharedInstance.removerMusic(music: PlaylistMusicSaif(id: String(describing: self.albums[indexPath.row].songId), title: self.albums[indexPath.row].songTitle, artist: self.albums[indexPath.row].artistName, image: "", provider: "apple"))
                i = i - 1
            }
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "musicCell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        let icloud_image = cell.viewWithTag(200) as! UIImageView
         let ImageSelect = cell.viewWithTag(54) as! UIImageView
        var verif = false
        for a in self.tableMusic {
            if a == String(indexPath.row) {
                verif = true
            }
        }
        ImageSelect.isHidden = verif == true ? false : true
        print(indexPath.row)
        print("title:",albums[indexPath.row].songTitle)
        title.text =  albums[indexPath.row].songTitle
        artist.text = albums[indexPath.row].artistName
        let songId: NSNumber = albums[indexPath.row].songId
        imageview.image = UIImage(named:"default_song")
        if local_remote {
            let item: MPMediaItem = songQuery.getItem2( songId: songId,row: indexPath.row,playlistNum: self.playlistNumX )
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                
            }
            if item.assetURL == nil{
                icloud_image.isHidden = false
                
                
                
            }
        }else{
            let item: MPMediaItem = songQuery.getItem( songId: songId )
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                
            }
        }
        
        return cell
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        for const in cell.constraints {
            const.isActive = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         height = self.collection.collectionViewLayout.collectionViewContentSize.height
        self.title = NavigationTitle
        self.collection.delegate = self
        self.collection.dataSource = self
        self.collection.allowsMultipleSelection = true
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                if self.local_remote {
                    self.albums = self.songQuery.get(songCategory: "Playlists", collection: self.collection,playlistNum: self.playlistNumX )
                    for q in StaticPlaylist.sharedInstance.MusicPlaylist{
                        if q.provider == "apple" {
                            for i in 0 ... self.albums.count - 1 {
                                if q.song_id == String(describing:self.albums[i].songId) {
                                    // self.collection.delegate?.collectionView!(self.collection, didSelectItemAt: IndexPath(row: i, section: 0))
                                    self.tableMusic.append(String(i))
                                }
                            }
                        }
                    }
                }else{
                    self.albums = self.songQuery.get(songCategory: "Artist",collection : self.collection)
                    for q in StaticPlaylist.sharedInstance.MusicPlaylist{
                        if q.provider == "apple" {
                            for i in 0 ... self.albums.count - 1 {
                                if q.song_id == String(describing:self.albums[i].songId) {
                                    // self.collection.delegate?.collectionView!(self.collection, didSelectItemAt: IndexPath(row: i, section: 0))
                                    self.tableMusic.append(String(i))
                                }
                            }
                        }
                    }
                }
                // self.albums = self.songQuery.get(songCategory: "Playlists",collection : self.collection)
                DispatchQueue.main.async {
                    self.collection.reloadData()
                }
                
            } else {
                self.displayMediaLibraryError()
            }
        }
    }
    func displayMediaLibraryError() {
        
        var error: String
        switch MPMediaLibrary.authorizationStatus() {
        case .restricted:
            error = "Media library access restricted by corporate or parental settings"
        case .denied:
            error = "Media library access denied by user"
        default:
            error = "Unknown error"
        }
        
        let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        controller.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        present(controller, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
