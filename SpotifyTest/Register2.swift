//
//  Register2.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 27/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON
class Register2 : UIViewController {
    
    @IBOutlet weak var firstname: UITextField!
    
    @IBOutlet weak var lastname: UITextField!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var retype_password: UITextField!
    var username :String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func RegisterAction(_ sender: UIButton) {
        
         if firstname.text != "" && lastname.text != "" && email.text != "" && username != "" && password.text != "" && retype_password.text != "" {
            if password.text == retype_password.text {
                 SwiftSpinner.show("Registring...")
         let params: Parameters = [
         "user_name": username ,
         "password": password.text! ,
         "email" : email.text! ,
         "first_name" : firstname.text! ,
         "last_name" : lastname.text!
         ]
         Alamofire.request("https://larry2.herokuapp.com/api/user/register" , method: .post, parameters: params, encoding: JSONEncoding.default)
         .responseJSON { response in
             SwiftSpinner.hide()
         let a = JSON(response.data)
            print(a)
            if a["name"].stringValue != "ValidationError" {
                if (a["errmsg"].stringValue).contains("E11000") {
                    let alert = UIAlertController(title: "Register", message: "the email is already taken", preferredStyle: .alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    // show the alert
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }else {
         if a["status"].stringValue == "false"{
         /* let player = self.storyboard!.instantiateViewController(withIdentifier: "player2") as! player2
         self.navigationController!.pushViewController(player, animated: true) */
         
         
         }else if a["status"].stringValue == "true"{
         SwiftSpinner.hide()
            let alert = UIAlertController(title: "Register", message: "Welcome to larry " + self.username, preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) in
               self.navigationController?.popToRootViewController(animated: true)
            }))
            
            // show the alert
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
      
         }
                }
            }else {
                  SwiftSpinner.hide()
                let alert = UIAlertController(title: "Register", message: "the email is not in the correct format", preferredStyle: .alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                
                // show the alert
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            }
         /*
         self.comments =  self.comments.merged(other: json1)
         self.tableview.beginUpdates()
         self.tableview.insertRows(at: [IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0)], with: .automatic)
         self.verif = "yes"
         self.tableview.endUpdates()
         self.tableview.reloadData()
         self.tableview.setNeedsLayout()
         self.tableview.scrollToRow(at: IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
         */
         
         
                }
         
            }else {
                ////password
                  SwiftSpinner.hide()
                let alert = UIAlertController(title: "Register", message: "Passwords Missmatch", preferredStyle: .alert)
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                
                // show the alert
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            }
         }else{
         SwiftSpinner.hide()
         let alert = UIAlertController(title: "Register", message: "please fill all the champs", preferredStyle: .alert)
         // add an action (button)
         alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
         
         // show the alert
         DispatchQueue.main.async(execute: {
         self.present(alert, animated: true, completion: nil)
         })
            }
        }
        
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
}
