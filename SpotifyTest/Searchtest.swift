//
//  Searchtest.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 04/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AZSearchView
import MapleBacon
class Searchtest : UIViewController,UISearchBarDelegate {
    @IBOutlet weak var search : UISearchBar!
    var searchController: AZSearchViewController!
    var resultArray:[String] = []
    var result : JSONX = []
    var resultsImage: [String] = []
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let q  = searchBar.text
         searchController.show(in: self)
        //print(q)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        SocketIOManager.sharedInstance.search(Text: searchText, Spotify: "BQBSPSLZZ-HZ5WaT-HDb10-eAOInOctdodKcKJe70Bo13WNe6pJY8w5-zycXdO--KbSw61a7CoupYnZEX0CJ8blVrnMx8uuvHdDbFX0Ibp0xO4XSnk2Xsr_PS70YtLjI6TXWmOnX3KmoLXY", Apple: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldRN1ZOWTNRTDMifQ.eyJpYXQiOjE1MTIzOTkwMzEsImV4cCI6MTUxMzk3NjczMSwiaXNzIjoiNzk4OVk1REY1QyJ9.zIvQrDZ_fVSr4tuTKvrscwAs6bdXU8NmrW_PLzZ6k3tjOntphzlDz2yWX9Feuiw3yuQaafNBs8Qs8X4Aw7MdAw")
        SocketIOManager.sharedInstance.GetResults{ (results) -> Void in
        
            print(results)
        
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        search.delegate = self
        self.searchController = AZSearchViewController()
        self.searchController.delegate = self
        self.searchController.dataSource = self
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension Searchtest: AZSearchViewDelegate{
    
    func searchView(_ searchView: AZSearchViewController, didSearchForText text: String) {
        searchView.dismiss(animated: false, completion: nil)
    }
    
    func searchView(_ searchView: AZSearchViewController, didTextChangeTo text: String, textLength: Int) {
        self.resultArray.removeAll()
        if text == ""{
        searchView.reloadData()
        }
        SocketIOManager.sharedInstance.search(Text: text, Spotify: "BQBSPSLZZ-HZ5WaT-HDb10-eAOInOctdodKcKJe70Bo13WNe6pJY8w5-zycXdO--KbSw61a7CoupYnZEX0CJ8blVrnMx8uuvHdDbFX0Ibp0xO4XSnk2Xsr_PS70YtLjI6TXWmOnX3KmoLXY", Apple: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldRN1ZOWTNRTDMifQ.eyJpYXQiOjE1MTIzOTkwMzEsImV4cCI6MTUxMzk3NjczMSwiaXNzIjoiNzk4OVk1REY1QyJ9.zIvQrDZ_fVSr4tuTKvrscwAs6bdXU8NmrW_PLzZ6k3tjOntphzlDz2yWX9Feuiw3yuQaafNBs8Qs8X4Aw7MdAw")
        SocketIOManager.sharedInstance.GetResults{ (results) -> Void in
            self.result = []
            self.result = JSONX.parse(results.arrayValue[0].description)
            print("Results0:",results.arrayValue)
            //self.result = JSON(results["results"].arrayObject)
            print("***************")
            print("results:",self.result)
             print("***************")
            searchView.reloadData()
        }
        
        
    }
    
    func searchView(_ searchView: AZSearchViewController, didSelectResultAt index: Int, text: String) {
        searchView.dismiss(animated: true, completion: {
           // self.pushWithTitle(text: text)
        })
    }
}
extension Searchtest: AZSearchViewDataSource{
   
    
    
    func results() -> JSONX {
        return self.result
    }
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchView.cellIdentifier)
        cell?.textLabel?.text = self.result[indexPath.row]["songName"].stringValue
        if self.result[indexPath.row]["type"].stringValue == "apple" {
            cell?.imageView?.image = #imageLiteral(resourceName: "apple_music")
        }else if self.result[indexPath.row]["type"].stringValue == "spotify" {
            cell?.imageView?.image = #imageLiteral(resourceName: "Spotify-icon")
        }
       // cell?.imageView?.image = #imageLiteral(resourceName: "ic_history").withRenderingMode(.alwaysTemplate)
        //cell?.imageView?.tintColor = UIColor.gray
        cell?.contentView.backgroundColor = .white
        return cell!
    }

    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
}
