//
//  StaticPlaylist.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 02/01/2018.
//  Copyright © 2018 Seth Rininger. All rights reserved.
//

import Foundation
class PlaylistMusicSaif :  NSObject {
    
    public var song_id : String?
    public var  title: String?
    public var  artist: String?
    public var  image: String?
    public var  provider: String?

    init(id:String,title:String,artist:String,image:String,provider:String) {
        
    self.song_id = id
        self.title = title
        self.artist = artist
        self.image = image
        self.provider = provider
       
    }
    
}
