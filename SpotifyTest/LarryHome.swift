//
//  LarryHome.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 31/12/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import LNPopupController
import SwiftSpinner
import SwiftyJSON
import AZSearchView
import MapleBacon
import AVFoundation
import Spartan
class LarryHome : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var viewss: UIView!
    
     @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var search: UIBarButtonItem!
    @IBOutlet weak var collection: DynamicCollectionView!
    var resultArray:[String] = []
    var result : JSONX = []
    var resultsImage: [String] = []
     var height : CGFloat!
    var interval : Double = 0.0
    var playlists : JSON = []
     let gradientLayer = CAGradientLayer()
    @IBOutlet weak var youdont: UILabel!
    var searchController: AZSearchViewController!
    
    @IBAction func SearchNow(_ sender: UIBarButtonItem) {
        searchController.show(in: self)
    }
    
    @IBAction func go_to_inter(_ sender: Any) {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "inter-playlist") as! LarryInterPlaylist
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let q  = searchBar.text
        searchController.show(in: self)
        
        //print(q)
    }
    func loadData() {
        let abc = UserDefaults.standard.value(forKey: "Token") as! String
        let header: HTTPHeaders = [
            "Authorization" : "Bearer " + abc
        ]
        Alamofire.request("https://larry2.herokuapp.com/api/user/playlists" , method: .get, encoding: JSONEncoding.default,headers : header)
            .responseJSON { response in
                let q = JSON(response.data)
                print(q)
                self.playlists = q
                if self.playlists.arrayObject?.count != nil {
                    if self.playlists.arrayObject!.count > 0 {
                        self.collection.isHidden = false
                        self.youdont.isHidden = true
                        self.collection.reloadData()
                    }
                }
                
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    func animateLayer(){
        let gradientAnimation = CABasicAnimation(keyPath: "locations")
        //gradientAnimation.delegate = self
        gradientAnimation.fromValue = [0.0,0.0,0.25]
        gradientAnimation.toValue = [0.75,1.0,1.0]
        gradientAnimation.duration = 3.0
        gradientAnimation.autoreverses = true
        gradientAnimation.repeatCount = Float.infinity
        
        gradientLayer.add(gradientAnimation, forKey: nil)
        
    }
    func CreateLayer() {
        let thirdthFrame = CGRect(x: 0 , y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: 13)
        let secondFrame = CGRect(x: 0  , y: thirdthFrame.width / 2 , width: thirdthFrame.width / 2 , height: thirdthFrame.height )
        let firstUIView  = UIView(frame: thirdthFrame)
        
        gradientLayer.colors = [UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor, UIColor(red: 196 / 255 , green: 70 / 255, blue: 107 / 255, alpha: 1).cgColor,UIColor(red: 48 / 255, green: 62 / 255, blue: 103 / 255, alpha: 1).cgColor ]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.locations = [0.0 , 0.75, 1.0 ]
        
        let secondLabel = UILabel(frame: secondFrame)
        secondLabel.text = "Early Release"
        secondLabel.textAlignment = .center
        secondLabel.textColor = UIColor.white
        secondLabel.translatesAutoresizingMaskIntoConstraints = false
        secondLabel.font = UIFont.systemFont(ofSize: 12.0)
        firstUIView.addSubview(secondLabel)
        self.navigationController?.navigationBar.addSubview(firstUIView)
        gradientLayer.frame = firstUIView.frame
        firstUIView.layer.insertSublayer(gradientLayer, at: 0)
        
        let widthConstraint = NSLayoutConstraint(item: secondLabel, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.navigationController?.navigationBar.frame.width)!)
        secondLabel.addConstraint(widthConstraint)
        let heightConstraint = NSLayoutConstraint(item: secondLabel, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: thirdthFrame.height)
        secondLabel.addConstraint(heightConstraint)
        let xConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerX, relatedBy: .equal, toItem: firstUIView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: secondLabel, attribute: .centerY, relatedBy: .equal, toItem: firstUIView, attribute: .centerY, multiplier: 1, constant: 0)
        firstUIView.addConstraint(xConstraint)
        firstUIView.addConstraint(yConstraint)
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       CreateLayer()
         //secondLabel.center = CGPoint(x: (self.navigationController?.navigationBar.frame.width)! / 2, y: firstUIView.frame.height / 2)
        //secondLabel.addConstraint(N)
       // secondLabel.centerXAnchor.constraint(equalTo: firstUIView.centerXAnchor).isActive = true
       // secondLabel.centerYAnchor.constraint(equalTo: firstUIView.centerYAnchor).isActive = true
        //self.navigationController?.navigationBar.addSubview(viewss)
      
        self.viewss.backgroundColor = UIColor.red
        self.searchController = AZSearchViewController()
        self.searchController.delegate = self
        self.searchController.dataSource = self
        self.searchController.statusBarStyle = .lightContent
        self.searchController.keyboardAppearnce = .dark
        self.searchController.searchBarPlaceHolder = "Search For Songs"
        
        
        self.searchController.navigationBarClosure = { bar in
            //The navigation bar's background color
            bar.barTintColor = UIColor(red: 0/255, green: 144/255, blue: 81/255, alpha: 1)
            
            //The tint color of the navigation bar
            bar.tintColor = UIColor.lightGray
        }
        let item = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(AllMusic.close(sender:)))
        item.tintColor = .white
        self.searchController.navigationItem.rightBarButtonItem = item
        //UITabBar.appearance().tintColor = UIColor.white
        // self.tabBarController?.tabBar.items![0].selectedImage.
        self.tabBarController?.tabBar.tintColor = UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)
        self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)], for: UIControlState.selected)
        
        collection.delegate = self
        collection.dataSource = self
      //  Spartan.authorizationToken = SPTAuth.defaultInstance().session.accessToken
       /* Spartan.loggingEnabled = true
        _ = Spartan.getMe(success: { (user) in
            // Do something with the user object
            self.userr = user.id as! String
            _ = Spartan.getUsersPlaylists(userId: user.id as! String, limit: 20, offset: 0, success: { (pagingObject) in
                // Get the playlists via pagingObject.playlists
                self.spotifyPlaylist =   pagingObject.items
                
                self.collection.reloadData()
            }, failure: { (error) in
                print(error)
            })
        }, failure: { (error) in
            print(error)
        }) */
        
        height = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        if #available(iOS 10.0, *) {
            //  flowlayout.estimatedItemSize =  CGSize(width: 96, height: 133)
            
            // flowlayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        } else {
            // Fallback on earlier versions
        }
        self.collection.isHidden = true
        // Do any additional setup after loading the view.
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.playlists.arrayObject != nil {
            return (self.playlists.arrayObject?.count)!
        }else {
            return 0
        }
    }
    @objc func close(sender:AnyObject?){
        searchController.searchBar.text = ""
        self.result = []
        searchController.reloadData()
        searchController.dismiss(animated: true)
    }
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        //imageview.contentMode = .scaleAspectFill
        //imageview.clipsToBounds = true
        title.text = self.playlists[indexPath.row]["name"].stringValue
        artist.text = String(self.playlists[indexPath.row]["songs"].arrayObject!.count) + " titles"
        //print("spotify playlist :",spotifyPlaylist[indexPath.row].images.count)
        if self.playlists[indexPath.row]["image"].stringValue != ""  {
            
            imageview.setImage(withUrl: URL(string: self.playlists[indexPath.row]["image"].stringValue)!, placeholder: UIImage(named:"default_song"), cacheScaled: true , completion: { (ImageInstance, error) in
                let i = ImageInstance?.image
                
                imageview.image = i?.scaleImage(toSize: CGSize(width: 95, height: 96))
            })
        
        }else{
            imageview.image = UIImage(named:"default_song")
        }
        print(imageview.image?.size)
        
        //cell.bounds.size.height = (title.bounds.size.height + artist.bounds.size.height + imageview.bounds.size.height) + 15 */
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AllMusic", for: indexPath)
        
        return cell
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
          animateLayer()
        self.tabBarController?.tabBar.tintColor = UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)
        self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor(red: 1/255.0, green: 185/255.0, blue: 224/255.0, alpha: 1.0)], for: UIControlState.selected)
        self.collection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        self.tabBarController?.tabBar.isHidden = false
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    let view = self.storyboard?.instantiateViewController(withIdentifier: "LarryHomeDetails") as! LarryHomeDetails
        view.Musics = self.playlists[indexPath.row]["songs"]
        self.navigationController?.pushViewController(view, animated: true)
        // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
      /*  let Tracksspoti = self.storyboard?.instantiateViewController(withIdentifier: "TracksSpotify") as! TracksSpotify
        Tracksspoti.userId = self.userr
        print((spotifyPlaylist[indexPath.row].id) as! String)
        Tracksspoti.playlistId = (spotifyPlaylist[indexPath.row].id) as! String
        self.navigationController?.pushViewController(Tracksspoti, animated: true) */
        
        //7el el fou9
        //  let appdelegate = UIApplication.shared.delegate as! AppDelegate
        /*
         tabBarController?.presentPopupBar(withContentViewController: player, animated: true, completion: nil)
         tabBarController?.popupBar.popupItem?.title = songs[indexPath.row]
         tabBarController?.popupBar.popupItem?.subtitle = artists[indexPath.row]
         tabBarController?.popupBar.popupItem?.image = UIImage(named: "default_song")
         tabBarController?.popupBar.progressViewStyle = .top
         
         tabBarController?.popupBar.backgroundStyle = .light
         tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
         
         //tabBarController?.popupBar.progressViewColor  = UIColor.red
         tabBarController?.popupBar.popupItem?.progress = Float(0.5)
         player.SpotiyOrApple = "spot"
         player.playSound(id: indexPath.row, wher: "begin") */
        // tabBarController?.popupBar.tintColor = UIColor(white: 38.0 / 255.0, alpha: 1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension LarryHome: AZSearchViewDelegate{
    
    func searchView(_ searchView: AZSearchViewController, didSearchForText text: String) {
        searchView.dismiss(animated: false, completion: nil)
    }
    
    func searchView(_ searchView: AZSearchViewController, didTextChangeTo text: String, textLength: Int) {
        self.result = []
        if text != "" {
            SocketIOManager.sharedInstance.search(Text: text, Spotify: SPTAuth.defaultInstance().session.accessToken, Apple: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldRN1ZOWTNRTDMifQ.eyJpYXQiOjE1MTIzOTkwMzEsImV4cCI6MTUxMzk3NjczMSwiaXNzIjoiNzk4OVk1REY1QyJ9.zIvQrDZ_fVSr4tuTKvrscwAs6bdXU8NmrW_PLzZ6k3tjOntphzlDz2yWX9Feuiw3yuQaafNBs8Qs8X4Aw7MdAw")
            SocketIOManager.sharedInstance.GetResults{ (results) -> Void in
                self.result = []
                self.result = JSONX.parse(results.arrayValue[0].description)
                print("Results0:",results.arrayValue)
                //self.result = JSON(results["results"].arrayObject)
                print("***************")
                print("results:",self.result)
                print("***************")
                searchView.reloadData()
            }
        }
        
        if text == ""{
            self.result = []
            searchView.reloadData()
        }
        
    }
    
    func searchView(_ searchView: AZSearchViewController, didSelectResultAt index: Int, text: String) {
        searchView.searchBar.text = ""
        if self.result[index]["type"].stringValue == "apple"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "apple"
            
            if MediaPlayer.shared.isPlaying {
                MediaPlayer.shared.pause()
                playerLarry.timerSpotify.cancel()
                // playerLarry.timer.cancel()
                playerLarry.timerTemp.cancel()
                
                playerLarry.timerSpotify = nil
                playerLarry.timer = nil
                playerLarry.timerTemp = nil
                
            }
            
            
            print("localMusicStopped")
            playerLarry.playlistApple = 0
            playerLarry.SongApple = 0
            playerLarry.local_remote = true
            playerLarry.appleSearch = true
            
            let app = SongInfo(albumTitle: self.result[index]["album"].stringValue, artistName: self.result[index]["artistName"].stringValue, songTitle: self.result[index]["songName"].stringValue, songId: NSNumber(value:Int(self.result[index]["id"].stringValue)!))
            playerLarry.albums = [app]
            playerLarry.whernow = IndexPath(row: 0, section: 0).row
            tabBarController?.popupBar.popupItem?.progress = Float(0.0)
            
            
            
            playerLarry.collection = self
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            //playerLarry.playLocal(album:[app],at: IndexPath(row: 0, section: 0).row)
            playerLarry.playAppleSearch(result: self.result,index: index)
            tabBarController?.popupBar.progressViewStyle = .top
            
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
            self.result = []
            self.searchController.reloadData()
        }else if self.result[index]["type"].stringValue == "spotify"{
            let playerLarry = player2.shared
            
            playerLarry.SpotiyOrApple = "spot"
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            // let musicPlayerController = appDelegate.musicPlayerManager.shared
            
            if MusicPlayerManager.shared.musicPlayerController.playbackState == .playing ||  MusicPlayerManager.shared.musicPlayerController.playbackState == .paused {
                MusicPlayerManager.shared.musicPlayerController.stop()
                print("sure playing local")
                playerLarry.duration = 0.0
                
                playerLarry.timerApple = nil
                playerLarry.timer = nil
                
                
                self.deactivateAudioSession()
                // MediaPlayer.shared.configurePlayer(authSession: SPTAuth.defaultInstance().session, id: SPTAuth.defaultInstance().clientID)
            }
            
            playerLarry.SpotifySearch = true
            
            //playerLarry.timerSpotify.cancel()
            tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
            tabBarController?.popupBar.progressViewStyle = .top
            tabBarController?.popupInteractionStyle = .drag
            tabBarController?.popupBar.backgroundStyle = .light
            tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
           _ = Spartan.getTracks(ids: [ self.result[index]["id"].stringValue], market: .us, success: { (track) in
                
                let a : [Track] = track
                
                playerLarry.x1 = a[0].name
                playerLarry.x2 = a[0].artists[0].name
                playerLarry.x3 = UIImage(named:"default_song")!
                
                //playerLarry.albumsSpotify = tracks
                playerLarry.whernowSpotify = index
                self.tabBarController?.popupBar.popupItem?.progress = Float(0.0)
                // print("id:",tracks[indexPath.row].track.id as! String)
                //playerLarry.timerSpotify.cancel()
                self.tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
                self.tabBarController?.popupBar.progressViewStyle = .top
                self.tabBarController?.popupInteractionStyle = .drag
                self.tabBarController?.popupBar.backgroundStyle = .light
                self.tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
                print(a[0].popularity)
                self.interval = Double(a[0].durationMs / 1000)
                self.tabBarController?.popupBar.popupItem?.title =  a[0].name
                var artistname = ""
                for art in a[0].artists{
                    artistname = artistname + art.name + ", "
                }
                artistname.removeLast()
                artistname.removeLast()
                self.tabBarController?.popupBar.popupItem?.subtitle = artistname
                artistname = ""
                let url = URL(string: a[0].album.images[0].url)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.tabBarController?.popupBar.popupItem?.image = UIImage(data: data!)
                    }
                }
                // self.tabBarController?.popupBar.popupItem?.image
                playerLarry.collection = self
                playerLarry.tt = a
                playerLarry.durationspotify = a[0].durationMs
                playerLarry.TrackSpotifySearch = a[0]
                playerLarry.playSound(trackSpotify: a[0])
                
            }, failure: { (error) in
                print(error)
            })
            self.result = []
            self.searchController.reloadData()
        }
        
        searchView.dismiss(animated: true, completion: {
            // self.pushWithTitle(text: text)
        })
    }
    @objc func deactivateAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }
        catch let error {
            print(error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                UIApplication.shared.beginReceivingRemoteControlEvents()
                
                print("AVAudioSession is Active")
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }
}
extension LarryHome: AZSearchViewDataSource{
    
    
    
    func results() -> JSONX {
        if searchController.searchBar.text != ""{
            return self.result
        }else{
            self.result = []
            return self.result
        }
    }
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchView.cellIdentifier)
        cell?.textLabel?.text = self.result[indexPath.row]["songName"].stringValue + " - " + self.result[indexPath.row]["artistName"].stringValue
        if self.result[indexPath.row]["type"].stringValue == "apple" {
            cell?.imageView?.image = #imageLiteral(resourceName: "apple_music")
        }else if self.result[indexPath.row]["type"].stringValue == "spotify" {
            cell?.imageView?.image = #imageLiteral(resourceName: "Spotify-icon")
        }
        // cell?.imageView?.image = #imageLiteral(resourceName: "ic_history").withRenderingMode(.alwaysTemplate)
        //cell?.imageView?.tintColor = UIColor.gray
        cell?.contentView.backgroundColor = .white
        return cell!
    }
    
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
}
extension UIImage {
    
    func scaleImage(toSize newSize: CGSize) -> UIImage? {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext() {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}
