//
//  login.swift
//  Alamofire
//
//  Created by Bouzid saif on 24/09/2017.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: newValue!])
        }
    }
}
class loginn : UIViewController, WebViewControllerDelegate {
    func webViewControllerDidFinish(_ controller: WebViewController) {
        // User tapped the close button. Treat as auth error
    }
    
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var create: UILabel!
    
    @IBOutlet weak var password: UITextField!
    @objc var authViewController: UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        login.placeHolderColor = UIColor.white
        password.placeHolderColor = UIColor.white
        /*login.layer.cornerRadius = 25.0
        login.layer.borderWidth = 0
        login.layer.borderColor = UIColor.clear.cgColor
        login.backgroundColor = UIColor(hexString: "#F5F5F5", withAlpha: 0.2) */
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.gonext(sender:)))
        self.create.isUserInteractionEnabled = true
        self.create.addGestureRecognizer(tapRecognizer)
         NotificationCenter.default.addObserver(self, selector: #selector(self.sessionUpdatedNotification), name: NSNotification.Name(rawValue: "sessionUpdated"), object: nil)
     /*   self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        //self.navigationController?.navigationBar.barStyle = UIBarStyle.c
        self.navigationController?.navigationBar.tintColor = UIColor.white */
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      self.navigationController?.isNavigationBarHidden = true
          UIApplication.shared.statusBarStyle = .lightContent
    }
    @objc func gonext(sender: UITapGestureRecognizer){
        let register = self.storyboard?.instantiateViewController(withIdentifier: "register") as! register
        self.navigationController?.pushViewController(register, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logg(_ sender: Any) {
        SwiftSpinner.show("Loading...")
        if login.text != "" && password.text != "" {
            let params: Parameters = [
                "user_name": login.text! ,
                "password": password.text!
            ]
            Alamofire.request("https://larry2.herokuapp.com/api/user/login" , method: .post, parameters: params, encoding: JSONEncoding.default)
                .responseJSON { response in
                    let a = JSON(response.data)
                    print(a)
                    if a["status"].stringValue == "false"{
                    /* let player = self.storyboard!.instantiateViewController(withIdentifier: "player2") as! player2
                     self.navigationController!.pushViewController(player, animated: true) */
                    
                    SwiftSpinner.hide()
                        let alert = UIAlertController(title: "Login", message: "Wrong username/password", preferredStyle: .alert)
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                        
                        // show the alert
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }else if a["status"].stringValue == "true"{
                        SwiftSpinner.hide()
                        UserDefaults.standard.setValue(a["user"].rawString(), forKey: "User")
                        UserDefaults.standard.setValue(a["token"].stringValue, forKey: "Token")
                        UserDefaults.standard.synchronize()
                        let auth = SPTAuth.defaultInstance()
                        if auth!.session == nil {
                            print("nil")
                            let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabar") as! UITabBarController
                            
                            let alert = UIAlertController(title: "Welcome", message: "Do you want to connect your spootify account?", preferredStyle: .alert)
                            // add an action (button)
                            
                            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                                
                                if SPTAuth.supportsApplicationAuthentication() {
                                    print("jé houni")
                                    let login = SPTAuth.loginURL(forClientId: auth!.clientID, withRedirectURL: auth!.redirectURL, scopes: auth!.requestedScopes, responseType: "code")
                                    UIApplication.shared.openURL(login!)
                                    
                                } else {
                                    // print("jé houni")
                                    self.authViewController = self.getAuthViewController(withURL: SPTAuth.defaultInstance().spotifyWebAuthenticationURL())
                                    self.definesPresentationContext = true
                                    self.present(self.authViewController!, animated: true, completion: nil)
                                }
                                
                                
                            }))
                            alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: {(_) -> Void in
                                     home.viewControllers?.remove(at: 0)
                                self.navigationController?.pushViewController(home, animated: true)
                            }))
                            // show the alert
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        }else{
                            
                            if auth!.session.isValid() == false{
                                if auth!.hasTokenRefreshService{
                                    SwiftSpinner.show("Refreshing Token...")
                                    self.renewTokenAndShowPlayer()
                                }else{
                                    let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabar") as! UITabBarController
                                    
                                    let alert = UIAlertController(title: "Welcome", message: "Do you want to reconnect your spotify account?", preferredStyle: .alert)
                                    // add an action (button)
                                    
                                    alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { (_) -> Void in
                                        
                                        if SPTAuth.supportsApplicationAuthentication() {
                                            print("jé houni")
                                            UIApplication.shared.openURL(auth!.spotifyAppAuthenticationURL())
                                            // let login = SPTAuth.loginURL(forClientId: auth!.clientID, withRedirectURL: auth!.redirectURL, scopes: auth!.requestedScopes, responseType: "code")
                                            // UIApplication.shared.openURL(login!)
                                            
                                        } else {
                                            // print("jé houni")
                                            self.authViewController = self.getAuthViewController(withURL: SPTAuth.defaultInstance().spotifyWebAuthenticationURL())
                                            self.definesPresentationContext = true
                                            self.present(self.authViewController!, animated: true, completion: nil)
                                            
                                        }
                                        
                                        
                                    }))
                                    alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: {(_) -> Void in
                                        home.viewControllers?.remove(at: 0)
                                        self.navigationController?.pushViewController(home, animated: true)
                                    }))
                                    // show the alert
                                    DispatchQueue.main.async(execute: {
                                        self.present(alert, animated: true, completion: nil)
                                    })
                                }
                            } else{
                                let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabar") as! UITabBarController
                                
                                guard let session = SPTAuth.defaultInstance().session else { return }
                                MediaPlayer.shared.configurePlayer(authSession: session, id: SPTAuth.defaultInstance().clientID)
                                
                                self.navigationController?.pushViewController(home, animated: true)
                            }
                            
                        }
                    }
                    /*
                     self.comments =  self.comments.merged(other: json1)
                     self.tableview.beginUpdates()
                     self.tableview.insertRows(at: [IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0)], with: .automatic)
                     self.verif = "yes"
                     self.tableview.endUpdates()
                     self.tableview.reloadData()
                     self.tableview.setNeedsLayout()
                     self.tableview.scrollToRow(at: IndexPath(row: (self.comments.arrayObject?.count)!-1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                     */
                    
                 SwiftSpinner.hide()
                    
            }
        }else{
            SwiftSpinner.hide()
            let alert = UIAlertController(title: "Login", message: "Please fill all in the blanks", preferredStyle: .alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            
            // show the alert
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
        
    }
    }
    @objc func renewTokenAndShowPlayer() {
        let auth = SPTAuth.defaultInstance()
        
        let params: Parameters = [
            "grant_type": "refresh_token",
            "refresh_token": auth!.session.encryptedRefreshToken
        ]
       // print("error:",auth!.session.encryptedRefreshToken)
        Alamofire.request(auth!.tokenRefreshURL.absoluteString , method: .post, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { response in
                SwiftSpinner.hide()
              let a = JSON(response.data)
                print(a)
                let session =  SPTSession(userName: "754dd512a3ee424eb1311ce013b778f3", accessToken: a["access_token"].stringValue, encryptedRefreshToken: auth!.session.encryptedRefreshToken, expirationDate: Date(timeIntervalSinceNow: TimeInterval(a["expires_in"].intValue)))
                let userDefaults = UserDefaults.standard
                let sessionData = NSKeyedArchiver.archivedData(withRootObject: session)
                
                userDefaults.set(sessionData, forKey: "SpotifySession")
                userDefaults.synchronize()
                SPTAuth.defaultInstance().session = session
                print("refresh:",SPTAuth.defaultInstance().session.encryptedRefreshToken)
                 NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "startCount"), object: self)
                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "sessionUpdated"), object: self)
        }
     
    }
    
    @objc func getAuthViewController(withURL url: URL) -> UIViewController {
        let webView = WebViewController(url: url)
        webView.delegate = self
        
        return UINavigationController(rootViewController: webView)
    }
    @objc func sessionUpdatedNotification(_ notification: Notification) {
       let home = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabar") as! UITabBarController
        let auth = SPTAuth.defaultInstance()
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if auth!.session != nil && auth!.session.isValid() {
            guard let session = SPTAuth.defaultInstance().session else { return }
            MediaPlayer.shared.configurePlayer(authSession: session, id: SPTAuth.defaultInstance().clientID)
           
            self.navigationController?.pushViewController(home, animated: true)
        }
        else {
           
            print("*** Failed to log in")
        }
    }
    
    
}

