//
//  LocalMusic.swift
//  SpotifyTest
//
//  Created by Bouzid saif on 13/10/2017.
//  Copyright © 2017 Seth Rininger. All rights reserved.
//

import Foundation
import UIKit
import LNPopupController
import MediaPlayer
import AudioPlayerManager
class LocalMusic: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    @IBOutlet weak var collection: DynamicCollectionView!
    @IBOutlet weak var flowlayout: UICollectionViewFlowLayout!
    @IBOutlet weak var progressView: UIProgressView!
    var NavigationTitle = ""
    var height : CGFloat!
    var i = -1
    @objc var songs : [String] = ["Thunder","Something Just Like This","What About Else"]
    @objc var artists : [String] = ["Imagine Dragons","Imagine Dragons","Pink"]
    let accessibilityDateComponentsFormatter = DateComponentsFormatter()
    var timer : Timer?
    var albums: [SongInfo] = []
    var Playlists: [PlaylistInfo] = []
    var songQuery: SongQuery = SongQuery()
    var playlistQuery : PlaylistQuery = PlaylistQuery()
    var interval : Double = 0.0
    var local_remote = false
    var playlistNumX = 0
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if albums.count != 0{
            return albums.count
        }else{
            return 0
        }
     
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if  albums.count != 0 {
            return 1
        }else{
            return 0
        }
    
            
    
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "musicCell", for: indexPath)
        let title = cell.viewWithTag(2) as! UILabel
        let  artist = cell.viewWithTag(3) as! UILabel
        let imageview = cell.viewWithTag(1) as! UIImageView
        let icloud_image = cell.viewWithTag(200) as! UIImageView
        print(indexPath.row)
        print("title:",albums[indexPath.row].songTitle)
        title.text =  albums[indexPath.row].songTitle
        artist.text = albums[indexPath.row].artistName
        let songId: NSNumber = albums[indexPath.row].songId
        imageview.image = UIImage(named:"default_song")
        if local_remote {
            let item: MPMediaItem = songQuery.getItem2( songId: songId,row: indexPath.row,playlistNum: self.playlistNumX )
            if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
                imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
                if imageview.image == nil {
                    imageview.image = UIImage(named:"default_song")
                }
                
            }
            if item.assetURL == nil{
                icloud_image.isHidden = false
                
            
                
            }
        }else{
        let item: MPMediaItem = songQuery.getItem( songId: songId )
        if  let imageSound: MPMediaItemArtwork = item.value( forProperty: MPMediaItemPropertyArtwork ) as? MPMediaItemArtwork {
           imageview.image = imageSound.image(at: CGSize(width: imageview.frame.size.width, height: imageview.frame.size.height))
            
        }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        for const in cell.constraints {
            const.isActive = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if albums[indexPath.row].artistName != "Item Not Available" {
        
       // tabBarController?.dismissPopupBar(animated: true, completion: nil)
         let playerLarry = player2.shared
        
        playerLarry.SpotiyOrApple = "apple"
        playerLarry.appleSearch = false
        if MediaPlayer.shared.isPlaying {
            MediaPlayer.shared.pause()
            playerLarry.timerSpotify.cancel()
           // playerLarry.timer.cancel()
            playerLarry.timerTemp.cancel()
            
            playerLarry.timerSpotify = nil
            playerLarry.timer = nil
            playerLarry.timerTemp = nil
            
        }
        if local_remote{
           
            print("localMusicStopped")
            playerLarry.playlistApple = self.playlistNumX
            playerLarry.SongApple = indexPath.row
            playerLarry.local_remote = true
        }else{
            playerLarry.local_remote = false
        }
        
        
        playerLarry.albums = albums
        playerLarry.whernow = indexPath.row
         tabBarController?.popupBar.popupItem?.progress = Float(0.0)
       
        
        
        playerLarry.collection = self
          tabBarController?.presentPopupBar(withContentViewController: playerLarry, animated: true, completion: nil)
         playerLarry.playLocal(album:albums,at: indexPath.row)
       
        tabBarController?.popupBar.progressViewStyle = .top
        
        tabBarController?.popupBar.backgroundStyle = .light
        tabBarController?.popupBar.tintColor = UIColor(hexString: "#2979FF")
        
       
        }else{
            let alert = UIAlertController(title: "Item Not Available", message: "This item can't be played", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
       
        
        // tabBarController?.popupBar.tintColor = UIColor(white: 38.0 / 255.0, alpha: 1.0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = UIColor.magenta
          if self.tabBarController?.tabBar.items?.count == 2 {
        self.tabBarController?.tabBar.items![1].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
            ], for: UIControlState.selected)
        i = -1
          }else {
            self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
                ], for: UIControlState.selected)
            i = -1
        }
       // self.collection.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)  {
        
        let newHeight : CGFloat = self.collection.collectionViewLayout.collectionViewContentSize.height
        
        var frame : CGRect! = self.collection.frame
        frame.size.height = newHeight
        
        self.collection.frame = frame
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.tintColor = UIColor.magenta
         if self.tabBarController?.tabBar.items?.count == 2 {
        self.tabBarController?.tabBar.items![1].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
            ], for: UIControlState.selected)
         }else {
            self.tabBarController?.tabBar.items![0].setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.magenta
                ], for: UIControlState.selected)
        }
        collection.delegate = self
        collection.dataSource = self
        height = self.collection.collectionViewLayout.collectionViewContentSize.height
        self.title = NavigationTitle
      
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                if self.local_remote {
                self.albums = self.songQuery.get(songCategory: "Playlists", collection: self.collection,playlistNum: self.playlistNumX )
                }else{
                    self.albums = self.songQuery.get(songCategory: "Artist",collection : self.collection)
                }
               // self.albums = self.songQuery.get(songCategory: "Playlists",collection : self.collection)
                DispatchQueue.main.async {
                   self.collection.reloadData()
                }
                
            } else {
                self.displayMediaLibraryError()
            }
        }
    }
    func displayMediaLibraryError() {
        
        var error: String
        switch MPMediaLibrary.authorizationStatus() {
        case .restricted:
            error = "Media library access restricted by corporate or parental settings"
        case .denied:
            error = "Media library access denied by user"
        default:
            error = "Unknown error"
        }
        
        let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        controller.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }))
        present(controller, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
