//
//  UIColor+Blend.swift
//  SWGauge
//
//  Created by David Pelletier on 2015-03-06.
//  Copyright (c) 2015 Petr Korolev. All rights reserved.
//

import UIKit

func + (left: UIColor, right: UIColor) -> UIColor {
    var leftRGBA = [CGFloat](repeating: 0.0, count: 4)
    var rightRGBA = [CGFloat](repeating: 0.0, count: 4)
    var left1 = leftRGBA[0]
    var left2 = leftRGBA[1]
    var left3 = leftRGBA[2]
    var left4 = leftRGBA[3]
    var right1 = rightRGBA[0]
    var right2 = rightRGBA[1]
    var right3 = rightRGBA[2]
    var right4 = rightRGBA[3]
    left.getRed(&left1, green: &left2, blue: &left3, alpha: &left4)
    right.getRed(&right1, green: &right2, blue: &right3, alpha: &right4)
    
    return UIColor(
        red: max(leftRGBA[0], rightRGBA[0]),
        green: max(leftRGBA[1], rightRGBA[1]),
        blue: max(leftRGBA[2], rightRGBA[2]),
        alpha: max(leftRGBA[3], rightRGBA[3])
    )
}

func * (left: CGFloat, right: UIColor) -> UIColor {
    var rightRGBA = [CGFloat](repeating: 0.0, count: 4)
    var right1 = rightRGBA[0]
    var right2 = rightRGBA[1]
    var right3 = rightRGBA[2]
    var right4 = rightRGBA[3]
     right.getRed(&right1, green: &right2, blue: &right3, alpha: &right4)
    
    return UIColor(
        red: rightRGBA[0] * left,
        green: rightRGBA[1] * left,
        blue: rightRGBA[2] * left,
        alpha: rightRGBA[3]
    )
}

func * (left: UIColor, right: CGFloat) -> UIColor {
    return right * left
}
