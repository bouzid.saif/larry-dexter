import UIKit

public extension UIView {

    @objc public func onTap(_ handler: @escaping (UITapGestureRecognizer) -> Void) {
        addGestureRecognizer(UITapGestureRecognizer(taps: 1, handler: handler))
    }

    @objc public func onDoubleTap(_ handler: @escaping (UITapGestureRecognizer) -> Void) {
        addGestureRecognizer(UITapGestureRecognizer(taps: 2, handler: handler))
    }

    @objc public func onLongPress(_ handler: @escaping (UILongPressGestureRecognizer) -> Void) {
        addGestureRecognizer(UILongPressGestureRecognizer(handler: handler))
    }

    @objc public func onSwipeLeft(_ handler: @escaping (UISwipeGestureRecognizer) -> Void) {
        addGestureRecognizer(UISwipeGestureRecognizer(direction: .left, handler: handler))
    }

    @objc public func onSwipeRight(_ handler: @escaping (UISwipeGestureRecognizer) -> Void) {
        addGestureRecognizer(UISwipeGestureRecognizer(direction: .right, handler: handler))
    }

    @objc public func onSwipeUp(_ handler: @escaping (UISwipeGestureRecognizer) -> Void) {
        addGestureRecognizer(UISwipeGestureRecognizer(direction: .up, handler: handler))
    }

    @objc public func onSwipeDown(_ handler: @escaping (UISwipeGestureRecognizer) -> Void) {
        addGestureRecognizer(UISwipeGestureRecognizer(direction: .down, handler: handler))
    }

    @objc public func onPan(_ handler: @escaping (UIPanGestureRecognizer) -> Void) {
        addGestureRecognizer(UIPanGestureRecognizer(handler: handler))
    }

    @objc public func onPinch(_ handler: @escaping (UIPinchGestureRecognizer) -> Void) {
        addGestureRecognizer(UIPinchGestureRecognizer(handler: handler))
    }

    @objc public func onRotate(_ handler: @escaping (UIRotationGestureRecognizer) -> Void) {
        addGestureRecognizer(UIRotationGestureRecognizer(handler: handler))
    }

    @objc public func onScreenEdgePan(_ handler: @escaping (UIScreenEdgePanGestureRecognizer) -> Void) {
        addGestureRecognizer(UIScreenEdgePanGestureRecognizer(handler: handler))
    }
}
