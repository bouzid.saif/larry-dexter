✨  )   8,@��
�P�P
�@
�0
� 
��
��
A�Ќ�+�-��+��/�+��(��C�B)�B(�B(�B(<0B+�)�+��/�B(�B)<-��,�B)��A�(�B+�B)�-��(��+��*<8)��(��/��+�B+��+��,<0�)��+�,��+�B+��, �	  
  %     Tf�� 0"��  m  �   Apple Swift version 4.0.3 (swiftlang-900.0.74.1 clang-900.0.39.2)   �   armv7-apple-ios8.0      �  �	  2J�&4�        �P�[   W  s:7Chronos13VariableTimerCACyAA09RepeatingC0_p_Sitc7closure_SdAC_Sitc16intervalProvidertcfc   Creates a VariableTimer object.         /**
    Creates a VariableTimer object.
    
    - parameter executionClosure:    The closure to execute at a variable interval.
    - parameter intervalClosure:     The closure to execute to obtain a variable
    interval.
    
    - returns: A newly created VariableTimer object.
    */        �,   �   c:@M@Chronos@objc(cs)VariableTimer(im)start:   Starts the timer.      j   /**
    Starts the timer.
    
    - parameter now:     true, if the timer should fire immediately.
    */        �n��\   �  s:7Chronos13DispatchTimerCACSd8interval_yAA09RepeatingC0_p_Sitc7closureSo0B5QueueC5queuetcfc   Creates a DispatchTimer object.      �  /**
    Creates a DispatchTimer object.
    
    - parameter interval:        The execution interval, in seconds.
    - parameter closure:         The closure to execute at the given interval.
    - parameter queue:           The queue that should execute the given closure.
    - parameter failureClosure:  The closure to execute if creation fails.
    
    - returns: A newly created DispatchTimer object.
    */        I��p-   e   c:@M@Chronos@objc(cs)VariableTimer(py)closure    The timer’s execution closure.      -   /**
    The timer's execution closure.
    */       �}�L'      c:@M@Chronos@objc(pl)Timer(py)isRunning,   Returns whether the timer is running or not.      ;   /**
    Returns whether the timer is running or not.
    */       I�0,   �   c:@M@Chronos@objc(cs)DispatchTimer(im)cancel   Permanently cancels the timer.      �   /**
    Permanently cancels the timer.
    
    Attempting to start or pause an invalid timer is considered an error and will throw an exception.
    */        �5�-,   �   c:@M@Chronos@objc(cs)DispatchTimer(im)start:   Starts the timer.      j   /**
    Starts the timer.
    
    - parameter now:     true, if the timer should fire immediately.
    */    	    ��9�+   a   c:@M@Chronos@objc(cs)VariableTimer(py)queue   The timer’s execution queue.      +   /**
    The timer's execution queue.
    */       Q;3q#   �   c:@M@Chronos@objc(pl)RepeatingTimeri   Types adopting the RepeatingTimer protocol can be used to implement methods to control a repeating timer.      p   /**
Types adopting the RepeatingTimer protocol can be used to implement methods to
control a repeating timer.
*/         �a�"   �  c:@M@Chronos@objc(cs)VariableTimer�   A VariableTimer allows you to create a Grand Central Dispatch-based timer object that allows for variable time intervals between each firing. Each successive time interval is obtained by executing the given interval closure.      �  /**
A VariableTimer allows you to create a Grand Central Dispatch-based timer object
that allows for variable time intervals between each firing. Each successive
time interval is obtained by executing the given interval closure.

A timer has limited accuracy when determining the exact moment to fire; the
actual time at which a timer fires can potentially be a significant period of
time after the scheduled firing time. However, successive fires are guarenteed
to occur in order.
*/         	+�-   e   c:@M@Chronos@objc(cs)DispatchTimer(py)closure    The timer’s execution closure.      -   /**
    The timer's execution closure.
    */        T�Ċ,   �   c:@M@Chronos@objc(pl)RepeatingTimer(py)count<   The number of times the execution closure has been executed.      K   /**
    The number of times the execution closure has been executed.
    */        W���+   �   c:@M@Chronos@objc(cs)VariableTimer(im)pause.   Pauses the timer and does not reset the count.      =   /**
    Pauses the timer and does not reset the count.
    */        �[As   �  s:7Chronos13VariableTimerCACyAA09RepeatingC0_p_Sitc7closure_SdAC_Sitc16intervalProviderSo13DispatchQueueC5queuetcfc   Creates a VariableTimer object.      f  /**
    Creates a VariableTimer object.
    
    - parameter executionClosure:    The closure to execute at a variable interval.
    - parameter intervalClosure:     The closure that provides time intervals.
    - parameter queue:               The queue that should execute the given closure.
    
    - returns: A newly created VariableTimer object.
    */    	   X�"M-   �   c:@M@Chronos@objc(cs)VariableTimer(py)isValid.   true, if the timer is valid; otherwise, false.         /**
    true, if the timer is valid; otherwise, false.
    
    A timer is considered valid if it has not been canceled.
    */         ]M   �   s:7Chronos16ExecutionClosurea,   The closure to execute when the timer fires.      �   /**
The closure to execute when the timer fires.

- parameter timer:   The timer that fired.
- parameter count:   The current invocation count. The first count is 0.
*/         ڮ*K   8  s:7Chronos13DispatchTimerCACSd8interval_yAA09RepeatingC0_p_Sitc7closuretcfc   Creates a DispatchTimer object.        /**
    Creates a DispatchTimer object.
    
    - parameter interval:        The execution interval, in seconds.
    - parameter closure:         The closure to execute at the given interval.
    
    - returns: A newly created DispatchTimer object.
    */        g�+   a   c:@M@Chronos@objc(cs)DispatchTimer(py)queue   The timer’s execution queue.      +   /**
    The timer's execution queue.
    */        ��uN"   �  c:@M@Chronos@objc(cs)DispatchTimer�   A DispatchTimer allows you to create a Grand Central Dispatch-based timer object. A timer waits until a certain time interval has elapsed and then fires, executing a given closure.      �  /**
A DispatchTimer allows you to create a Grand Central Dispatch-based timer
object. A timer waits until a certain time interval has elapsed and then
fires, executing a given closure.

A timer has limited accuracy when determining the exact moment to fire; the
actual time at which a timer fires can potentially be a significant period
of time after the scheduled firing time. However, successive fires are
guarenteed to occur in order.
*/         �Ң$   k   c:@M@Chronos@objc(pl)Timer(im)cancel"   Cancels and invalidates the timer.      1   /**
    Cancels and invalidates the timer.
    */        !OF=$   �   c:@M@Chronos@objc(pl)Timer(im)start:   Starts the timer      n   /**
    Starts the timer
    
    - parameter now: true, if timer starts immediately; false, otherwise.
    */       ��I�+   �   c:@M@Chronos@objc(cs)DispatchTimer(im)pause.   Pauses the timer and does not reset the count.      =   /**
    Pauses the timer and does not reset the count.
    */    
    "'�u-   �   c:@M@Chronos@objc(cs)DispatchTimer(py)isValid.   true, if the timer is valid; otherwise, false.         /**
    true, if the timer is valid; otherwise, false.
    
    A timer is considered valid if it has not been canceled.
    */        f~+G*      s:7Chronos13DispatchTimerC8intervalSQySdGv-   The timer’s execution interval, in seconds.      :   /**
    The timer's execution interval, in seconds.
    */        ���z'   �   s:7Chronos13VariableTimerC8scheduleySbF$   Schedules the next execution closure      �   /**
    Schedules the next execution closure
    
    - parameter now: true, if the execution closure should be scheduled immediately;
    false, otherwise
    */    
    ��H.   e   c:@M@Chronos@objc(pl)RepeatingTimer(py)closure    The timer’s execution closure.      -   /**
    The timer's execution closure.
    */        ���/   �   c:@M@Chronos@objc(cs)VariableTimer(py)isRunning:   true, if the timer is currently running; otherwise, false.      I   /**
    true, if the timer is currently running; otherwise, false.
    */        �`D:   c   s:7Chronos13VariableTimerC16intervalProviderSQySdAC_SitcGv   The timer’s interval closure.      ,   /**
    The timer's interval closure.
    */        �x#   a   c:@M@Chronos@objc(pl)Timer(py)queue   The timer’s execution queue.      +   /**
    The timer's execution queue.
    */        ����   �   c:@M@Chronos@objc(pl)TimerV   Types adopting the Timer protocol can be used to implement methods to control a timer.      ]   /**
Types adopting the Timer protocol can be used to implement methods to control
a timer.
*/         �Ù/   �   c:@M@Chronos@objc(cs)DispatchTimer(py)isRunning:   true, if the timer is currently running; otherwise, false.      I   /**
    true, if the timer is currently running; otherwise, false.
    */        ��9�+   �   c:@M@Chronos@objc(cs)VariableTimer(py)count<   The number of times the execution closure has been executed.      K   /**
    The number of times the execution closure has been executed.
    */        7���,     s:7Chronos13VariableTimerC15IntervalClosurea0   The closure to execute the next interval length.      �   /**
    The closure to execute the next interval length.
    
    - parameter timer:   The timer that fired.
    - parameter count:   The next invocation count. The first count is 0.
    */       w.tf#   I   c:@M@Chronos@objc(pl)Timer(im)pause   Pauses the timer.          /**
    Pauses the timer.
    */        xsCs%   {   c:@M@Chronos@objc(pl)Timer(py)isValid*   Returns whether the timer is valid or not.      9   /**
    Returns whether the timer is valid or not.
    */        �h��,   �   c:@M@Chronos@objc(cs)VariableTimer(im)cancel   Permanently cancels the timer.      �   /**
    Permanently cancels the timer.
    
    Attempting to start or pause an invalid timer is considered an error and
    will throw an exception.
    */       ��+   �   c:@M@Chronos@objc(cs)DispatchTimer(py)count<   The number of times the execution closure has been executed.      K   /**
    The number of times the execution closure has been executed.
    */        @   %      �              �              �      (                      �  �	  �  [          4  �    #  �  N                �  !              !  �  �  �          d                    �  �       l!      D"  �#                          �$  "
h!               